#!/usr/bin/env python3
from candr_analysis.transformers import pickle
from candr_analysis.utilities import progress, pipeline

loader = pickle.PickleLoader('test-pickle.pkl')
printer = pipeline.PipePrinter()
progress = progress.CLIProgress("Test Pickle")
processes = pipeline.Pipeline(progress=progress)
passthru = pipeline.RateLimit(0.05)
processes.add(loader)
processes.add(passthru)
processes.add(printer)
processes.start()
processes.stop()
