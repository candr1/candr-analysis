#!/usr/bin/env python3
import sys
import code
from sqlalchemy.orm import aliased, with_polymorphic
from sqlalchemy import select
from candr_analysis.analysis.graph import models, graph_analysis


orm = graph_analysis.ORMWrapper(sys.argv[1])
rep = orm.session.query(models.Representation).first().representation
print("Representation is: " + str(rep))
node_q = orm.session.query(models.Node.id).filter(
    models.Node.representations.any(representation=rep)
)
all_node_ids = list([n[0] for n in node_q.all()])
print("Node_q: " + str(len(all_node_ids)))
link_assoc_q = orm.session.query(models.LinkAssociation.link_id).join(models.Node).filter(
    models.LinkAssociation.node_id.in_(all_node_ids)
).filter(
    models.Node.id == models.LinkAssociation.node_id
)
print("Link_assoc_q: " + str(link_assoc_q.count()))
link_q = orm.session.query(models.Link.id).join(
    models.LinkAssociation
).join(
    models.Node
).filter(
    models.LinkAssociation.node_id.in_(all_node_ids)
).filter(
    models.Node.id == models.LinkAssociation.node_id
)
print("Link_q: " + str(link_q.count()))
link_assoc_a = aliased(models.LinkAssociation)
link_assoc_b = aliased(models.LinkAssociation)
node_a = with_polymorphic(models.Representable, [models.Node], flat=True, aliased=True)
node_b = with_polymorphic(models.Representable, [models.Node], flat=True, aliased=True)
representationAssociation_a = aliased(models.RepresentationAssociation)
representationAssociation_b = aliased(models.RepresentationAssociation)
representation_a = aliased(models.Representation)
representation_b = aliased(models.Representation)
representation_type_a = aliased(models.RepresentationType)
representation_type_b = aliased(models.RepresentationType)
instance_b = with_polymorphic(models.Representable, [models.Instance], flat=True, aliased=True)
direction_type_a = aliased(models.DirectionType)
direction_type_b = aliased(models.DirectionType)
linked_q = orm.session.query(
    instance_b.Instance.name,
    models.Link.strength,
    node_a.id,
    representation_a.representation,
    representation_type_a.name,
    node_b.id,
    representation_b.representation,
    representation_type_b.name,
    models.LinkType.name
).join(
    link_assoc_a,
    node_a.id == link_assoc_a.node_id
).join(
    direction_type_a,
    link_assoc_a.type_id == direction_type_a.id
).join(
    models.Link,
    link_assoc_a.link_id == models.Link.id
).join(
    models.LinkType,
    models.Link.type_id == models.LinkType.id
).join(
    link_assoc_b,
    link_assoc_b.link_id == models.Link.id
).join(
    direction_type_b,
    link_assoc_b.type_id == direction_type_b.id
).join(
    node_b,
    node_b.id == link_assoc_b.node_id
).join(
    instance_b,
    node_b.Node.instance_id == instance_b.Instance.id
).join(
    representationAssociation_b,
    node_b.Node.id == representationAssociation_b.columns.representable_id
).join(
    representation_b,
    representationAssociation_b.columns.representation_id == representation_b.id
).join(
    representation_type_b,
    representation_type_b.id == representation_b.type_id
).join(
    representationAssociation_a,
    node_a.Node.id == representationAssociation_a.columns.representable_id
).join(
    representation_a,
    representationAssociation_a.columns.representation_id == representation_a.id
).join(
    representation_type_a,
    representation_type_a.id == representation_a.type_id
).filter(
    direction_type_a.name == 'out'
).filter(
    direction_type_b.name == 'in'
).filter(
    link_assoc_a.node_id != link_assoc_b.node_id
).filter(
    node_a.Node.instance_id == node_b.Node.instance_id
).filter(
    representation_a.representation == rep
)
print("Linked_q: " + str(linked_q.count()))
print(str(linked_q))
code.interact(local=locals())
