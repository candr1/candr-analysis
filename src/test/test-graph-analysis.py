#!/usr/bin/env python3
from candr_analysis.utilities import pipeline, iterator
from candr_analysis.utilities import progress as prog
from candr_analysis.analysis.graph import graph_analyser, graph_record
from candr_analysis.parsers.MEI import MEIDivisione, MEINote

test_record = graph_record.MEIGraphRecord(
    'test',
    (
        MEIDivisione(loc=None, len=None, uniqid=None, editorial=False),
        MEINote(pname='g', oct=2, ligature_type='square', uniqid=None, editorial=False)
    ), 
    (
        MEINote(pname='b', oct=2, ligature_type=False, uniqid=None, editorial=False),
        MEIDivisione(loc=None, len=None, uniqid=None, editorial=False)
    ),
    'testinstance',
    'testtype',
    1
)


loader = iterator.IteratorLoader([test_record])
analyser = graph_analyser.MEIGraphAnalyser('setting-MEI-graphs-2.db')
printer = pipeline.PipePrinter()
progress_module = prog.CLIProgress("Test Analysis")
processes = pipeline.Pipeline(progress=progress_module)
processes.add(loader)
processes.add(analyser)
processes.add(printer)
processes.start()
processes.stop()
