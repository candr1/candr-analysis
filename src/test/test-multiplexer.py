#!/usr/bin/env python3
import random
from candr_analysis.utilities.pipeline import Pipeline, PipePrinter, Multiplexer, Counter, Delay
from candr_analysis.utilities.progress import CLIProgress
prog = CLIProgress("Test Multiplexer")
processes = Pipeline(progress=prog)
processes.add(Counter(1000))
processes.add(Multiplexer([Delay(random.random()) for i in range(8)]))
processes.add(PipePrinter())
processes.start()
processes.stop()
