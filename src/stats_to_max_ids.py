#!/usr/bin/env python3
import csv
import sys
import io
import numpy as np

take = abs(int(sys.argv[1]))
smooth = abs(int(sys.argv[2]))
fname = sys.argv[3]
keys = []
with open(fname, newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    keys = []
    records = []
    for i, row in enumerate(reader):
        # Python CSV reader overwrites columns with the same names, so we only
        # have to worry about p values.  DictReader preserves original ordering
        if i == 0:
            keys = list([k.split('/')[-2] for k in row.keys()])
        records.append(list(row.values()))
if len(keys) == 0:
    sys.exit(0)
data = np.array(records, dtype='f')
means = np.mean(data, axis=0)
best = sorted(zip(means, keys))[:take]
idxs = list([keys.index(b[1]) for b in best])
filtered_data = data[:,idxs]
smoothed = np.apply_along_axis(
    lambda x: np.convolve(x, np.ones(smooth), 'valid') / smooth,
    axis=0,
    arr=filtered_data
)
out = io.StringIO()
fieldnames = list([b[1] for b in best])
writer = csv.DictWriter(out, fieldnames=fieldnames, dialect='unix')
writer.writeheader()
writer.writerows([dict(zip(fieldnames, row)) for row in smoothed])
out.seek(0)
print(str(out.read()))
