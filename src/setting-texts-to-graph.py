#!/usr/bin/env python3
import cli_ui
import sys
from typing import Any
from candr_analysis.transformers import pickle
from candr_analysis.transformers.text import gram
from candr_analysis.utilities import progress, pipeline
from candr_analysis.analysis.graph import graph_record, graph_analyser

progress = progress.CLIProgress()
processes = pipeline.Pipeline(progress=progress)
processes.add(pickle.PickleLoader('setting-texts.pkl', progress=progress))
processes.add(gram.TextNGrams(1, case=False, progress=progress))
processes.add(graph_record.TextGraphRecordTransformer())
#def print_fun(*tokens: Any, **kwargs: Any):
#    ret = progress.info_2(cli_ui.bold, "PipePrinter received", cli_ui.reset, *tokens, **kwargs)
#    sys.stdout.flush()
#    return ret
#processes.add(pipeline.PipePrinter(print_fun))
processes.add(graph_analyser.TextGraphAnalysis())
processes.start()
processes.stop()
