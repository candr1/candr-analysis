#!/usr/bin/env python3
from candr_analysis.utilities.pipeline import Pipeline, Truncate
from candr_analysis.utilities.progress import CLIProgress
from candr_analysis.transformers.pickle import PickleLoader, PickleSaver
prog = CLIProgress("Truncate links")
processes = Pipeline(progress=prog)
processes.add(PickleLoader('links-MEI-2.pkl', progress=prog))
processes.add(Truncate(2001))
processes.add(PickleSaver('links-MEI-2-trunc.pkl', flush=True))
processes.start()
processes.stop()
