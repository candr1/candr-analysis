#!/usr/bin/env python3
import pickle
from collections import defaultdict
from itertools import count
from candr_analysis.parsers import MEI
f = open('parsed-MEI-accid.pkl', 'rb')
nodes = defaultdict(lambda: set())
try:
    for i in count():
        item = pickle.load(f)
        print(i)
        these_accids = {MEI.MEIElementFactory.strip_uniqid(x) for x in item.transcription.nodes if isinstance(x, MEI.MEIAccidental)}
        for accid in these_accids:
            nodes[accid].add((item.subject, item.type))
except EOFError:
    pass
print(nodes)
