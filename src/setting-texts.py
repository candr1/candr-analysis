#!/usr/bin/env python3
import cli_ui
from typing import Any
from collections import deque
from candr_analysis.scrapers.candr.rdf import setting
from candr_analysis.transcribers.candr import text
from candr_analysis.utilities import pipeline, progress
from candr_analysis.transformers.text import gram
from candr_analysis.transformers import pickle

progress = progress.CLIProgress()
processes = pipeline.Pipeline()
processes.add(setting.Setting(progress=progress))
processes.add(text.Setting(progress=progress))
def print_fun(*tokens: Any, **kwargs: Any):
    ret = progress.info_2(cli_ui.bold, "PipePrinter received", cli_ui.reset, *tokens, **kwargs)
processes.add(pipeline.PipePrinter(print_fun))
processes.add(pickle.PickleSaver('setting-texts.pkl', flush = True))
processes.start()
processes.stop()
