#!/usr/bin/env python3
import pickle
import io
import sys
import cli_ui
import click
from itertools import count
from xml.etree import ElementTree
from candr_analysis.transcribers.candr.MEI import MEITranscriptionRecord
from tqdm import tqdm

f_in = open(sys.argv[1], 'rb')
f_out = io.BytesIO()
try:
    for i in count():
        loaded = pickle.load(f_in)
        cli_ui.info(f"Received:")
        print(str(loaded), type(loaded))
        try:
            tree = ElementTree.fromstring(loaded.transcription)
        except ElementTree.ParseError:
            correct = cli_ui.ask_yes_no(f"{i} correct?", default=True)
            while not correct:
                text = loaded.transcription
                new_text = click.edit(text)
                if not new_text:
                    new_text = text
                loaded = MEITranscriptionRecord(loaded.subject, loaded.type, new_text)
                print(loaded)
                correct = cli_ui.ask_yes_no(f"{i} correct?", default=True)
        pickle.dump(loaded, f_out, pickle.HIGHEST_PROTOCOL)
except EOFError:
    pass
f_in.close()
f_in = open(sys.argv[2], 'wb')
f_in.write(f_out.read())
