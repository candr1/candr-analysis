#!/usr/bin/env python3
import cli_ui
from candr_analysis.scrapers.candr.rdf import setting
from candr_analysis.transcribers.candr import MEI
from candr_analysis.utilities import pipeline, progress
from candr_analysis.transformers import pickle

progress = progress.CLIProgress()
processes = pipeline.Pipeline()
processes.add(setting.Setting(name="RDFSetting", progress=progress))
processes.add(MEI.Setting(name="MEITranscriber", progress=progress))
processes.add(pickle.PickleSaver('setting-MEI.pkl', flush = True))
processes.start()
processes.stop()
