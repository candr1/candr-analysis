#!/usr/bin/env python3
import sys
import code
from candr_analysis.analysis.graph import graph_analysis as ga
from candr_analysis.analysis.graph import models as mods

orm = ga.ORMWrapper(sys.argv[1])
instances = orm.session.query(mods.Instance).all()
nodes = orm.session.query(mods.Node).all()
code.interact(local=locals(), banner="Instances available in variable \"instances\", nodes in variable \"nodes\"")
