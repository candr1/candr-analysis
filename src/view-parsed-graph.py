#!/usr/bin/env python3
import code
from candr_analysis.transformers import pickle
from candr_analysis.parsers import MEI
from candr_analysis.utilities import pipeline
pickle_file = 'setting-MEI.pkl'
pickle_loader = pickle.PickleLoader(pickle_file)
pickle_loader.setup()
def only_missing(x):
    missing_settings  = {'https://www.candr.tk/browse/setting/279/'}
    is_missing = x.subject in missing_settings
    if is_missing:
        print(x.subject + " is missing")
    return is_missing
filter = pipeline.Filter(only_missing)
filter.setup()
parser = MEI.MEIParser(editorial=False)
parser.setup()
rets = []
def wrap_eof(gen):
    try:
        yield from gen
    except EOFError:
        return
for i in wrap_eof(pickle_loader.process(None)):
    for j in wrap_eof(filter.process(i)):
        for k in wrap_eof(parser.process(j)):
            rets.append(k)
print("Returned " + str(len(rets)) + " results")
local_dict = {
    '__name__': '__console__',
    '__doc__': None,
    'graphs': rets
}
code.interact(local=local_dict, banner="Graphs available in variable \"graphs\"")
