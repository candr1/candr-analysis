#!/usr/bin/env python3
import pickle
from itertools import count
from candr_analysis.parsers import MEI
f = open('parsed-MEI.pkl', 'rb')
nodes = set()
try:
    for i in count():
        item = pickle.load(f)
        print(i)
        nodes |= {MEI.MEIElementFactory.strip_uniqid(x) for x in item.transcription.nodes}
except EOFError:
    pass
print(nodes)
