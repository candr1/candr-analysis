#!/usr/bin/env python3
from multiprocessing import cpu_count
from pqdm.processes import pqdm
from candr_analysis.transformers import pickle
from candr_analysis.parsers import MEI
from candr_analysis.utilities import progress, pipeline
from candr_analysis.analysis.graph import corpus_graph

pickle_file = 'setting-MEI.pkl'

prog = progress.CLIProgress("MEI corpus graph")
processes = pipeline.Pipeline(progress=prog)
processes.add(pickle.PickleLoader('setting-MEI.pkl', progress=prog))
processes.add(MEI.MEIParser(editorial=False))
processes.add(corpus_graph.CorpusGraph(MEI.MEIElementFactory.strip_uniqid, prog))
processes.add(pickle.PickleSaver('MEI-settings-corpus.pkl', flush=True))
processes.start()
processes.stop()
