#!/usr/bin/env python3
import csv
import gzip
import sys
import numpy as np
csv_file = sys.argv[1]
reader = csv.DictReader(gzip.open(csv_file, 'rt'))
for row in reader:
    print('\t'.join(np.format_float_positional(float(x), trim='-') for x in row.values()))
