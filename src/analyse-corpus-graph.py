#!/usr/bin/env python3
from multiprocessing import cpu_count
from slugify import slugify
from datetime import datetime
from candr_analysis.transformers import pickle
from candr_analysis.parsers import MEI
from candr_analysis.utilities import progress, pipeline
from candr_analysis.analysis.graph import corpus_svm, graph_statistics
prog = progress.CLIProgress(f"Corpus analyser n-grams")
processes = pipeline.Pipeline(progress=prog)
# processes.add(pickle.PickleLoader('setting-MEI.pkl', progress=None))
# processes.add(
#     pipeline.Multiplexer(
#         [
#             MEI.MEIParser(editorial=False, accidentals=True, ligatures=False)
#             for _ in range(cpu_count())
#         ]
#     )
# )
# processes.add(pickle.PickleSaver('parsed-MEI-accid-no-lig.pkl', flush=True))
log_folder = 'stats-' + slugify(datetime.now().isoformat())
processes.add(pickle.PickleLoader('parsed-MEI-accid-no-lig.pkl', progress=None))
processes.add(corpus_svm.CorpusSVM(
    strip_fun = MEI.MEIElementFactory.strip_uniqid,
    pca_components = 100,
    kernel_approximation_components = 1000,
    minimum_batch = 50000,
    nu = 0.01,
    min_svm = 3,
    visualisation_samples = 100
))
processes.add(graph_statistics.GraphStatisticSaver(folder=log_folder))
processes.start()
processes.stop()
