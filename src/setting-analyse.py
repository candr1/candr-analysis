#!/usr/bin/env python3
import multiprocessing
from candr_analysis.utilities.pipeline import Pipeline, PipePrinter, Multiplexer
from candr_analysis.utilities.progress import CLIProgress
from candr_analysis.transformers.pickle import PickleLoader, PickleSaver
from candr_analysis.analysis.graph import graph_analyser, graph_statistics, graph_similarity_plot
from candr_analysis.parsers import MEI
from candr_analysis.transformers.MEI import gram

prog = CLIProgress("Analyse 2-grams")
processes = Pipeline(progress=prog)
# processes.add(graph_analyser.MEIGraphAnalyserQuery(
#     store = 'setting-MEI-graphs-2.db'
# ))
# processes.add(PickleSaver('links-MEI-2.pkl', flush=True))
#processes.add(graph_analyser.DummyAnalyserQuery(100, 2))

# processes.add(PickleLoader('links-MEI-2.pkl', progress=None))
# link_similarity = graph_analyser.link_similarity_default()
# graph_similarity = graph_analyser.graph_similarity_default(link_similarity)
# processes.add(
#     Multiplexer([
#         graph_analyser.GraphNodesSimilarityAnalyse(graph_similarity)
#         for _ in range(multiprocessing.cpu_count())
#     ])
# )
# processes.add(PickleSaver('similarities-MEI-2.pkl', flush=True))

# processes.add(PickleLoader('similarities-MEI-2.pkl', progress=prog))
# processes.add(graph_statistics.GraphSimilarityStatistics())
# processes.add(graph_statistics.GraphStatisticSaver('stats'))
# processes.add(PickleSaver('statistics-MEI-2.pkl', flush=True))
stats_loader = PickleLoader('statistics-MEI-2.pkl', progress=prog)
# processes.add(PickleLoader('setting-MEI.pkl', progress=prog))
# processes.add(MEI.MEIParser(editorial=False))
# processes.add(gram.MEINGrams(2, progress=prog))
# processes.add(PickleSaver('NGrams-MEI-2.pkl', flush=True))
processes.add(PickleLoader('NGrams-MEI-2.pkl', progress=prog))
processes.add(graph_similarity_plot.GraphSimilarityPlot(
    'setting-MEI-graphs-2.db',
    stats_loader
))
processes.add(graph_statistics.GraphStatisticSaver('stats'))
processes.start()
processes.stop()
