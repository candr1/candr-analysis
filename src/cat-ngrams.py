#!/usr/bin/env python3
from candr_analysis.utilities.pipeline import Pipeline, PipePrinter, Filter, Cache
from candr_analysis.utilities.progress import CLIProgress
from candr_analysis.transformers.pickle import PickleLoader
from candr_analysis.analysis.graph import graph_record

prog = CLIProgress("Cat N-grams")
processes = Pipeline(progress=prog)
processes.add(PickleLoader('NGrams-MEI-2.pkl', progress=None))
def pred(input_data):
    if isinstance(input_data, (graph_record.GraphRecord, graph_record.MEIGraphRecord)):
        input_data = graph_record.GraphRecordCollection([input_data])
    return any(
        r.relationship == 'nextgram' and
        r.instance == 'https://www.candr.tk/browse/setting/441/' # report 146
        for r in input_data.records
    )
processes.add(Filter(pred))
processes.add(Cache())
processes.add(PipePrinter())
processes.start()
processes.stop()
