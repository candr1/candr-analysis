#!/usr/bin/env python3
import sys
import pickle
from candr_analysis.analysis.graph import graph_analysis as ga
from candr_analysis.analysis.graph import models as mods

print("Loading graph...")
orm = ga.ORMWrapper(sys.argv[1])
instances = orm.session.query(mods.Instance).all()
graph_subjects = set([x.name for x in instances])
print("Loading pickle...")
pickle_file = 'setting-MEI.pkl'
fp = open(pickle_file, 'rb')
pickle_subjects = set()
try:
    while True:
        x = pickle.load(fp)
        pickle_subjects.add(x.subject)
except EOFError:
    pass
diff = pickle_subjects - graph_subjects
print(diff)
print(len(diff))
