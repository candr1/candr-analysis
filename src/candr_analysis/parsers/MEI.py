"""Parses an MEI stream into an AST"""

from collections import namedtuple, deque
from typing import Iterator
from copy import deepcopy
from itertools import chain
from xml.etree import ElementTree
import networkx as nx
from ..validators.MEI import MEIValidator
from ..transcribers.candr.MEI import MEITranscriptionRecord
from ..utilities.progress import CLIProgress
from ..utilities.pipeline import PipeObject

three_none_tuple = (None, None, None, False)
four_none_tuple = (None, None, None, None, False)
five_none_tuple = (None, None, None, None, None, False)
MEIClef = namedtuple('MEIClef', ['shape', 'line', 'uniqid', 'editorial'], defaults=three_none_tuple)
MEINote = namedtuple('MEINote', ['pname', 'oct', 'ligature_type', 'uniqid', 'editorial'], defaults=four_none_tuple)
MEINoteWithAlter = namedtuple('MEINoteWithAlter', ['pname', 'oct', 'accid', 'ligature_type', 'uniqid', 'editorial'], defaults=five_none_tuple)
MEIDivisione = namedtuple('MEIDivisione', ['loc', 'len', 'uniqid', 'editorial'], defaults=three_none_tuple)
MEISyllable = namedtuple('MEISyllable', ['wordpos', 'text', 'uniqid', 'editorial'], defaults=three_none_tuple)
MEIAccidental = namedtuple('MEIAccidental', ['accid', 'pname', 'oct', 'uniqid', 'editorial'], defaults=four_none_tuple)
MEIPlica = namedtuple('MEIPlica', ['dir', 'ligature', 'uniqid', 'editorial'], defaults=three_none_tuple)

MEIParsedRecord = namedtuple(
    'MEIParsedRecord',
    MEITranscriptionRecord._fields + ('routes',)
)

class MEIElementFactory:
    """Creates MEIElements"""
    def __init__(self, editorial = False, accidentals = False, ligatures = True, error_log = print):
        self._ns = {
            'mei': 'http://www.music-encoding.org/ns/mei',
            'xml': 'http://www.w3.org/XML/1998/namespace'
        }
        self._counter = 0
        self._error_log = error_log
        self._editorial = editorial
        self._funs = {
            f'{{{self._ns["mei"]}}}clef': self._create_clef,
            f'{{{self._ns["mei"]}}}note': self._create_note,
            f'{{{self._ns["mei"]}}}ligature': self._create_ligature,
            f'{{{self._ns["mei"]}}}divisione': self._create_divisione,
            f'{{{self._ns["mei"]}}}syl': self._create_syllable,
            f'{{{self._ns["mei"]}}}accid': self._create_accidental,
            f'{{{self._ns["mei"]}}}plica': self._create_plica,
            f'{{{self._ns["mei"]}}}supplied': self._create_supplied
        }
        if accidentals == False:
            self._funs[f'{{{self._ns["mei"]}}}accid'] = lambda: []
            self._funs[f'{{{self._ns["mei"]}}}note'] = self._create_note_with_alter
        if ligatures == False:
            self._funs[f'{{{self._ns["mei"]}}}ligature'] = self._create_no_ligature
    def create_element(self, element: ElementTree.Element, unique=True, editorial=False):
        """Create an element from an MEI tag"""
        try:
            if unique:
                uniqid = self._uniqid()
                return self._funs[element.tag](element, uniqid, (editorial if self._editorial else False))
            else:
                return self._funs[element.tag](element, None)
        except KeyError:
            self._error_log("Element not supported: " + element.tag)
            return None
    def _uniqid(self):
        """Generate a uniqid (incremental)"""
        self._counter += 1
        return self._counter
    def _create_supplied(self, supplied: ElementTree.Element, uniqid, editorial=True):
        unique = uniqid is not None
        return list(chain.from_iterable(
            [(
                self.create_element(x, unique, bool(self._editorial))
            ) for x in supplied.findall('*')]
        ))
    def _create_clef(self, clef: ElementTree.Element, uniqid, editorial=False):
        """Create a clef"""
        shape = clef.get(f'{{{self._ns["mei"]}}}shape')
        line = clef.get(f'{{{self._ns["mei"]}}}line')
        return [MEIClef(shape, int(line), uniqid, editorial)]
    def _create_note(self, note: ElementTree.Element, uniqid, ligature=None, editorial=False):
        """Create a note"""
        pname = note.get(f'{{{self._ns["mei"]}}}pname')
        oct_ = note.get(f'{{{self._ns["mei"]}}}oct')
        return [MEINote(pname, int(oct_), ligature, uniqid, editorial)] + list(
            chain.from_iterable(
                [self._create_plica(
                    xml_plica,
                    (None if uniqid is None else self._uniqid()),
                    ligature=ligature,
                    editorial=editorial
                ) for xml_plica in note.findall('mei:plica', self._ns)]
            )
        )
    def _create_note_with_alter(self, note: ElementTree.Element, uniqid, ligature=None, editorial=False):
        note_list = self._create_note(note, uniqid, ligature, editorial)
        alter = note.get(f'{{{self._ns["mei"]}}}accid.ges', default='n')
        note_list[0] = MEINoteWithAlter(
            note_list[0].pname, note_list[0].oct, alter, note_list[0].ligature_type,
            note_list[0].uniqid, note_list[0].editorial
        )
        return note_list
    def _create_plica(self, plica: ElementTree.Element, uniqid, ligature=None, editorial=False):
        """Create a plica"""
        dir_ = plica.get(f'{{{self._ns["mei"]}}}dir')
        return [MEIPlica(dir_, ligature, uniqid, editorial)]
    def _create_accidental(self, accidental: ElementTree.Element, uniqid, editorial=False):
        pname = accidental.get(f'{{{self._ns["mei"]}}}ploc')
        oct_ = accidental.get(f'{{{self._ns["mei"]}}}oloc')
        accid = accidental.get(f'{{{self._ns["mei"]}}}accid')
        return [MEIAccidental(accid, pname, int(oct_), uniqid, editorial)]
    def _create_ligature(self, ligature: ElementTree.Element, uniqid, editorial=False):
        """Create a ligature"""
        type_ = ligature.get(f'{{{self._ns["mei"]}}}type')
        return list(
            chain.from_iterable(
                [self._create_note(
                    xml_note,
                    (None if uniqid is None else self._uniqid()),
                    ligature=type_,
                    editorial=editorial
                ) for xml_note in ligature.findall('mei:note', self._ns)]
            )
        )
    def _create_no_ligature(self, ligature: ElementTree.Element, uniqid, editorial=False):
        return list(
            chain.from_iterable(
                [self._create_note(
                    xml_note,
                    (None if uniqid is None else self._uniqid()),
                    ligature=None,
                    editorial=editorial
                ) for xml_note in ligature.findall('mei:note', self._ns)]
            )
        )
    def _create_divisione(self, divisione: ElementTree.Element, uniqid, editorial=False):
        loc = divisione.get(f'{{{self._ns["mei"]}}}loc')
        len_ = divisione.get(f'{{{self._ns["mei"]}}}len')
        return [MEIDivisione(int(loc), float(len_), uniqid, editorial)]
    def _create_syllable(self, syllable: ElementTree.Element, uniqid, editorial=False):
        wordpos = syllable.get(f'{{{self._ns["mei"]}}}wordpos')
        text = syllable.text.strip()
        return [MEISyllable(wordpos, text, uniqid, editorial)]
    @staticmethod
    def strip_uniqid(element):
        strip_attrs = ['uniqid', 'loc', 'len']
        do_strip = {x: None for x in strip_attrs if hasattr(element, x)}
        return element._replace(**do_strip)
    @staticmethod
    def strip_ligature(element):
        strip_attrs = ['ligature_type']
        do_strip = {x: None for x in strip_attrs if hasattr(element, x)}
        return element._replace(**do_strip)
class MEIParser(PipeObject):
    """Parses an MEI stream into a NetworkX graph"""
    def __init__(self, editorial = False, accidentals = False, ligatures = True, progress = CLIProgress(), **kwargs):
        self._progress = progress
        self._validator = MEIValidator(error_log = progress.error)
        self._element_factory = MEIElementFactory(editorial = editorial, accidentals = accidentals, ligatures = ligatures, error_log = progress.error)
        self._ns = {
            'mei': 'http://www.music-encoding.org/ns/mei',
            'xml': 'http://www.w3.org/XML/1998/namespace'
        }
        super().__init__(**kwargs)
    def process(self, input_data: MEITranscriptionRecord) -> Iterator[MEIParsedRecord]:
        """Convert an MEI transcription into a graph"""
        item_store = {}
        node_id = 0

        def link_nodes(graph, a, b, mode, add_weights=True, **kwargs):
            def filter_kwargs(**kwgs):
                return (lambda x: all(k in x[3] and (x[3][k] == v) for k, v in kwgs.items()))
            synched_a_edges = list(
                filter(
                    # Second node is b
                    (lambda x: x[1] == b),
                    filter(
                        # Filter by mode and other things
                        filter_kwargs(mode = mode, **kwargs),
                        # Return edges to a, give data and keys
                        graph.edges(a, True, True)
                    )
                )
            )
            if len(synched_a_edges) == 0:
                graph.add_edge(a, b, mode=mode, weight=1, **kwargs)
            elif len(synched_a_edges) == 1:
                if add_weights:
                    edge = synched_a_edges[0]
                    key = (edge[0], edge[1], edge[2])
                    weight = 2
                    attr_dict = edge[3]
                    if 'weight' in attr_dict:
                        attr_dict['weight'] = attr_dict['weight'] + 1
                    else:
                        attr_dict['weight'] = 1
                    for k, v in kwargs.items():
                        attr_dict[k] += v
                    nx.set_edge_attributes(graph, {key: attr_dict})
            else:
                raise Exception("Nodes are linked more than once using the same mode '" + mode + "'")
            return
        def synch_nodes(graph, a, b):
            link_nodes(graph, a, b, 'synchronous', add_weights=False)
            link_nodes(graph, b, a, 'synchronous', add_weights=False)
        def add_to_graph(graph, item):
            nonlocal node_id
            nonlocal item_store
            hashed = hash(item)
            if hashed in item_store:
                return item_store[hashed]
            node_id += 1
            graph.add_node(item, node_id=node_id)
            item_store[hashed] = item
            return item_store[hashed]

        # Parse the XML
        tree = ElementTree.fromstring(input_data.transcription)

        # Validate
        if not self._validator.validate_tree(tree):
            return False

        # Create graph and stores
        graph = nx.MultiDiGraph()
        id_store = {}
        prev = {}

        routes = {}
        # Iterate through elements
        for division in tree.findall(f'.//{{{self._ns["mei"]}}}mdiv'):
            for section in division.findall('mei:section', self._ns):
                synch_store = {}
                for staff_n, staff in enumerate(section.findall('mei:staff', self._ns), start=1):
                    # The staves are ordered using mei:n
                    xml_staff_n = staff.get(f'{{{self._ns["mei"]}}}n')
                    if xml_staff_n:
                        staff_n = int(xml_staff_n)
                    if staff_n not in routes:
                        routes[staff_n] = []
                    for layer in staff.findall('mei:layer', self._ns):
                        for xml_element in layer.findall('*'):
                            # Create the Python element
                            created_elements = self._element_factory.create_element(xml_element)
                            if created_elements is None:
                                continue
                            for created_element in created_elements:
                                # Add the element to the graph
                                element = add_to_graph(graph, created_element)

                                # Add this element to the route
                                routes[staff_n].append(element)

                                # Link this node to the previous node
                                if staff_n in prev:
                                    link_nodes(graph, prev[staff_n], element, 'sequential')

                                # Add the XMLID to the common store
                                xmlid = xml_element.get(f'{{{self._ns["xml"]}}}id')
                                id_store[xmlid] = element

                                # If there are items that are known to synch with
                                # this element
                                if xmlid in synch_store:
                                    synchs = synch_store[xmlid]
                                    # Synchronise!
                                    for synch in synchs:
                                        el = id_store[synch]
                                        synch_nodes(graph, element, el)

                                # Parse this element synchs
                                synch_attr = xml_element.get(f'{{{self._ns["mei"]}}}synch')
                                xml_synchs = []
                                if synch_attr:
                                    xml_synchs = [x[1:] for x in synch_attr.split(' ')]
                                for synch in xml_synchs:
                                    # Update the synch store with this
                                    if synch not in synch_store:
                                        synch_store[synch] = []
                                    synch_store[synch].append(xmlid)

                                    # We have this one to synch
                                    # They should already be synched, if the MEI is
                                    # correct, but just to be sure!
                                    if synch in id_store:
                                        el = id_store[synch]
                                        synch_nodes(graph, element, el)

                                # Move along one node
                                prev[staff_n] = element
        # Graph should be mostly complete here, 'sequential' and 'synch'.
        # Now we add the 'softsynchs'
        # First, set all nodes to unvisited
        for node in graph.nodes:
            nx.set_node_attributes(graph, {node: {'visited_by': frozenset()}})

        # Create a set of all nodes that have a sequential connection coming in
        has_seq_in = set()
        for u, v, k, d in filter(lambda x: x[3] == 'sequential', graph.edges(None, True, True)):
            has_seq_in.add(v)

        def visit_node(node, visitor):
            nonlocal graph
            visitor_node_ids = frozenset(
                map(
                    (lambda x: x[1]),
                    filter(
                        (lambda x: x[0] == visitor),
                        graph.nodes(data="node_id")
                    )
                )
            )
            any_result = False
            for n, visited_by in filter(lambda x: x[0] == node, graph.nodes(data="visited_by")):
                if len(visited_by.intersection(visitor_node_ids)) > 0:
                    continue
                any_result = True
                visited_by_set = visited_by.union(visitor_node_ids)
                nx.set_node_attributes(graph, {n: {'visited_by': visited_by_set}})
            return any_result

        # Traverse from first nodes, adding weights
        def traverse_node(node):
            nonlocal graph
            filter_seq = lambda x: x[3]['mode'] == 'sequential'
            filter_synch = lambda x: x[3]['mode'] == 'synchronous'
            stack = deque([node])
            while len(stack) > 0:
                n = stack.pop()
                if not visit_node(n, n):
                    continue
                # Recursive function for walking sequential links
                def walk_seq(original, head, dist):
                    nonlocal graph
                    ret = []
                    head_edges = graph.edges(head[0], True, True)
                    # If this node itself has synch
                    if len(list(filter(filter_synch, head_edges))) > 0:
                        ret.append(head[0])
                        return ret
                    # For every sequential link (should only be 1)
                    for u, sequential, key, seqdata in list(filter(filter_seq, head_edges)):
                        # Should only be one of these
                        for sequential_with_dat in filter(lambda x: x[0] == sequential, graph.nodes(True)):
                            if original[1]['node_id'] not in sequential_with_dat[1]['visited_by']:
                                if not visit_node(sequential_with_dat[0], original[0]):
                                    continue
                                # Link this with dist
                                graph.add_edge(original[0], sequential_with_dat[0], mode='softsynch', weight=(1/dist))
                                # Recurse sequentially, keep walking with greater
                                # dist
                                ret += walk_seq(original, sequential_with_dat, dist + 1)
                    return ret
                # Get the node with its data
                for node_with_dat in filter(lambda x: x[0] == n, graph.nodes(True)):
                    # Get its synchronous links
                    for u_synch, v_synch, key_synch, data_synch in list(filter(filter_synch, graph.edges(n, True, True))):
                        # Get every synchs next sequential link
                        for u_seq, v_seq, key_seq, data_seq in list(filter(filter_seq, graph.edges(v_synch, True, True))):
                            # Get that sequential link's node
                            for seq_with_dat in filter(lambda x: x[0] == v_seq, graph.nodes(True)):
                                # Link the original node with that sequential
                                # node on the other voice
                                stack.append(walk_seq(node_with_dat, seq_with_dat, 1))
                for u, sequential, key, seqdata in filter(filter_seq, graph.edges(node, True, True)):
                    stack.append(sequential)
        # For every node that does not have a sequential in (i.e. start nodes)
        start_nodes = set(filter(lambda x: x not in has_seq_in, graph.nodes()))
        for start_node in start_nodes:
            traverse_node(start_node)

        # Check that every node was visited
        unvisited_nodes = list([node for node, data in graph.nodes(True) if len(data['visited_by']) == 0])
        if len(unvisited_nodes) > 0:
            unvisit_str = ', '.join([str(n) for n in unvisited_nodes])
            raise Exception(f'{len(unvisited_nodes)} nodes were not visited in the final traversal: {unvisit_str}')
        for node, data in graph.nodes(True):
            del data['visited_by']

        # For each node, find the distance to the next synchronisation
        def dist_to_synch(node) -> None:
            nonlocal graph
            stack = deque()
            stack.append(node)
            while len(stack) > 0:
                n = stack.pop()
                attr = nx.get_node_attributes(graph, 'dist_to_synch')
                if n in attr:
                    continue
                try:
                    _ = next(filter(
                        (lambda x: x[2]['mode'] == 'synchronous'),
                        graph.edges(n, True)
                    ))
                    nx.set_node_attributes(graph, {n: {'dist_to_synch': 0}})
                except StopIteration:
                    redo = []
                    mins = []
                    min_dist = None
                    for u, v, d in filter(
                        (lambda x: x[2]['mode'] == 'sequential'),
                        graph.edges(n, True)
                    ):
                        if v not in attr:
                            redo.append(v)
                        else:
                            mins.append(v)
                    if redo:
                        stack.append(n)
                        stack.extend(redo)
                    else:
                        min_dist = min([attr[v] for v in mins], default=0)
                        nx.set_node_attributes(graph, {n: {'dist_to_synch': min_dist}})
        for u, d in graph.nodes(True):
            dist_to_synch(u)

        yield MEIParsedRecord(
            input_data.subject,
            input_data.type,
            deepcopy(graph),
            routes
        )
        return
