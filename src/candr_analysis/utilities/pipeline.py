"""Connects generators together with a pipeline"""

import multiprocessing
import threading
import itertools
import datetime
import traceback
import time
import random
import setproctitle
from collections import deque
from queue import Empty, Queue
from typing import Any, Iterator
from .pipetuples import PipePoison, PipeStatus

from .progress import CLIProgress

class Timer:
    def __init__(self):
        self.total = datetime.timedelta(0)
        self.__start = None
        self.__stop = None
        pass
    def start(self):
        if self.__start:
            self.stop()
        self.__stop = None
        self.__start = datetime.datetime.now()
    def stop(self):
        if self.__stop or not self.__start:
            return
        self.__stop = datetime.datetime.now()
        self.total += self.__stop - self.__start
        self.__start = None

class DummyConnection:
    """Pipe that does nothing"""
    def recv(self) -> None:
        """Receives a value from the connection"""
        return None
    def send(self, _) -> None:
        """Sends a value to the connection"""
        return None
    def close(self) -> None:
        """Close the connection"""
        return None

class SingleConnection:
    """Pipe that only sends and receives once, then raises EOFError"""
    def __init__(self):
        self.recvd = False
        self.sent = False
    def recv(self) -> None:
        """Receives a single value from the connection"""
        if self.recvd:
            raise EOFError
        return None
    def send(self, _) -> None:
        """Sends a single value to the connection"""
        if self.sent:
            raise EOFError
        return None
    def close(self) -> None:
        return

class DummyQueue:
    """A queue that discards its input"""
    def put(self, _):
        pass

class PipeObject(multiprocessing.Process):
    """Abstract process in a pipe"""
    def __init__(self, name = None, pipe_in = DummyConnection(), pipe_out = DummyConnection()):
        self.pipe_in = pipe_in
        self.pipe_out = pipe_out
        self.status_queue = DummyQueue()
        self._status_recvd = 0
        self._status_sent = 0
        self._timer = Timer()
        self._status_min_delta = 1
        self._setting_threads = set()
        self._setting_threads_abort_condition = threading.Condition()
        if name is None:
            name = self.__class__.__name__
        self.display_name = name
        self.set_status("Idle", force=True)
        super().__init__()
    def set_status(self, status, error=False, force=False):
        """Set the status of this object in the process meter"""
        def do_status(abort_condition, queue, error, status, delay=0):
            try:
                # Wait for abort signal
                abort_condition.wait(delay)
                # Abort signal received, leave
                abort_condition.release()
                return
            except RuntimeError:
                # Never got abort signal, go ahead setting the status
                queue.put(PipeStatus(
                    error,
                    self.display_name,
                    status,
                    self._status_recvd,
                    self._status_sent,
                    self._timer.total
                ))
                return
        with self._setting_threads_abort_condition:
            self._setting_threads_abort_condition.notify_all()
            for t in self._setting_threads:
                t.join()
            self._setting_threads.clear()
            delay = 0 if force else self._status_min_delta
            thread = threading.Thread(
                target=do_status,
                args=(
                    self._setting_threads_abort_condition,
                    self.status_queue,
                    error,
                    status,
                    delay
                )
            )
            thread.start()
            self._setting_threads.add(thread)
    def added(self) -> None:
        return
    def setup(self) -> None:
        return
    def _processing_string(self) -> str:
        return "Processing"
    def run(self) -> None:
        """The main process loop"""
        self._status_recvd = 0
        self._status_sent = 0
        self.set_status("Starting", force=True)
        self.setup()
        setproctitle.setproctitle(f"{self.display_name} [CANDR Analysis]")
        pp = PipePoison()
        try:
            # Forever, or until the pipe is poisoned
            while True:
                self.set_status("Waiting")
                # Receive a value
                input_data = self.pipe_in.recv()
                # Check for poison
                if input_data == pp:
                    raise EOFError
                self._status_recvd += 1
                # Create a generator for this input
                gen = self.process(input_data)
                try:
                    # This could be more simply written as
                    #
                    # [self.pipe_out.send(x) for x in gen]
                    #
                    # but we want to set statuses between, so we loop
                    # indefinitely until we find StopIteration
                    while True:
                        # Getting the next value in the generator is a process
                        self.set_status(self._processing_string())
                        self._timer.start()
                        x = next(gen)
                        self._timer.stop()
                        # Trying to send that value into the pipe is sending
                        # Will get stuck here if processes further down the pipe
                        # are blocking
                        self.set_status("Sending")
                        self.pipe_out.send(x)
                        self._status_sent += 1
                except StopIteration:
                    pass
                except EOFError as e:
                    raise e
                except Exception as e:
                    self.set_status(traceback.format_exc(), error=True, force=True)
                    self.pipe_out.send(pp)
                    self.pipe_in.close()
                    self.pipe_out.close()
                    return
        except (EOFError, OSError) as e:
            self.set_status("Finalising", force=True)
            gen = self.finalise()
            try:
                while True:
                    self.set_status("Finalising: " + self._processing_string())
                    self._timer.start()
                    x = next(gen)
                    self._timer.stop()
                    self.set_status("Finalising: sending")
                    self.pipe_out.send(x)
                    self._status_sent += 1
            except StopIteration:
                pass
            except EOFError as e:
                pass
            except Exception as e:
                self.set_status(traceback.format_exc(), error=True, force=True)
                self.pipe_out.send(pp)
                self.pipe_in.close()
                self.pipe_out.close()
                return
            # Repeat the poison down the pipe
            self.pipe_out.send(pp)
            # Pipe has been poisoned, or something else sent EOFError
            self.set_status("Ending", force=True)
            return
    def finalise(self):
        yield from []
    def process(self, _) -> bool:
        """Placeholder process, does nothing"""
        return False

class Pipe:
    """A pipeline between generators"""
    def __init__(self, a: PipeObject, b: PipeObject):
        self.a, self.b = multiprocessing.Pipe()
        a.pipe_out = self.a
        b.pipe_in = self.b
    def close(self) -> None:
        """Close the constituent members of this pipe"""
        self.a.close()
        self.b.close()

class PipePrinter(PipeObject):
    """A PipeObject that prints whatever it receives to a function"""
    def __init__(self, print_fun = print, **kwargs):
        self._print_fun = print_fun
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        """Convert input to printed output"""
        self._print_fun(input_data)
        yield input_data

class RateLimit(PipeObject):
    """A PipeObject that just returns its input straight through"""
    def __init__(self, sleep = 1, **kwargs):
        self._sleep = sleep
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        time.sleep(self._sleep)
        yield input_data

class SkipN(PipeObject):
    """A PipeObject that skips n inputs before continuing"""
    def __init__(self, skip = 0, **kwargs):
        self._skip = skip
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        if self._skip > 0:
            self._skip -= 1
            yield from []
        else:
            yield input_data

class Filter(PipeObject):
    """A PipeObject that only lets through objects that pass pred"""
    def __init__(self, pred = lambda _: True, **kwargs):
        self._pred = pred
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        if self._pred(input_data):
            yield input_data
        else:
            yield from []

class Truncate(PipeObject):
    """A PipeObject that only lets through the first n items"""
    def __init__(self, n, **kwargs):
        self._n = n
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        if self._n > 0:
            yield input_data
        else:
            yield from []
        self._n -= 1

class Counter(PipeObject):
    """A PipeObject that simply counts up"""""
    def __init__(self, n = None, **kwargs):
        if n is not None:
            self._gen = range(n)
        else:
            self._gen = itertools.count()
        super().__init__(**kwargs)
    def process(self, _) -> Iterator[int]:
        for i in self._gen:
            yield i
        raise EOFError

class AddN(PipeObject):
    """A PipeObject that simply adds n to what is input"""""
    def __init__(self, n, **kwargs):
        self._n = n
        super().__init__(**kwargs)
    def process(self, input_data: int) -> Iterator[int]:
        result = (input_data + self._n)
        yield result

class Delay(PipeObject):
    """A PipeObject that delays its input"""""
    def __init__(self, delay = 1, **kwargs):
        self._delay = delay
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        counts = int(999 * random.random()) + 1
        this_delay = self._delay / counts
        for i in range(counts):
            percent = i * 100 / counts
            self.set_status(f"Processing ({percent:.2f})")
            time.sleep(this_delay)
        yield input_data

class Cache(PipeObject):
    """A PipeObject that stores what it receives until finalise() is called"""
    def __init__(self, **kwargs):
        self._store = Queue()
        super().__init__(**kwargs)
    def process(self, input_data: Any) -> Iterator[Any]:
        self._store.put(input_data)
        yield from []
    def finalise(self):
        try:
            while True:
                yield self._store.get_nowait()
        except Empty:
            pass

class Multiplexer(PipeObject):
    """A PipeObject that splits its input among child processes"""
    def __init__(self, children, **kwargs):
        self._shared_data_manager = multiprocessing.Manager()
        self._this_status = None
        self._children = list([[
            child,
            multiprocessing.Pipe(),             # pipe in
            multiprocessing.Pipe(),             # pipe out
            multiprocessing.Queue(),            # status pipe
            self._shared_data_manager.list([]), # current jobs
            self._shared_data_manager.Lock(),   # lock for this list
            None                                # current status
        ] for child in children])
        self._out_pipe = multiprocessing.Queue()
        self._in_pipe = multiprocessing.Queue()
        self._time_since_last_status = 0
        self._status_min_delta = 1
        self._stats_pipe = multiprocessing.Queue()
        self._out_len_int = multiprocessing.Value('i', 0)
        self._job_i = 0
        for child in self._children:
            child[0].pipe_in = child[1][1]
            child[0].pipe_out = child[2][0]
            child[0].status_queue = child[3]
        for child in self._children:
            child[0].start()
        super().__init__(**kwargs)
    def join(self):
        super().join()
        for child in self._children:
            child[0].join()
            child[1][0].close()
            child[1][1].close()
            child[2][0].close()
            child[2][1].close()
            child[3].close()

    def setup(self):
        def _in_get(pipe_in, children):
            setproctitle.setproctitle(f"Multiplexer (in get) [CANDR Analysis]")
            pp = PipePoison()
            try:
                for i in itertools.count():
                    x = pipe_in.get()
                    if x == pp:
                        for k, j in enumerate(range(i, i + len(children))):
                            with children[k][5]:
                                if pp not in children[k][4]:
                                    children[k][1][0].send(pp)
                                    children[k][4].append(j)
                        raise ValueError
                    def get_child_job_len(child):
                        ret = None
                        acq = child[5].acquire(True, 1)
                        if acq:
                            ret = len(child[4])
                            child[5].release()
                        else:
                            print("_in_get failed to acquire lock in time")
                        return ret
                    lens = [(c, get_child_job_len(c)) for c in children]
                    filtered_lens = [l for l in lens if l[1] is not None]
                    child, job_len = min(filtered_lens, key=lambda c: c[1])
                    child[1][0].send(x)
                    with child[5]:
                        child[4].append(i)
            except ValueError:
                return
        def _out_get(pipe_out, children, out_len_int):
            setproctitle.setproctitle(f"Multiplexer (out get) [CANDR Analysis]")
            pp = PipePoison()
            out_queue = []
            children_done = []
            job_idx = 0
            def get_child_job_len(child):
                ret = None
                acq = child[5].acquire(True, 1)
                if acq:
                    ret = len(child[4])
                    child[5].release()
                else:
                    print("_out_get failed to acquire lock in time")
                return ret
            while True:
                got_one = True
                while got_one:
                    got_one = False
                    if len(children_done) == len(children):
                        pipe_out.put(pp)
                        return
                    each_timeout = 1 / (len(children) - len(children_done))
                    missed_locks = []
                    for i, child in enumerate(children):
                        if i in children_done:
                            continue
                        job_len = get_child_job_len(child)
                        if job_len is None:
                            missed_locks.append(i)
                            continue
                        if job_len == 0:
                            continue
                        if not child[2][1].poll(each_timeout):
                            continue
                        try:
                            with child[5]:
                                x = child[2][1].recv()
                                job_no = child[4].pop(0)
                            if x == pp:
                                raise EOFError
                            got_one = True
                            out_queue.append((job_no, x))
                        except EOFError:
                            children_done.append(i)
                            continue
                    if len(missed_locks) > 0:
                        children_not_done = sorted(list(set(range(len(children))) - set(children_done)))
                        missed_locks.sort()
                        print(f"Missed locks: {missed_locks}/{children_not_done}")
                    out_queue.sort(key=lambda a: a[0])
                    while len(out_queue) > 0 and out_queue[0][0] == job_idx:
                        next_job = out_queue.pop(0)
                        pipe_out.put(next_job[1])
                        job_idx += 1
                out_len_int.value = len(out_queue)
                time.sleep(1)
        def _status_update(status_pipe, children, self_stats_pipe, out_len_int):
            setproctitle.setproctitle(f"Multiplexer (status update) [CANDR Analysis]")
            stats = None
            pp = PipePoison()
            children_done = []
            flush = len(children) * 2
            while True:
                got = True
                rec = 0
                while got and rec < flush:
                    got = False
                    n_children_remaining = len(children) - len(children_done)
                    flush = n_children_remaining * 2
                    each_timeout = 1
                    if n_children_remaining > 0:
                        each_timeout = 1 / n_children_remaining
                    for child in children:
                        if child in children_done:
                            continue
                        try:
                            child[6] = child[3].get(block=True, timeout=each_timeout)
                            if child[6] == pp:
                                children_done.append(child)
                                got = True
                                continue
                            rec += 1
                            got = True
                        except Empty:
                            continue
                if rec == 0:
                    if len(children_done) == len(children):
                        return
                    time.sleep(1)
                    continue
                str_statuses = ''
                for child in children:
                    if child[6] is None or len(child[6]) == 0:
                        str_statuses += "\nNone"
                        continue
                    if child[6][0]: # is error
                        status_pipe.put(child[6])
                        return
                    str_statuses += "\n" + child[6][1]
                    try:
                        if len(child[4]) > 0:
                            str_statuses += f" (#{child[4][0]} & {len(child[4]) - 1} others)"
                    except IndexError: # already been grabbed
                        pass
                    str_statuses += f" | {child[6][2]} | {child[6][3]}:{child[6][4]}"
                try:
                    while True:
                        stats = self_stats_pipe.get_nowait()
                except Empty:
                    pass
                if stats is None:
                    time.sleep(1)
                    continue
                if stats[4]:
                    if stats[4][0]:
                        status_pipe.put(stats[4])
                        return
                    str_statuses = f"{stats[4][1]} | {stats[4][2]} | {stats[4][3]}:{stats[4][4]} ({out_len_int.value} finished)" + str_statuses
                final_status = PipeStatus(
                    False,
                    stats[0],
                    str_statuses.strip(),
                    stats[1],
                    stats[2],
                    stats[3]
                )
                status_pipe.put(final_status)
        self._in_getter = multiprocessing.Process(
            target=_in_get,
            args=(self._in_pipe, self._children)
        )
        self._out_getter = multiprocessing.Process(
            target=_out_get,
            args=(self._out_pipe, self._children, self._out_len_int)
        )
        self._status_updater = multiprocessing.Process(
            target=_status_update,
            args=(self.status_queue, self._children, self._stats_pipe, self._out_len_int)
        )
        self._in_getter.start()
        self._out_getter.start()
        self._status_updater.start()
    def set_status(self, status, error=False, force=False):
        now = time.time()
        delta = now - self._time_since_last_status
        if force or delta >= self._status_min_delta:
            self._this_status = PipeStatus(
                error,
                self.display_name,
                status,
                self._status_recvd,
                self._status_sent
            )
            self._stats_pipe.put((
                self.display_name,
                self._status_recvd,
                self._status_sent,
                self._timer.total,
                self._this_status
            ))
            self._time_since_last_status = now
    def process(self, input_data: Any) -> Iterator[Any]:
        self._job_i += 1
        self._in_pipe.put(input_data)
        self._stats_pipe.put((
            self.display_name,
            self._status_recvd,
            self._status_sent,
            self._timer.total,
            self._this_status
        ))
        pp = PipePoison()
        try:
            while True:
                try:
                    x = self._out_pipe.get_nowait()
                    if x == pp:
                        raise ValueError
                    yield x
                except Empty:
                    break
        except ValueError:
            pass
    def finalise(self):
        self._stats_pipe.put((
            self.display_name,
            self._status_recvd,
            self._status_sent,
            self._timer.total,
            self._this_status
        ))
        pp = PipePoison()
        self._in_pipe.put(pp)
        while True:
            try:
                x = self._out_pipe.get_nowait()
                if x == pp:
                    raise ValueError
                yield x
            except Empty:
                time.sleep(1)
                continue
            except ValueError:
                break
        self._in_getter.join()
        self._out_getter.join()
        for child in self._children:
            child[3].put(pp)
        self._status_updater.join()

class Pipeline:
    """Class for managing Pipes and PipeObjects"""
    def __init__(self, progress = CLIProgress()):
        self._processes = deque()
        self._pipes = deque()
        self._meter = progress.process_meter()
    def add(self, process: PipeObject) -> None:
        """Adds a PipeObject to the pipeline"""
        process.status_queue = self._meter.queue
        if self._processes:
            last = self._processes.pop()
            self._pipes.append(Pipe(last, process))
            self._processes.append(last)
        self._processes.append(process)
        self._meter.add_process(process.display_name, "Starting")
        process.added()
    def start(self) -> None:
        """Start all the processes in the pipeline, from back to front to
        prevent congestion"""
        for item in reversed(self._processes):
            item.start()
        self._meter.start()
    def stop(self) -> None:
        """Stop all the processes in the pipeline"""
        for item in self._processes:
            item.join()
        for pipe in self._pipes:
            pipe.close()
        self._meter.join()
