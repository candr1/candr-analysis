"""Defines progress reporting routines"""

import multiprocessing
import time
import datetime
from queue import Empty
from typing import Any
import humanize
from tqdm import tqdm
import cli_ui
from .pipetuples import PipePoison

class ProcessMeter(multiprocessing.Process):
    """A meter that displays current PipeObject statuses"""
    def __init__(self, name, update = 1):
        self._data = {}
        self._display_name = name
        self.update = update
        # Put status items into this queue
        self.queue = multiprocessing.Queue()
        super().__init__()
    def add_process(self, name: str, status: str):
        """Add a process to the queue before running"""
        self._data[name] = {
            "status": status,
            "recvd": 0,
            "sent": 0,
            "elapsed": datetime.timedelta(0)
        }
    def join(self):
        self.queue.put(PipePoison())
        super().join()
    def run(self):
        """Begin the meter"""
        # Forever...
        last = time.time()
        while True:
            # Exhaust items on the queue and update internal state
            pp = PipePoison()
            try:
                while True:
                    item = self.queue.get(block=False)
                    if item == pp:
                        return
                    if item.error:
                        cli_ui.error(f'[{item.display_name}] {item.status} ({item.recvd}/{item.sent})')
                        try:
                            while True:
                                _ = self.queue.get(block=False)
                        except Empty:
                            pass
                        return
                    self._data[item.display_name] = {
                        "status": item.status,
                        "recvd": item.recvd,
                        "sent": item.sent,
                        "elapsed": item.elapsed
                    }
            except Empty:
                # No more items on the queue
                pass
            # Create the table that displays the status
            table_data = [
                [
                    (cli_ui.bold, k),
                    (cli_ui.reset, v['status']),
                    (cli_ui.reset, v['recvd']),
                    (cli_ui.faint, "→"),
                    (cli_ui.reset, v['sent']),
                    (cli_ui.reset, humanize.naturaldelta(v['elapsed']))
                ] for k, v in self._data.items()
            ]
            headers = ["Name", "Status", "Recvd", "", "Sent", "Elapsed"]
            now = time.time()
            delta = now - last
            cli_ui.info_section(self._display_name + " (" + humanize.naturaldelta(delta) + ")")
            cli_ui.info_table(table_data, headers=headers)
            # Wait until the next update cycle
            time.sleep(self.update)
            last = now

class CLIProgress:
    """Default progress report for CLI"""
    def __init__(self, name = None):
        self._name = str(name)
    def info_1(self, *tokens: Any, **kwargs: Any):
        """Print an important informative message"""
        return cli_ui.info_1('[' + self._name + ']', *tokens, **kwargs)
    def info_2(self, *tokens: Any, **kwargs: Any):
        """Print a not so important informative message"""
        return cli_ui.info_2('[' + self._name + ']', *tokens, **kwargs)
    def info_3(self, *tokens: Any, **kwargs: Any):
        """Print an even less important informative message"""
        return cli_ui.info_3('[' + self._name + ']', *tokens, **kwargs)
    def error(self, *tokens: Any, **kwargs: Any):
        """Print an error message"""
        return cli_ui.error('[' + self._name + ']', *tokens, **kwargs)
    def warning(self, *tokens: Any, **kwargs: Any):
        """Print an warning message"""
        return cli_ui.warning('[' + self._name + ']', *tokens, **kwargs)
    def bar(self, *args: Any, **kwargs: Any):
        """Return a progress bar item"""
        return tqdm(*args, **kwargs)
    def process_meter(self, *args: Any, **kwargs: Any):
        """Return a process status meter"""
        return ProcessMeter(self._name, *args, **kwargs)
