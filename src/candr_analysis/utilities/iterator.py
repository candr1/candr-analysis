from typing import Any, Iterator, Union, List
from ..utilities.pipeline import PipeObject
from ..utilities.progress import CLIProgress

"""Converting iterator to pipe"""

class IteratorLoader(PipeObject):
    def __init__(self, iterator: Union[List[Any],Iterator[Any]], progress = CLIProgress(), **kwargs):
        self._iterator = iterator
        self._progress = progress
        super().__init__(**kwargs)
    def __len__(self):
        return len(self._iterator)
    def process(self, _) -> Iterator[Any]:
        for it in self._iterator:
            yield it
        raise EOFError
        return
