from collections import namedtuple

"""PipeStatus is an item added to the status queue"""
PipeStatus = namedtuple('PipeStatus', ['error', 'display_name', 'status', 'recvd', 'sent', 'elapsed'], defaults=(False, None, None, 0, 0, 0))

"""PipePoison is a stop item that tells a pipe to close"""
PipePoison = namedtuple('PipePoison', [])

