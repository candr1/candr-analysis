import pickle
from typing import Any, Iterator, Union
from ..utilities.pipeline import PipeObject
from ..utilities.progress import CLIProgress

"""Loading/saving pickled outputs"""

class PickleSaver(PipeObject):
    """Saves the output of a pipe to a pickle file"""
    def __init__(self, fname: str, flush: bool = False, **kwargs):
        self._file = open(fname, 'ab')
        self._flush = flush
        super().__init__(**kwargs)
    def __del__(self):
        self._file.close()
    def process(self, input_data: Any) -> Iterator[None]:
        """Dumps what is sent to the pickle file"""
        ret = pickle.dump(input_data, self._file, pickle.HIGHEST_PROTOCOL)
        if self._flush:
            self._file.flush()
        yield ret
        return

class PickleLoader(PipeObject):
    def __init__(self, fname: str, progress: Union[CLIProgress, None] = CLIProgress(), **kwargs):
        self._file = open(fname, 'rb')
        self._progress = progress
        self._count = 0
        super().__init__(**kwargs)
    def __del__(self):
        self._file.close()
    def __len__(self):
        return self._count
    def setup(self) -> None:
        if self._progress:
            self._count = 0
            self._progress.info_3("Loading pickle file...")
            try:
                while True:
                    pickle.load(self._file)
                    self._count += 1
            except EOFError:
                pass
            finally:
                self._progress.info_3("%d records loaded" % self._count)
        self._file.seek(0)
    def process(self, _) -> Iterator[Any]:
        """Get everything from the pickle file"""
        while True:
            try:
                yield pickle.load(self._file)
            except EOFError:
                break
        self._file.seek(0)
        raise EOFError

def count_pickle(fname: str) -> int:
    with open(fname, 'rb') as f:
        n = 0
        try:
            while True:
                _ = pickle.load(f)
                n += 1
        except EOFError:
            pass
        return n



