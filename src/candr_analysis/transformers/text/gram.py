import re
from typing import Iterator, List
from ...transcribers.candr.text import TextTranscriptionRecord
from ...utilities.pipeline import PipeObject
from ...utilities.progress import CLIProgress

"""Creates n-grams for a text stream"""

class NGramsTranscriptionRecord(TextTranscriptionRecord):
    pass

class TextNGrams(PipeObject):
    """Transforms a text string into n-grams"""
    def __init__(self, n: int = 2, case: bool = True, split_regex: str = '[^a-zA-Z]+', progress = CLIProgress(), **kwargs):
        self._n = n
        self._progress = progress
        self._split_regex = split_regex
        self._case = lambda a: a
        if not case:
            self._case = lambda a: a.lower()
        super().__init__(**kwargs)
    def _split(self, raw):
        """Splt a transcription into tokens such as words"""
        return re.split(self._split_regex, raw)
    def process(self, input_data: TextTranscriptionRecord) -> Iterator[List[str]]:
        """Convert a transcription into NGrams"""
        return self._process(input_data)
    def _process(self, input_data) -> Iterator[List[str]]:
        """Split a transcription, create NGrams"""
        split = list([self._case(x) for x in filter(lambda a: a != '', self._split(input_data.transcription))])
        ret = list(zip(*[split[i:] for i in range(self._n)]))
        if ret:
            yield NGramsTranscriptionRecord(input_data.subject, input_data.type, ret)
        return

class CharacterNGrams(TextNGrams):
    """Transforms a set of characters into n-grams"""
    def __init__(self, n: int = 2, progress = CLIProgress(), **kwargs):
        super().__init__(n, progress = progress, **kwargs)
    def _split(self, raw):
        """Split a transcription into characters"""
        return [char for char in raw]
    def process(self, input_data: TextTranscriptionRecord) -> Iterator[List[str]]:
        """Convert a transcription into NGrams"""
        return self._process(input_data)
