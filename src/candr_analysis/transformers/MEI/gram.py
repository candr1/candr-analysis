from typing import Iterator
from collections import namedtuple, UserList
from collections.abc import Hashable, MutableSet
from itertools import count, tee, chain
from frozendict import frozendict
from ...utilities.pipeline import PipeObject
from ...utilities.progress import CLIProgress
from ...transcribers.candr.MEI import MEITranscriptionRecord
from ...parsers.MEI import MEIParsedRecord, MEIElementFactory
from ...analysis.graph.graph_record import MEIGraphRecord

"""Creates n-grams for an MEI stream"""

MEINGramRecord = namedtuple('MEINGramRecord', MEITranscriptionRecord._fields + ('n',))

MEINGramBreadcrumbs = namedtuple('MEINGramBreadcrumbs', ['breadcrumbs', 'synch'])

class MEINGramNode:
    def __init__(self, hashable, graph):
        if not isinstance(hashable, Hashable):
            raise TypeError(f"Passed parameter {hashable} is not hashable")
        self._graph = graph
        self.hashable = hashable
    def __hash__(self):
        return hash((self.hashable, self._graph))
    def __eq__(self, other):
        return self.__hash__() == hash(other)
    def next_sequential(self):
        return MEINGramSynch(
            [MEINGramNode(v, self._graph) for u, v, k, d in filter(
                (lambda x: x[3]['mode'] == 'sequential'),
                self._graph.edges(self.hashable, True, True)
            )],
            self._graph
        )
    def next_synchronous(self, visited=set()):
        if self.hashable in visited:
            return None
        visited.add(self)
        def next_fun(instance):
            nonlocal visited
            if len(instance) < 2:
                return None
            last = instance[-1]
            next_synch = last.next_synchronous(visited=visited)
            if next_synch:
                return MEINGramNodelist(next_synch[0][1:], next_fun=next_fun)
            return None
        breadcrumbs = MEINGramNodelist([self], next_fun=next_fun)
        head = self.next_sequential()
        has_seq_out = {u for u, v, k, d in filter(
            (lambda x: x[3]['mode'] == 'sequential'), 
            self._graph.edges(None, True, True)
        )}
        while len(head) > 0:
            hashables = [n.hashable for n in head]
            breadcrumbs += MEINGramNodelist(head)
            # A "next node" is one that either has synchronous connections or
            # no outward sequential connections
            next_nodes = [MEINGramNode(v, self._graph) for u, v, k, d in filter(
                (lambda x: x[3]['mode'] == 'synchronous'),
                self._graph.edges(hashables, True, True)
            )]
            next_nodes += [MEINGramNode(hashable, self._graph)
                for hashable in hashables if hashable not in has_seq_out
            ]
            if len(next_nodes) > 0:
                synchs = MEINGramSynch(next_nodes, self._graph)
                return MEINGramBreadcrumbs(breadcrumbs, synchs.complete_synch())
            seqs = [(h, h.next_sequential()) for h in head]
            head = MEINGramSynch([], self._graph)
            for s in seqs:
                for item in s[1]:
                    head.add(item)
        return None

class MEINGramNodelist(UserList):
    def __init__(self, l, next_fun=lambda x: None):
        self.next_fun = next_fun
        self._next = None
        super().__init__(l)
    def create_view(self, n):
        view = self.data[:n]
        if len(view) < n:
            if (not self._next) and self.next_fun:
                self._next = self.next_fun(self)
            if self._next:
                view += self._next.create_view(n - len(view))
        return view
    def eternal_iterator(self):
        for i in count():
            if len(self.data) < (i + 1):
                if (not self._next) and self.next_fun:
                    self._next = self.next_fun(self)
                if self._next:
                    next_it = self._next.eternal_iterator()
                    for it in next_it:
                        yield it
                    return
                else:
                    break
            else:
                yield (self.data[i], self)
        return
    def ratioed_eternal_iterator(self, ratio, idx=0):
        return RatioedGeneratorWrapper(self.eternal_iterator(), ratio=ratio)

class RatioedGeneratorWrapper:
    def __init__(self, gen, ratio=lambda _: 1, rounding=round):
        self.gen = gen
        self.ratio = ratio
        self.__i = 0
        self.__rounding = rounding
        self.__last_ratioed_i = 0
        self.__last_value = None
    def __iter__(self):
        if self.gen:
            try:
                self.__last_value = next(self.gen)
            except StopIteration:
                pass
        return self
    def __next__(self):
        ratioed_i = int(self.__rounding(self.__i * self.ratio(self.__last_value)))
        for i in range(ratioed_i - self.__last_ratioed_i):
            self.__last_value = next(self.gen)
        self.__last_ratioed_i = ratioed_i
        self.__i += 1
        if self.__last_value is None:
            raise StopIteration
        return self.__last_value
    def last_value(self):
        return self.__last_value

class MEINGramBreadcrumbTrail(UserList):
    def __init__(self, l, graph):
        self._graph = graph
        super().__init__(l)
    def grams(self, n):
        # self.data contains Breadcrumbs, being:
        # [
        #   (MEINGramNodelist, MEINGramSynch),
        #   (MEINGramNodelist, MEINGramSynch),
        #   (MEINGramNodelist, MEINGramSynch)
        #   ... etc ...
        # ]
        def tee_iterator(it, n):
            teed_list = list(tee(it, n))
            for i, teed in enumerate(teed_list):
                try:
                    _ = [next(teed) for j in range(i)]
                except StopIteration:
                    pass
            return teed_list
        iterators = None
        last_values = {}
        # Returns a closure callback for calculating ratio. This also inserts an
        # 'idx' value used for referencing the 'last_values' dict
        def idx_ratio(idx):
            # Given an item, returns its ratio
            def ratio(item):
                nonlocal idx
                nonlocal last_values
                if not item:
                    return 1
                # Store this item's len in the dict
                itemlen = len(item[1])
                last_values[idx] = itemlen
                longest = max(last_values.values(), default=0)
                if longest <= 0:
                    return 1
                return itemlen / longest
            return ratio
        iterators = list(
            # Tee each iterator
            [tee_iterator(
                # That being a ratioed iterator of the nodelists of the
                # breadcrumbs
                bc[0].ratioed_eternal_iterator(
                    # With the idx_ratio callback for calculating ratio
                    idx_ratio(i)
                ),
                # One for each n in ngrams
                n
            ) for i, bc in enumerate(self.data) if bc]
        )
        # Each eternal_iterator yields:
        # [(node, current_nodelist_len), (node, current_nodelist_len), ... ]
        #
        # Then teed to be:
        # [
        #   [
        #     [(node1, cnl), (node2, cnl), (node3, cnl), ...],
        #     [(node2, cnl), (node3, cnl), (node4, cnl), ...],
        #     ... * n
        #   ],
        #   [
        #     ... other iterator ...
        #   ]
        # ]
        while True:
            ngram = MEINGramSynch([], self._graph)
            n_voices = 0
            for v, voice_iterator in enumerate(iterators):
                n_voices = v + 1
                for g, gram_iterator in enumerate(voice_iterator):
                    try:
                        item = next(gram_iterator)
                        if not item:
                            continue
                        ngram.add(item[0])
                    except StopIteration:
                        pass
            if len(ngram) == 0:
                break
            yield ngram
        return

class MEINGramSynch(Hashable, MutableSet):
    __hash__ = MutableSet._hash
    def __init__(self, s, graph):
        self._graph = graph
        self.data = set(s)
    def __contains__(self, value):
        return value in self.data
    def __iter__(self):
        return iter(self.data)
    def __len__(self):
        return len(self.data)
    def __repr__(self):
        return repr(self.data)
    def add(self, item):
        self.data.add(item)
    def discard(self, item):
        self.data.discard(item)
    def complete_synch(self):
        hashables = [n.hashable for n in self.data]
        for u, v, k, d in filter(lambda x: x[3]['mode'] == 'synchronous', self._graph.edges(hashables, True, True)):
            if v not in hashables:
                self.add(MEINGramNode(v, self._graph))
                self.complete_synch()
        return self
    def graph_nodes(self):
        return [n.hashable for n in self.data]
    def copy(self):
        return MEINGramSynch(self.data.copy(), self._graph)
    def union(self, other):
        self.data.union(other)
        return self
class MEINGramGraph:
    def __init__(self, graph):
        self._graph = graph
    def start_nodes(self):
        # Create a set of all nodes that have a sequential connection coming in
        has_seq_in = set()
        for u, v, k, d in filter(lambda x: x[3]['mode'] == 'sequential', self._graph.edges(None, True, True)):
            has_seq_in.add(v)
        # For every node that does not have a sequential in (i.e. start nodes)
        return MEINGramNodelist(
            [MEINGramNode(x, self._graph) for x in filter(
                (lambda x: x not in has_seq_in),
                self._graph.nodes()
            )]
        )
    def end_nodes(self):
        # Create a set of all nodes that have a sequential connection going out
        has_seq_out = set()
        for u, v, k, d in filter(lambda x: x[3]['mode'] == 'sequential', self._graph.edges(None, True, True)):
            has_seq_out.add(u)
        # For every node that does not have a sequential in (i.e. start nodes)
        return MEINGramNodelist(
            [MEINGramNode(x, self._graph) for x in filter(
                (lambda x: x not in has_seq_out),
                self._graph.nodes()
            )]
        )
    def subgraph(self, ngram):
        return self._graph.subgraph(set(ngram)).copy()

class MEINGrams(PipeObject):
    """Transforms an MEI stream in n-grams"""
    def __init__(self, n: int = 2, progress = CLIProgress(), **kwargs):
        self._n = n
        self._progress = progress
        super().__init__(**kwargs)
    def _walk_seq(self, graph, node):
        looped = False
        for u, v, k, d in filter(
            (lambda x: x[3]['mode'] == 'sequential'),
            graph.edges(node, True, True)
        ):
            looped = True
            gen = self._walk_seq(graph, v)
            yielded = False
            for yieldy in gen:
                yielded = True
                yield [u] + yieldy
            if not yielded:
                yield [u]
        if not looped:
            yield [node]
    def setup(self):
        self._prev_input = dict()
    def _set_processing_status(self, status):
        self.set_status("Processing: " + status)
    def process(self, input_data: MEIParsedRecord) -> Iterator[MEIGraphRecord]:
        """Convert an MEI transcription into n-grams"""
        subj = input_data.subject
        type_ = input_data.type
        prevhash = (subj, type_,)
        self._set_processing_status("Parsing graph")
        graph = MEINGramGraph(input_data.transcription)
        self._set_processing_status("Finding start")
        start_nodes = MEINGramNodelist(
            [MEINGramNode(x[0], graph._graph) if len(x) > 0 else None for x in input_data.routes.values()]
        )
        # returns a list of breadcrumbs to the next synch
        self._set_processing_status("Finding next synchs")
        ll = []
        for i, n in enumerate(start_nodes):
            self._set_processing_status("Finding next synchs " + str(i) + "/" + str(len(start_nodes)))
            ll.append(n.next_synchronous())
        breadcrumb_trail = MEINGramBreadcrumbTrail(ll, graph._graph)
        # dict of sets of head_nodes -> links
        links = dict()
        # dict of sets of head_nodes -> representations
        head_node_lookup = dict()
        # Creates ngrams
        self._set_processing_status("Making NGrams")
        for n_ngram, ngram in enumerate(breadcrumb_trail.grams(self._n)):
            self._set_processing_status("NGram " + str(n_ngram))
            # Make a subgraph of all the nodes in that ngram
            subgraph = graph.subgraph(ngram.graph_nodes())
            # Find all the nodes that have a sequential link in
            has_seq_in = {v for u, v, k, d in filter(
                (lambda x: x[3]['mode'] == 'sequential'),
                subgraph.in_edges(None, True, True)
            )}
            # start_nodes are nodes that don't have a sequential link in
            start_nodes = [x for x in subgraph.nodes() if x not in has_seq_in]
            # Find all the sequential walks for every start node
            voices = list(
                chain.from_iterable(
                    [self._walk_seq(subgraph, s) for s in start_nodes]
                )
            )
            nodes = set()
            for voice in voices:
                # Create a hashable representation of that voice
                voice_representation = tuple(
                    [MEIElementFactory.strip_uniqid(node) for node in voice]
                )
                head_node = voice_representation[0]
                if head_node not in links:
                    links[head_node] = set()
                if head_node not in head_node_lookup:
                    head_node_lookup[head_node] = set()
                links[head_node] |= {tuple([
                        MEIElementFactory.strip_uniqid(v),
                        frozendict(d)
                    ]) for u, v, k, d in filter(
                        (lambda x: x[3]['mode'] != 'sequential'),
                        graph._graph.edges(voice[0], True, True)
                )}
                head_node_lookup[head_node].add(voice_representation)
                nodes.add(voice_representation)
            if prevhash in self._prev_input:
                prev_input = self._prev_input[prevhash]
                for v in nodes:
                    for u in prev_input:
                        yield MEIGraphRecord(
                            'nextgram',
                            u,
                            v,
                            subj,
                            type_,
                            1
                        )
            self._prev_input[prevhash] = frozenset(nodes) 
        for i, (head_node, link_set) in enumerate(links.items()):
            self._set_processing_status("Linking " + str(i) + "/" + str(len(links)))
            for link in link_set:
                v = link[0]
                d = link[1]
                if v in head_node_lookup:
                    true_vs = head_node_lookup[v]
                    true_us = head_node_lookup[head_node]
                    for true_u in true_us:
                        for true_v in true_vs:
                            yield MEIGraphRecord(
                                d['mode'],
                                true_u,
                                true_v,
                                subj,
                                type_,
                                d['weight']
                            )
