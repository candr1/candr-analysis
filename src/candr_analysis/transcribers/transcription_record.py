from collections import namedtuple

"""A TranscriptionRecord is an item that contains a transcription of a subject
of a particular type"""

TranscriptionRecord = namedtuple('TranscriptionRecord', ['subject', 'type', 'transcription'])
