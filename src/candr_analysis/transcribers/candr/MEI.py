import requests
import rdflib
from typing import Iterator
from ..transcription_record import TranscriptionRecord
from ...utilities.pipeline import PipeObject
from ...utilities.progress import CLIProgress

"""Transcribes text from a items"""

class MEITranscriptionRecord(TranscriptionRecord):
    pass

class Setting(PipeObject):
    """CANDR setting MEI transcriber"""
    def __init__(self, progress = CLIProgress(), namespace: str = 'https://candr.org.uk/', **kwargs):
        self._candr_ns = rdflib.Namespace(namespace)
        self._progress = progress
        super().__init__(**kwargs)
    def process(self, input_data: rdflib.graph.Graph) -> Iterator[MEITranscriptionRecord]:
        """Convert an RDF graph of settings into MEI transcriptions using the
        API"""
        def make_object_iter():
            return input_data.subject_objects(self._candr_ns.MEITranscription)
        n_objects = sum(1 for x in make_object_iter())
        my_iter = make_object_iter()
        if n_objects > 1:
            my_iter = self._progress.bar(make_object_iter, total=n_objects, desc="Transcribing MEI")
        for sub, obj in my_iter:
            uri = str(obj)
            r = requests.get(uri, allow_redirects=True)
            yield MEITranscriptionRecord(str(sub), 'setting', r.content.decode())


        
