from xml.etree import ElementTree

"""Validates an MEI tree"""

class MEIValidator:
    """Validates an MEI tree"""
    def __init__(self, error_log = print):
        self._error_log = error_log
        self._ns = {
            'mei': 'http://www.music-encoding.org/ns/mei',
            'xml': 'http://www.w3.org/XML/1998/namespace'
        }
    def validate_tree(self, tree: ElementTree.Element):
        """Validate an MEI tree"""
        try:
            # root is mei
            assert tree.tag == f'{{{self._ns["mei"]}}}mei', "Root is not MEI"
            # has music element
            music = tree.find('mei:music', self._ns)
            assert music, "Cannot find music element"
            # has body element
            body = music.find('mei:body', self._ns)
            assert body, "Cannot find body element"
            for mdiv in body.findall('mei:mdiv', self._ns):
                for section in mdiv.findall('mei:section', self._ns):
                    for stave in section.findall('mei:staff', self._ns):
                        for layer in stave.findall('mei:layer', self._ns):
                            assert self._validate_layer(layer), "Layer is invalid"
        except AssertionError as e:
            self._error_log(e)
            return False
        else:
            return True
    def __str_is_int(self, string: str):
        """Test that a string only contains integer content"""
        return string.isdigit()
    def __str_is_positive_float(self, string: str):
        """Test that a string is a positive float"""
        return string.replace('.', '', 1).isdigit()
    def _validate_layer(self, layer: ElementTree.Element):
        """Validate an mei:layer"""
        ns_clf = f'{{{self._ns["mei"]}}}clef'
        ns_not = f'{{{self._ns["mei"]}}}note'
        ns_lig = f'{{{self._ns["mei"]}}}ligature'
        ns_div = f'{{{self._ns["mei"]}}}divisione'
        ns_syl = f'{{{self._ns["mei"]}}}syl'
        ns_acd = f'{{{self._ns["mei"]}}}accid'
        ns_sup = f'{{{self._ns["mei"]}}}supplied'
        funs = {
            ns_clf: self._validate_clef,
            ns_not: self._validate_note,
            ns_lig: self._validate_ligature,
            ns_div: self._validate_divisione,
            ns_syl: self._validate_syllable,
            ns_acd: self._validate_accidental,
            ns_sup: self._validate_supplied
        }
        for item in layer.findall('*'):
            try:
                assert funs[item.tag](item), "Element is invalid"
            except KeyError as e:
                raise AssertionError("Cannot find element: " + str(e))
                return False
        return True
    def _validate_supplied(self, supplied: ElementTree.Element):
        """Validate an mei:supplied"""
        return self._validate_layer(supplied)
    def _validate_clef(self, clef: ElementTree.Element):
        """Validate an mei:clef"""
        shape = clef.get(f'{{{self._ns["mei"]}}}shape')
        assert shape, "No shape to clef"
        assert shape in ["C", "D", "F"], "Shape is not C, D, or F"
        line = clef.get(f'{{{self._ns["mei"]}}}line')
        assert line, "No line to clef"
        assert self.__str_is_int(line), "Line in clef is not an integer"
        assert 0 <= int(line) <= 10, "Line in clef is not [0, 10]"
        return True
    def _validate_note(self, note: ElementTree.Element):
        """Validate an mei:note"""
        pname = note.get(f'{{{self._ns["mei"]}}}pname')
        assert pname, "No pname to note"
        assert pname in ["a", "b", "c", "d", "e", "f", "g"], "Pname in note is not a, b, c, d, e, f or g"
        oct_ = note.get(f'{{{self._ns["mei"]}}}oct')
        assert oct_, "No oct to note"
        assert self.__str_is_int(oct_), "Oct in note is not an integer"
        assert 1 <= int(oct_) <= 8, "Oct in note is not [1, 8]"
        for plica in note.findall('mei:plica', self._ns):
            assert self._validate_plica(plica)
        return True
    def _validate_plica(self, plica: ElementTree.Element):
        """Validate an mei:plica"""
        dir_ = plica.get(f'{{{self._ns["mei"]}}}dir')
        assert dir_, "No dir to plica"
        assert dir_ in ["up", "down"], "Dir in plica is not up or down"
        return True
    def _validate_accidental(self, accidental: ElementTree.Element):
        """Validate an mei:accid"""
        ploc = accidental.get(f'{{{self._ns["mei"]}}}ploc')
        assert ploc, "No ploc to accidental"
        assert ploc in ["a", "b", "c", "d", "e", "f", "g"], "Ploc in accidental is not a, b, c, d, e, f or g"
        oloc = accidental.get(f'{{{self._ns["mei"]}}}oloc')
        assert oloc, "No oct to accidental"
        assert self.__str_is_int(oloc), "Oct in accidental is not an integer"
        assert 1 <= int(oloc) <= 8, "Oct in accidental is not [1, 8]"
        accid = accidental.get(f'{{{self._ns["mei"]}}}accid')
        assert accid, "No accid to accidental"
        assert accid in ["f", "s", "n"], "Accid in accidental is not f, s or n"
        return True
    def _validate_ligature(self, ligature: ElementTree.Element):
        """Validate an mei:ligature"""
        type_ = ligature.get(f'{{{self._ns["mei"]}}}type')
        assert type_, "No type to ligature"
        assert type_ in ["square", "currentes"], "Ligature is not square or currentes"
        for note in ligature.findall('mei:note', self._ns):
            assert self._validate_note(note), "Note is invalid"
        return True
    def _validate_divisione(self, divisione: ElementTree.Element):
        """Validate an mei:divisione"""
        loc = divisione.get(f'{{{self._ns["mei"]}}}loc')
        assert loc, "No loc to divisione"
        assert self.__str_is_int(loc), "Loc in divisione is not integer"
        len_ = divisione.get(f'{{{self._ns["mei"]}}}len')
        assert len_, "No len to divisione"
        assert self.__str_is_positive_float(len_), "Len in divisione is not positive float"
        assert 0 <= float(len_), "Len in divisione is negative"
        return True
    def _validate_syllable(self, syllable: ElementTree.Element):
        """Validate an mei:syl"""
        wordpos = syllable.get(f'{{{self._ns["mei"]}}}wordpos')
        assert wordpos, "No wordpos in syllable"
        assert wordpos in ["i", "m", "s", "t"], "Wordpos in syllable not i, m, s or t"
        assert syllable.findall('*') == [], "Syllable has child elements"
        assert not syllable.text.isspace(), "Syllable is empty"
        return True
