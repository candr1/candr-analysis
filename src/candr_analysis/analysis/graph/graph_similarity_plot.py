"""Generate similarity plots for statistics"""
import time
import traceback
import multiprocessing
from typing import Iterator, Union
from queue import Empty, Queue
import numpy as np
from slugify import slugify
from sqlalchemy.orm import joinedload
from ordered_set import OrderedSet
from collections import defaultdict
from more_itertools import partition
from ...utilities.pipeline import PipeObject, PipeStatus, PipePoison
from .graph_statistics import GraphStatisticRecord
from .graph_analysis import Graph
from . import models, graph_record

class GraphSimilarityPlot(PipeObject):
    def __init__(self, store: str, stats_loader: PipeObject, **kwargs):
        super().__init__(**kwargs)
        self.graph = Graph(store)
        self._stats_loader = stats_loader
        self._stats_loader_status_queue = multiprocessing.Queue()
        self._stats_loader.status_queue = self._stats_loader_status_queue
        self._stats_loader_in_pipe = multiprocessing.Pipe() # send() to [0]
        self._stats_loader_out_pipe = multiprocessing.Pipe() # recv() from [1]
        self._stats_loader.pipe_in = self._stats_loader_in_pipe[1]
        self._stats_loader.pipe_out = self._stats_loader_out_pipe[0]
        self._own_status_queue = multiprocessing.Queue()
        self._to_yield = Queue()
        self._instance_lookup = dict()
        self._n_grams = defaultdict(lambda: [])
        self._node_lookup = defaultdict(lambda: defaultdict(lambda: []))
    def added(self) -> None:
        def _status_watcher(stats_loader_queue, this_queue, own_queue):
            pp = PipePoison()
            stats_loader_status = None
            own_status = None
            while True:
                status_changed = False
                # Get the most up-to-date stats loader status
                while True:
                    try:
                        stats_loader_status = stats_loader_queue.get_nowait()
                        if stats_loader_status == pp:
                            this_queue.put(pp)
                            return
                        if stats_loader_status[0]:
                            this_queue.put(stats_loader_status)
                            this_queue.put(pp)
                            return
                        status_changed = True
                    except Empty:
                        break
                # And the most up-to-date own status
                while True:
                    try:
                        own_status = own_queue.get_nowait()
                        if own_status == pp:
                            this_queue.put(pp)
                            return
                        if own_status[0]:
                            this_queue.put(own_status)
                            this_queue.put(pp)
                    except Empty:
                        break
                    status_changed = True
                if status_changed and own_status is not None:
                    if stats_loader_status is None:
                        this_queue.put(own_status)
                    else:
                        this_queue.put(PipeStatus(
                            False,
                            own_status[1],
                            own_status[2] + " (Stats loader: " + stats_loader_status[2] + ")",
                            own_status[3],
                            own_status[4],
                            own_status[5]
                        ))
                time.sleep(1)
        self._stats_loader_status_watcher = multiprocessing.Process(
            target=_status_watcher,
            args=(
                self._stats_loader_status_queue,
                self.status_queue,
                self._own_status_queue
            )
        )
        self._stats_loader.start()
        self._stats_loader_status_watcher.start()
    def __del__(self):
        self._stats_loader.join()
        self._stats_loader_status_watcher.join()
    def set_status(self, status, error=False):
        try:
            self._own_status_queue.put(PipeStatus(
                error,
                self.display_name,
                status,
                self._status_recvd,
                self._status_sent,
                self._timer.total
            ))
        except AttributeError:
            return super().set_status(status, error)
    def setup(self):
        self.set_status("Loading statistics")
        # Send something to the stats loader (doesn't matter what) to kickstart
        # the process
        self._stats_loader_in_pipe[0].send(None)
        pp = PipePoison()
        try:
            stats_received = 0
            while True:
                self.set_status("Waiting for statistics")
                pipe_data = self._stats_loader_out_pipe[1].recv()
                if pipe_data == pp:
                    raise EOFError
                stats_received += 1
                self.set_status(f"Received {stats_received} statistics")
                # Store the resulting stats for later
                self._to_yield.put(pipe_data)
                gen = self._stats_process(pipe_data)
                try:
                    while True:
                        self.set_status("Processing statistics")
                        _ = next(gen)
                except StopIteration:
                    pass
                except EOFError as e:
                    raise e
                except Exception as e:
                    self.set_status(traceback.format_exc(), error=True)
        except (EOFError, OSError) as e:
            self.set_status("Statistics loaded")
    def process(self, input_data: Union[graph_record.GraphRecord, graph_record.GraphRecordCollection]) -> Iterator[None]:
        # Yield all the stats we can
        try:
            while True:
                yield self._to_yield.get_nowait()
        except Empty:
            pass
        # Create GraphRecordCollection
        if isinstance(input_data, (graph_record.GraphRecord, graph_record.MEIGraphRecord)):
            input_data = graph_record.GraphRecordCollection([input_data])
        for record in input_data.records:
            if record.relationship != 'nextgram':
                continue
            first = record.first
            instance = record.instance
            self._n_grams[instance].append(first)
        yield from []
    def _stats_process(self, input_data: GraphStatisticRecord) -> Iterator[GraphStatisticRecord]:
        if input_data.name != 'Significant scores':
            return
        self.set_status("Processing: Creating lookup tables")
        # {
        #   "1": {
        #     "2": [
        #       (score, p_val),
        #       (score, p_val)
        #     ],
        #     "3": [
        #       ...
        #     ],
        #     ...
        #   },
        #   "2": {
        #     "3": [
        #       ...
        #     ],
        #     ...
        #   },
        #   ...
        # }
        for i, row in enumerate(input_data.values):
            percent = (i+1)*100/len(input_data.values)
            self.set_status(f"Processing: Creating lookup tables {percent:.2f}%")
            nodea = row[0]
            nodeb = row[1]
            score = row[2]
            pval = row[3]
            tup = (score, pval)
            # Append the score both for a -> b and b -> a
            for wayround in [[nodea, nodeb], [nodeb, nodea]]:
                if tup not in self._node_lookup[wayround[0]][wayround[1]]:
                    self._node_lookup[wayround[0]][wayround[1]].append(tup)
        yield from []
    def finalise(self):
        significant_node_ids = list(self._node_lookup.keys())
        current_its = 0
        total_its = sum([len(ngrams) + 1 for ngrams in self._n_grams.values()])
        instance_names = set(self._n_grams.keys())
        for i, (instance, ngrams) in enumerate(self._n_grams.items()):
            percent = current_its * 100 / total_its
            self.set_status(f"Processing: Generating plots {percent:.2f}%")
            results = []
            # results = [
            #   {
            #       0: {
            #           "instance a": (score, p_val),
            #           "instance b": (score, p_val),
            #           "instance c": (score, p_val)
            #       },
            #       1: {
            #           ...
            #       },
            #       ...
            #   },
            #   {
            #       ...
            #   },
            #   ...
            # ]
            instance_names = OrderedSet()
            ngram_set = set(ngrams)
            all_nodes = self.graph._orm.session.query(
                models.Node
            ).options(
                joinedload(models.Node.instance),
                joinedload(models.Node.representations)
            ).filter(
                models.Node.representations.any(
                    models.Representation.representation.in_(ngram_set)
                ),
                models.Node.id.in_(significant_node_ids)
            )
            current_its += 1
            all_other, all_this = map(list, partition(
                lambda n: n.instance.name == instance,
                all_nodes
            ))
            for gram in ngrams:
                percent = current_its * 100 / total_its
                self.set_status(f"Processing: Generating plots {percent:.2f}%")
                # Get all nodes in other instances that are this gram, that are
                # significant
                other_nodes = list([n for n in all_other if any(
                    [r.representation == gram for r in n.representations]
                )])
                if len(other_nodes) == 0:
                    print(f"No other nodes for {gram} in {instance}")
                    current_its += 1
                    results.append({})
                    continue
                this_node = list([n for n in all_this if any(
                    [r.representation == gram for r in n.representations]
                )])
                print(f"{len(this_node)} vs. {len(other_nodes)}")
                unfiltered_result = defaultdict(lambda: [])
                for node in other_nodes:
                    if node.id not in self._node_lookup.keys():
                        continue
                    # Lookup the significant results
                    for this in this_node:
                        if this.id not in self._node_lookup[node.id].keys():
                            continue
                        unfiltered_result[node.instance.name].extend(
                            self._node_lookup[node.id][this.id]
                        )
                # Distill the list down to a mean value
                result = {}
                for instance_name, node_dat in unfiltered_result.items():
                    score = np.mean([x[0] for x in node_dat])
                    p_val = np.mean([x[1] for x in node_dat])
                    result[instance_name] = (score, p_val)
                    instance_names.add(instance_name)
                results.append(result)
                current_its += 1

            self.set_status(f"Processing: Regularising nodes {percent:.2f}%")
            regularised_result = []
            for row in results:
                this_result = []
                for instance in instance_names:
                    if instance not in row.keys():
                        # Give a "zero" score (score = 0, p-val = 1)
                        this_result.extend([0, 1])
                    else:
                        this_result.extend(row[instance])
                regularised_result.append(this_result)
            def header_gen(instances):
                for instance in instances:
                    for _ in range(2):
                        yield instance
            generator = header_gen(instance_names)
            headers = tuple(generator)
            np_result = np.array(regularised_result, dtype='float')
            yield GraphStatisticRecord(
                f"Similarity plot for {slugify(instance)}",
                None,
                headers,
                np_result
            )
