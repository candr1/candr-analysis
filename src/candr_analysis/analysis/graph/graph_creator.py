"""Create analysis graphs from input data"""

from typing import Union, Iterator
from . import graph_analysis, models, graph_record
from ...utilities.pipeline import PipeObject

class GraphCreator(PipeObject):
    """PipeObject that creates graphs of incoming items"""
    def __init__(self, store: str = 'graph.db', logger = None, **kwargs):
        self.graph = graph_analysis.Graph(store, logger)
        self._representation_type = self._get_representation_type('Not specified')
        self._process_stage = ''
        super().__init__(**kwargs)
    def _get_instance(self, instance_str, instance_type: Union[str, models.InstanceType]):
        """An instance is something like a setting or folio or stave"""
        if type(instance_type) != models.InstanceType:
            instance_type = self.graph._get_or_create(
                models.InstanceType,
                name = instance_type
            )
        return self.graph._get_or_create(
            models.Instance,
            name = instance_str,
            type = instance_type
        )
    def _get_link_type(self, link_str):
        """A linkType is something like 'sequential' or 'synchronous': how items
        relate to each other"""
        return self.graph._get_or_create(
            models.LinkType,
            name = link_str
        )
    def _get_representation_type(self, representation_str):
        """A representationType is how that item is represented in Python, such
        as text or MEI"""
        return self.graph._get_or_create(
            models.RepresentationType,
            name = representation_str
        )
    def _processing_string(self) -> str:
        total_time = self._timer.total
        orm_time = self.graph._orm._timer[0].total
        orm_queries = self.graph._orm._timer[1]
        percent = 0 if total_time.seconds == 0 else int(orm_time * 1000 / total_time)
        return "Processing: " + self._process_stage + " (" + str(percent) + "% in DB)"
    def process(self, input_data: Union[graph_record.GraphRecord, graph_record.GraphRecordCollection]) -> Iterator[Union[graph_record.GraphRecord, graph_record.GraphRecordCollection]]:
        """For every record, link the first to the second relating to that
        instance"""
        if isinstance(input_data, (graph_record.GraphRecord, graph_record.MEIGraphRecord)):
            input_data = graph_record.GraphRecordCollection([input_data])
        self._process_stage = 'Getting instances'
        instances = [self._get_instance(i.instance, i.type) for i in input_data.records]
        #instance = self._get_instance(input_data.instance, input_data.type)
        self._process_stage = 'Getting link types'
        link_types = [self._get_link_type(i.relationship) for i in input_data.records]
        #link_type = self._get_link_type(input_data.relationship)
        self._process_stage = 'Adding nodes'
        firsts = self.graph.addmany([(i.first, self._representation_type, instance) for (i, instance) in zip(input_data.records, instances)])
        #first = self.graph.add(input_data.first, self._representation_type, instance)
        seconds = self.graph.addmany([(i.second, self._representation_type, instance) for (i, instance) in zip(input_data.records, instances)])
        #second = self.graph.add(input_data.second, self._representation_type, instance)
        self._process_stage = 'Linking nodes'
        if hasattr(input_data, 'weight'):
            #self.graph.link(first, second, link_type, instance, input_data.weight)
            self.graph.linkmany([(first, second, link_type, instance, i.weight) for (first, second, link_type, instance, i) in zip(firsts, seconds, link_types, instances, input_data)])
        else:
            self.graph.linkmany(zip(firsts, seconds, link_types, instances))
            #self.graph.link(first, second, link_type, instance)
        unrepresented = self.graph.unrepresented_nodes()
        if len(unrepresented) > 0:
            raise Exception(str([str(i) for i in unrepresented]) + " are unrepresented")
        yield input_data
        return

class TextGraphCreator(GraphCreator):
    """A graph creator just for text"""
    def __init__(self, store: str = 'graph.db', logger = None, **kwargs):
        super().__init__(store, logger = None, **kwargs)
        self._representation_type = self._get_representation_type('Text')

class MEIGraphCreator(GraphCreator):
    """A graph creator for MEI"""
    def __init__(self, store: str = 'graph.db', logger = None, **kwargs):
        super().__init__(store, logger, **kwargs)
        self._representation_type = self._get_representation_type('MEI')
