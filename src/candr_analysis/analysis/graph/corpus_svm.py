import random
import os
import zlib
import pickle
import dbm
import sparse
import base64
import math
import sys
import gc
import logging
import numpy as np
import numpy.ma as ma

from dataclasses import dataclass
from typing import Iterator
from itertools import count
from ordered_set import OrderedSet
from collections import deque, namedtuple
from collections_extended import frozenbag
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import IncrementalPCA, PCA
from sklearn.kernel_approximation import Nystroem
from sklearn.linear_model import SGDOneClassSVM
from sklearn.exceptions import NotFittedError

from ...utilities.pipeline import PipeObject
from ...transcribers.transcription_record import TranscriptionRecord
from .graph_statistics import GraphStatisticRecord

logging.basicConfig(
    filename="CorpusSVM.log",
    filemode='a',
    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
    datefmt='%H:%M:%S',
    level=logging.DEBUG
)

class SparseDiskStoreRandomSampler:
    def __init__(self, disk_store):
        self._disk_store = disk_store
        self._disk_store_keys = list(self._disk_store.keys())
    def get(self):
        while True:
            if len(self._disk_store_keys) == 0:
                raise KeyError("SparseDiskStore is empty")
            key_choice = random.choice(self._disk_store_keys)
            val = self._disk_store[key_choice]
            sub_indices = (val.mask[:, 0] != True).nonzero()[0]
            if sub_indices.shape[0] == 0:
                print(f"{key_choice} is fully masked, removing")
                del self._disk_store[key_choice]
                self._disk_store_keys = list(self._disk_store.keys())
                continue
            sub_idx = random.choice(sub_indices)
            break
        return (sub_idx, val[sub_idx])
    def sample(self, n):
        return [self.get() for _ in range(n)]

class OneToOneMapping:
    def __init__(self):
        self._a = {}
        self._b = {}
    def add(self, a, b):
        self._a[a] = b
        self._b[b] = a
    def a_contains(self, a):
        return a in self._a
    def b_contains(self, b):
        return b in self._b
    def iter_a(self):
        return self._a.keys()
    def iter_b(self):
        return self._b.keys()
    def iter_a_to_b(self):
        return self._a.items()
    def iter_b_to_a(self):
        return self._b.items()
    def a_to_b(self, a):
        return self._a[a]
    def b_to_a(self, b):
        return self._b[b]
    def del_a(self, a):
        b = self._a[a]
        del self._b[b]
        del self._a[a]
    def del_b(self, b):
        a = self._b[b]
        del self._b[b]
        del self._a[a]

class HashItemOneToOne(OneToOneMapping):
    def add(self, hash, item):
        return super().add(hash, item)
    def contains_hash(self, hash):
        return super().a_contains(hash)
    def contains_item(self, item):
        return super().b_contains(item)
    def iter_hashes(self):
        return super().iter_a()
    def iter_items(self):
        return super().iter_b()
    def iter_hash_to_items(self):
        return super().iter_a_to_b()
    def iter_items_to_hash(self):
        return super().iter_b_to_a()
    def hash_to_item(self, hash):
        return super().a_to_b(hash)
    def item_to_hash(self, item):
        return super().b_to_a(item)
    def del_hash(self, hash):
        return super().del_a(hash)
    def del_item(self, item):
        return super().del_b(item)

class BDBDict:
    def __init__(self, fname, keys=set([])):
        self._db = dbm.open(fname, flag='c')
        self._fname = fname
        self._keys = keys
    def __hash(self, item):
        return base64.b64encode(
            hash(item).to_bytes(
                int(
                    math.ceil(
                        sys.hash_info.width / 8
                    )
                ),
                byteorder='big',
                signed=True
            )
        )
    def __getitem__(self, key):
        hashed = self.__hash(key)
        try:
            return pickle.loads(zlib.decompress(bytes(self._db[hashed])))
        except KeyError:
            actual_keys = '[' + ', '.join(
                list(
                    [x.decode('ascii') for x in self._db.keys()]
                )
            ) + ']'
            raise KeyError(f"Key: {key}, hash: {hashed}\n{actual_keys}")
    def __setitem__(self, key, value):
        hashed = self.__hash(key)
        self._keys.add(key)
        self._db[hashed] = zlib.compress(
            pickle.dumps(
                value, pickle.HIGHEST_PROTOCOL
            )
        )
    def __len__(self):
        return len(self._db) 
    def __delitem__(self, key):
        hashed = self.__hash(key)
        self._keys.remove(key)
        del self._db[hashed]
    def __contains__(self, key):
        return key in self._keys
    def values(self):
        for k in self._db.keys():
            yield pickle.loads(zlib.decompress(self._db[k]))
    def keys(self):
        yield self._keys
    def items(self):
        to_discard = set()
        for k in self._keys:
            hashed = self.__hash(k)
            try:
                v = pickle.loads(zlib.decompress(self._db[hashed]))
                yield (k, v)
            except KeyError:
                logger = logging.getLogger('CorpusSVM')
                logger.warning(
                    f"Tried to get key {k}, but its hash {hashed} wasn't in " +
                    "the database. Discarding it"
                )
                to_discard.add(k)
        _ = [self._keys.discard(x) for x in to_discard]

    def close(self):
        self._db.close()

class SparseDiskStore:
    def __init__(self, fname, keys=set([])):
        self._dict = BDBDict(fname, keys)
    def __getitem__(self, key):
        return self.__sparse_store_to_masked(self._dict[key])
    def __setitem__(self, key, value):
        self._dict[key] = self.__masked_to_sparse_store(value)
    def __delitem__(self, key):
        del self._dict[key]
    def __iter__(self):
        raise NotImplementedError
    def __contains__(self, item):
        return item in self._dict
    def __len__(self):
        return len(self._dict._keys)
    def keys(self):
        return self._dict._keys
    def values(self):
        return [self.__sparse_store_to_masked(x) for x in self._dict.values()]
    def items(self):
        for key, item in self._dict.items():
            yield (key, self.__sparse_store_to_masked(item))
    def __masked_to_sparse_store(self, masked):
        return (
            sparse.COO(
                ma.getdata(masked)
            ),
            sparse.COO(
                ma.getmaskarray(masked)
            ),
            masked.fill_value
        )
    def __sparse_store_to_masked(self, sparse_store):
        return ma.masked_array(
            sparse_store[0].todense(),
            mask=sparse_store[1].todense(),
            fill_value=sparse_store[2]
        )
    def close(self):
        self._dict.close()

class WeightedFlushDeque(deque):
    def __init__(self, maxweight=None, flush_func=lambda items: None):
        self.maxweight = maxweight
        self.current_weight = 0
        self.__flush_func = flush_func
        super().__init__()
    def _overweight(self, weight):
        return (self.current_weight + weight) > self.maxweight
    def flush(self):
        def popleft_all():
            try:
                while True:
                    yield self.popleft()
            except IndexError:
                return
        iterator = popleft_all()
        ret = self.__flush_func(iterator)
        self.current_weight = 0
        return ret
    def __add_weight(self, weight=1):
        if self._overweight(weight):
            self.flush()
        self.current_weight += weight
        if self.current_weight < 0:
            raise ValueError("Weight < 0")
    def append(self, x, weight=1):
        self.__add_weight(weight)
        super().append((x, weight))
    def appendleft(self, x, weight=1):
        self.__add_weight(weight)
        super().appendleft((x, weight))
    def clear(self):
        self.current_weight = 0
        super().clear()
    def extend(self, _):
        raise NotImplementedError
    def extendleft(self, _):
        raise NotImplementedError
    def insert(self, i, x, weight=1):
        self.__add_weight(weight)
        super().insert(i, (x, weight))
    def pop(self):
        ret, weight = super().pop()
        self.__add_weight(-weight)
        return ret
    def popleft(self):
        ret, weight = super().popleft()
        self.__add_weight(-weight)
        return ret
    def remove(self, value):
        idx = self.index(value)
        weight = self[idx][1]
        self.__add_weight(-weight)
        del self[idx]

SVMStatistics = namedtuple(
    "SVMStatistics",
    (
        "explained_variance",
        "positive_results_ratio",
        "scaler_mean",
        "scaler_variance"
    )
)

class SVMWrapper:
    def __init__(
        self, pca_components, nu, kernel, minimum_batch = 100,
        post_flush=lambda: None, pre_flush=lambda: None,
        flush_update=lambda msg: None, pretend=False
    ):
        self._scaler = StandardScaler()
        self._pca = IncrementalPCA(pca_components, whiten=True)
        self._visualisation_pca = IncrementalPCA(2, whiten=True)
        self._kernel = kernel
        self._svm = SGDOneClassSVM(nu)
        self._post_flush = post_flush
        self._pre_flush = pre_flush
        self._flush_update = flush_update
        self._queue = WeightedFlushDeque(minimum_batch, self.__flush_queue)
        self._pretend = pretend
    def process(self, weighting, callback, args, train=True):
        self._queue.append(
            (weighting, callback, train, args),
            weight=weighting.shape[1]
        )
    def fill_ratio(self):
        return self._queue.current_weight / self._queue.maxweight
    def flush(self):
        return self._queue.flush()
    def __label_array(self, arr, train_list=None):

        if train_list is None:
            train_list = np.ones((arr.shape[0],), dtype='int')

        if self._pretend:
            return (
                np.full((arr.shape[0],), -1),
                SVMStatistics(
                    np.full(
                        (self._pca.n_components,),
                        1 / self._pca.n_components
                    ),
                    1, 0, 0
                )
            )
        n_batch = np.count_nonzero(train_list == True)
        train_ratio = n_batch / train_list.shape[0]
        if (
            train_ratio > 0 and
            n_batch >= self._pca.n_components and
            n_batch >= self._visualisation_pca.n_components
        ):
            self._scaler.partial_fit(arr[train_list])
            scaled = self._scaler.transform(arr)

            self._pca.partial_fit(scaled[train_list])
            self._visualisation_pca.partial_fit(scaled[train_list])
            pcaed = self._pca.transform(scaled)
            kerneled = self._kernel.transform(pcaed)

            self._svm.partial_fit(kerneled[train_list])
        else:
            scaled = self._scaler.transform(arr)
            pcaed = self._pca.transform(scaled)
            kerneled = self._kernel.transform(pcaed)
        try:
            labels = self._svm.predict(kerneled)
        except NotFittedError:
            labels = np.full((kerneled.shape[0],), -1)
        ratio = np.count_nonzero(labels == -1) / labels.shape[0]
        statistics = self.create_statistics(ratio)
        return (labels, statistics)
    def create_statistics(self, ratio = 0):
        try:
            explained_variance_ratio = np.array(
                self._pca.explained_variance_ratio_
            )
        except AttributeError:
            explained_variance_ratio = 0
        try:
            scaler_mean = np.mean(np.array(self._scaler.mean_))
        except AttributeError:
            scaler_mean = 0
        try:
            scaler_var = np.mean(np.array(self._scaler.var_))
        except AttributeError:
            scaler_var = 0
        return SVMStatistics(
            explained_variance_ratio, ratio, scaler_mean, scaler_var
        )
    def __flush_queue(self, items):
        self._pre_flush()
        records = []

        for i, item in enumerate(items):
            weighting, callback, train, args = item
            unmasked_idxs = (weighting.mask[:, 0] != True).nonzero()[0]
            reshaped = weighting.compressed().reshape(
                (-1,) + weighting.shape[1:]
            )
            assert unmasked_idxs.shape[0] == reshaped.shape[0]
            if reshaped.shape[0] == 0:
                continue
            records.append(
                (reshaped, unmasked_idxs, weighting, callback, train, args)
            )

        if len(records) == 0:
            self._post_flush()
            return

        arr = np.concatenate(
            [x[0] for x in records]
        )
        train_list = np.concatenate(
            [
                (
                    np.ones((x[0].shape[0],), dtype='int')
                ) if x[4] else (
                    np.zeros((x[0].shape[0],), dtype='int')
                ) for x in records
            ]
        ) == 1
        assert arr.shape[0] == train_list.shape[0]
        labels, statistics = self.__label_array(arr, train_list)

        start_offset = 0
        for (
            i, (reshaped, unmasked_idxs, weighting, callback, train, args)
        ) in enumerate(records):
            percent_done = i * 100 / len(records)
            end_offset = start_offset + reshaped.shape[0]
            these_labels = labels[start_offset:end_offset]
            masked_labels = ma.masked_array(
                np.ones(weighting.shape[0]),
                mask=True
            )
            assert unmasked_idxs.shape[0] == these_labels.shape[0]
            masked_labels[unmasked_idxs] = these_labels
            callback(weighting, masked_labels, statistics, *args)
            start_offset = end_offset
        self._post_flush()

class CorpusSVM(PipeObject):
    def __init__(
        self, strip_fun = lambda a: a, pca_components = 100,
        kernel_approximation_components = 150, nystroem_gamma = 0.1, nu = 0.5,
        minimum_batch = 100, min_svm = 3, tmp_a = 'tmp_a.db',
        tmp_b = 'tmp_b.db', visualisation_samples = 100,
        training_steps = 1000, training_decay = 0.99, **kwargs
    ):
        self._nodes = OrderedSet([])
        self._transcriptions = {}
        self._tmp_a = tmp_a
        self._tmp_b = tmp_b
        self._strip_fun = strip_fun
        self._kernel_approximation_components = kernel_approximation_components
        self._pca_components = pca_components
        self._nystroem_gamma = nystroem_gamma
        self._minimum_batch = minimum_batch
        self._nu = nu
        self._min_svm = min_svm
        self._svm = None
        self._visualisation_samples = visualisation_samples
        def decayed_training_scheduler(
            initial_training, decay_steps, decay_rate
        ):
            step = yield
            while True:
                step = yield float(
                    initial_training *
                    math.pow(decay_rate, step / decay_steps)
                )
        def make_training_scheduler():
            ret = decayed_training_scheduler(
                1, training_steps, training_decay
            )
            next(ret)
            return ret
        self._training_scheduler = make_training_scheduler
        super().__init__(**kwargs)
    def process(self, input_data: TranscriptionRecord) -> Iterator[None]:
        """Add a transcription record to the corpus"""
        inp_subject = input_data.subject
        inp_transcription = input_data.transcription
        self._nodes.update(
            {self._strip_fun(x) for x in inp_transcription.nodes()}
        )
        self._transcriptions[inp_subject] = inp_transcription
        yield from []
    def __create_weighting_matrix(self, transcriptions, nodes, n_modes):
        weighting_matrix = np.zeros(
            (len(transcriptions), len(nodes), len(nodes), n_modes)
        )
        node_lookup = {node: nodes.index(node) for node in nodes}
        for i, (_, transcription) in enumerate(transcriptions.items()):
            percent_done = i * 100 / len(transcriptions)
            self.set_status(
                f"Finalising: Creating weighting matrix {percent_done:.2f}%"
            )
            for u_unstripped, v_unstripped, d in transcription.edges(data=True):
                u = self._strip_fun(u_unstripped)
                v = self._strip_fun(v_unstripped)
                mode = d['mode']
                weight = 1
                if 'weight' in d.keys():
                    weight = d['weight']
                mode_idx = 1
                if mode == 'sequential':
                    mode_idx = 0
                u_idx = node_lookup[u]
                v_idx = node_lookup[v]
                weighting_matrix[i, u_idx, v_idx, mode_idx] += weight
        return weighting_matrix
    def __overwrite_ngram_files(self, new, old):
        old_keys = set(new.keys())
        old.close()
        new.close()
        old_fname = new._dict._fname
        new_fname = old._dict._fname
        del new
        del old
        try:
            os.remove(new_fname)
        except FileNotFoundError:
            pass
        new = SparseDiskStore(new_fname)
        old = SparseDiskStore(old_fname, keys=old_keys)
        print(
            f"Swapped disk stores.  New ({new._dict._fname}) has {len(new)} " +
            f"keys, and old ({old._dict._fname}) has {len(old)}."
        )
        return (new, old)
    def __arithmetic(self, a, b):
        assert a.shape == b.shape
        return a + b - a * b
    def __make_kernel_approximation(self, ngrams, weighting_matrix, components):
        self.set_status(
            f"Creating kernel approximation with {components} components"
        )
        sample = SparseDiskStoreRandomSampler(ngrams).sample(components)

        next_idxs = np.random.randint(0, weighting_matrix.shape[2], components)
        
        a = np.array([x[1] for x in sample])
        b = weighting_matrix[tuple(x[0] for x in sample),next_idxs].reshape(
            components, -1
        )
        created_sample = self.__arithmetic(a, b)
        pca_components = min(self._pca_components, components)
        scaler = StandardScaler()
        pca = PCA(pca_components, whiten=True)
        kernel_approximation = Nystroem(
            gamma=self._nystroem_gamma,
            n_components=components
        )
        kernel_approximation.fit(
            pca.fit_transform(
                scaler.fit_transform(
                    created_sample
                )
            )
        )
        return kernel_approximation
    def __make_n_grams(
        self, n, ngrams, next_grams, weighting_matrix,
        kernel_approximation_components, nu, training_scheduler
    ):
        self.set_status(f"Creating {n}-grams")

        try:
            kernel_approximation = self.__make_kernel_approximation(
                ngrams, weighting_matrix, kernel_approximation_components
            )
        except KeyError as e:
            print(str(e))
            return
        pca_components = min(
            self._pca_components, kernel_approximation_components
        )

        def after_flush_callback():
            self.set_status(f"Finalising: Saving {n}-grams")
            gc.collect()
        def flush_update(msg):
            self.set_status(f"Finalising (flushing): {msg}")
        self._svm = SVMWrapper(
            pca_components, nu, kernel_approximation, self._minimum_batch,
            post_flush=after_flush_callback, flush_update=flush_update,
            pretend=(n < self._min_svm)
        )

        to_yield = deque()
        def popleft_all(dq):
            try:
                while True:
                    yield dq.popleft()
            except IndexError:
                return

        @dataclass
        class AllTimePositiveRatio:
            ratio = 0
            total = 0

        all_time_positive_ratio = AllTimePositiveRatio()

        for i, (ngram, weighting) in enumerate(ngrams.items()):
            for next_node_idx in range(len(self._nodes)):
                percent_done = (
                    i * (next_node_idx + 1) * 100
                ) / (
                    len(ngrams) * (len(self._nodes) + 1)
                )
                try:
                    all_time_ratio = (
                        all_time_positive_ratio.ratio /
                        all_time_positive_ratio.total
                    )
                except ZeroDivisionError:
                    all_time_ratio = 0
                statistics = self._svm.create_statistics(all_time_ratio)
                mean_var = np.sum(statistics.explained_variance)
                # Get the current training step
                step = i * (next_node_idx + 1)
                # Calculate the current training rate
                training_rate = training_scheduler.send(step)
                fill_ratio = self._svm.fill_ratio()
                self.set_status(
                    f"Creating {n}-grams ({percent_done:.2f}%)\nScaler: mean=" +
                    f"{statistics.scaler_mean:.2f}, var=" +
                    f"{statistics.scaler_variance:.2f}\nPCA: " +
                    f"explained_variance={(mean_var * 100):.2f}%\nSVM: nu=" +
                    f"{(all_time_ratio * 100):.2f}%, training_rate=" +
                    f"{training_rate:.2f}\nQueue fill=" +
                    f"{(fill_ratio * 100):.2f}%"
                )
                new_frozenbag = ngram + frozenbag((next_node_idx,))
                reshaped_weighting = (
                    weighting_matrix[:, next_node_idx].reshape(
                        (weighting_matrix.shape[0], -1)
                    )
                )
                new_weighting = self.__arithmetic(
                    weighting,
                    reshaped_weighting
                )
                if new_frozenbag in ngrams:
                    original_weighting = ngrams[new_frozenbag]
                    original_unmasked = original_weighting.data
                    original_mask = original_weighting.mask
                    new_unmasked = new_weighting.data
                    new_mask = new_weighting.mask
                    only_new_masked = new_mask & ~original_mask
                    both_masked = original_mask & new_mask
                    none_masked = ~both_masked
                    new_unmasked[only_new_masked] = (
                        original_unmasked[only_new_masked]
                    )
                    new_unmasked[none_masked] = np.maximum(
                        original_unmasked[none_masked],
                        new_unmasked[none_masked]
                    )
                    new_weighting = ma.masked_array(
                        new_unmasked,
                        mask=both_masked
                    )
                    if ma.allclose(new_weighting, original_weighting):
                        continue
                def process_callback(
                    weighting, labels, statistics,
                    new_frozenbag, all_time_positive_ratio, training_rate, fill_ratio
                ):
                    to_yield.append(statistics)
                    all_time_positive_ratio.total += 1
                    all_time_positive_ratio.ratio += (
                        statistics.positive_results_ratio
                    )
                    all_time_ratio = (
                        all_time_positive_ratio.ratio /
                        all_time_positive_ratio.total
                    )
                    mean_var = np.sum(statistics.explained_variance)
                    status = (
                        f"Processing {n}-grams ({percent_done:.2f}%)\n" +
                        f"Scaler: mean={statistics.scaler_mean:.2f}, " +
                        f"var={statistics.scaler_variance:.2f}\nPCA: " +
                        f"explained variance=" +
                        f"{(mean_var * 100):.2f}%\nSVM: nu=" +
                        f"{(all_time_ratio * 100):.2f}%, training_rate=" +
                        f"{training_rate:.2f}\nQueue fill=" +
                        f"{(fill_ratio * 100):.2f}%"
                    )
                    self.set_status(status)
                    weighting[labels == 1] = ma.masked
                    if ma.any(weighting):
                        next_grams[new_frozenbag] = weighting
                # Draw training rate
                train = np.random.choice(
                    np.array([True, False]),
                    p=np.array([training_rate, 1 - training_rate])
                )
                # Send it off!
                self._svm.process(
                    new_weighting, process_callback,
                    (new_frozenbag, all_time_positive_ratio, training_rate, fill_ratio),
                    train
                )
            yield from popleft_all(to_yield)
        self._svm.flush()
        yield from popleft_all(to_yield)
    def __make_visualisation(self, svm):
        xx, yy = np.meshgrid(
            np.linspace(-1, 1, self._visualisation_samples),
            np.linspace(-1, 1, self._visualisation_samples)
        )
        visualisation_vals = np.c_[xx.ravel(), yy.ravel()]
        inverse_transformed = svm._visualisation_pca.inverse_transform(
            visualisation_vals
        )
        visualisation_pcaed = svm._pca.transform(inverse_transformed)
        visualisation_nystroemed = svm._kernel.transform(visualisation_pcaed)
        visualisation_z = svm._svm.decision_function(visualisation_nystroemed)
        return np.c_[xx.ravel(), yy.ravel(), visualisation_z]
    def __make_n_gram_statistics(self, ngrams):
        yield_n = 0
        len_n_grams = len(ngrams)
        percent = 0
        for items_n, (n_gram, val) in enumerate(ngrams.items(), 1):
            if ma.is_masked(val[:, 0]):
                indices = (val.mask[:, 0] != True).nonzero()[0]
            else:
                indices = range(val.shape[0])
            for index in indices:
                yield_n += 1
                yield (
                    percent,
                    tuple(
                        str(self._nodes[x]) for x in sorted(list(n_gram))
                    ) + (str(index,),)
                )
            average_yield_per_item = yield_n / items_n
            remaining_items = len_n_grams - items_n
            calc_remaining_yield = remaining_items * average_yield_per_item
            try:
                percent = calc_remaining_yield * 100 / (
                    yield_n + calc_remaining_yield
                )
            except ZeroDivisionError:
                percent = 0
    def finalise(self):
        self.set_status(f"Finalising: Creating weighting matrix")
        weighting_matrix = self.__create_weighting_matrix(
            self._transcriptions,
            self._nodes,
            2
        )
        ngrams = SparseDiskStore(self._tmp_a)
        self.set_status(f"Finalising: Creating unigrams")
        for i in range(len(self._nodes)):
            percent_done = i * 100 / len(self._nodes)
            self.set_status(
                f"Finalising: Creating unigrams ({percent_done:.2f}%)"
            )
            ngrams[frozenbag((i,))] = ma.masked_array(
                weighting_matrix[:, i].reshape(
                    (weighting_matrix.shape[0], -1)
                )
            )
        next_grams = SparseDiskStore(self._tmp_b, keys=set([]))
        for n in count(2):
            if len(ngrams) == 0:
                print("No more ngrams")
                break
            statistics = self.__make_n_grams(
                n, ngrams, next_grams, weighting_matrix,
                self._kernel_approximation_components,
                self._nu, self._training_scheduler()
            )
            if n < self._min_svm:
                _ = list(statistics)
            else:
                for i, (
                    explained_variance_record, positive_results_ratio_record,
                    scaler_mean_record, scaler_variance_record
                ) in enumerate(
                    [np.array([x]) for x in y] for y in statistics
                ):
                    self.set_status(f"Finalising: creating statistics")
                    yield GraphStatisticRecord(
                        "Explained Variance",
                        n,
                        tuple(
                            [f"Feature {i}" for i in range(
                                explained_variance_record.shape[1] if len(
                                    explained_variance_record.shape
                                ) > 1 else 0
                            )]
                        ),
                        explained_variance_record
                    )
                    yield GraphStatisticRecord(
                        'Positive Results Ratio',
                        n,
                        ("Ratio",),
                        np.expand_dims(positive_results_ratio_record, 0).reshape(-1, 1)
                    )
                    yield GraphStatisticRecord(
                        'Mean',
                        n,
                        ("Mean",),
                        np.expand_dims(scaler_mean_record, 0).reshape(-1, 1)
                    )
                    yield GraphStatisticRecord(
                        'Mean Variance',
                        n,
                        ("Mean variance",),
                        np.expand_dims(scaler_variance_record, 0).reshape(-1, 1)
                    )
                visualisation = self.__make_visualisation(self._svm)
                yield GraphStatisticRecord(
                    '2D PCA Visualisation',
                    n,
                    ("X", "Y", "Distance"),
                    visualisation
                )
            header = tuple([f"Item {i}" for i in range(n)]) + ("Setting",)
            for percent, record in self.__make_n_gram_statistics(
                next_grams
            ):
                self.set_status(
                    f"Finalising: sending {n}-gram results {percent:.2f}%"
                )
                yield GraphStatisticRecord(
                    "N-grams",
                    n,
                    header,
                    np.array([record], dtype='str')
                )
            self.set_status(f"Resetting database files", force=True)
            next_grams, ngrams = self.__overwrite_ngram_files(
                next_grams, ngrams
            )
            gc.collect()
        self.set_status(f"Done", force=True)
        yield from []
