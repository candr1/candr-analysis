import sys
from typing import Iterator
from collections import defaultdict
from itertools import product
import numpy as np
import numpy.ma as ma
from ordered_set import OrderedSet
from ...utilities.pipeline import PipeObject
from ...transcribers.transcription_record import TranscriptionRecord

class CorpusGraphComposable(PipeObject):
    """Creates a composable corpus graph"""
    def __init__(self, strip_fun = lambda a: a, n_limit = 10, v_limit = 4, convergence_tol = 0.05, **kwargs):
        self._nodes = OrderedSet()
        self._strip_fun = strip_fun
        self._transcriptions = {}
        self._n_limit = n_limit
        self._v_limit = v_limit
        self._convergence_tol = convergence_tol
        self._n_grams = defaultdict(lambda: {})
        super().__init__(**kwargs)
    def process(self, input_data: TranscriptionRecord) -> Iterator[None]:
        """Add a transcription to the corpus graph"""
        inp_subject = input_data.subject
        inp_transcription = input_data.transcription
        self._nodes.update({self._strip_fun(x) for x in inp_transcription.nodes()})
        self._transcriptions[inp_subject] = inp_transcription
        yield from []
    def finalise(self):
        """Process the graph"""
        # Step one: generate the weighting matrix

        # Add one to the n_nodes because it can be a link to the end, or nothing
        len_nodes = len(self._nodes)
        weighting_matrix = np.zeros((len(self._transcriptions), len_nodes, len_nodes, 2), dtype=float)
        # 0 = seq
        # 1 = sim
        node_lookup = {node: self._nodes.index(node) for node in self._nodes}
        edge_data = defaultdict(lambda: defaultdict(lambda: []))
        self.set_status(f"Finalising: Creating weighting matrix")
        for i, (subj, transcription) in enumerate(self._transcriptions.items()):
            percent_done = i * 100 / len(self._transcriptions)
            self.set_status(f"Finalising: Creating weighting matrix {percent_done:.2f}%")
            for u_unstripped, v_unstripped, d in transcription.edges(data=True):
                u = self._strip_fun(u_unstripped)
                v = self._strip_fun(v_unstripped)
                mode = d['mode']
                weight = 1
                if 'weight' in d.keys():
                    weight = d['weight']
                mode_idx = 1
                if mode == 'sequential':
                    mode_idx = 0
                u_idx = node_lookup[u]
                v_idx = node_lookup[v]
                weighting_matrix[i, u_idx, v_idx, mode_idx] += weight
                for k, v in d.items():
                    if k not in ['weight', 'mode']:
                        edge_data[(u, v)][k].append(v)
        # Step two: generate n-grams

        self.set_status(f"Finalising: Creating unigrams")
        # 2a: Start with 1-grams
        self._n_grams[1] = {}
        for i in range(len(self._nodes)):
            self._n_grams[1][(i,)] = ma.masked_array(weighting_matrix[:, i], mask=np.full(weighting_matrix[:, i].shape, False))
            print(f"Unigram: {(i,)}: {self._n_grams[1][(i,)]}")

        # 2b: start composing higher n-grams

        mask_shape = weighting_matrix.shape[0:1] + weighting_matrix.shape[2:]
        # mask_shape == (n_transcriptions, len_nodes, 2)
        for n in range(2, self._n_limit + 1):
            self.set_status(f"Finalising: Creating {n}-grams ({len(self._n_grams[n-1])} n-grams)")
            n_its = len_nodes ** n
            for already_it, already_indices in enumerate(product(range(len_nodes), repeat=(n - 1))):
                already_total_it = already_it ** n
                if already_indices not in self._n_grams[n-1]:
                    # It has already converged
                    continue

                # By default, continue on all fronts
                mask = np.full(mask_shape, False)

                # For trigrams and above, compute convergence
                if n > 2:
                    # Check convergence
                    convergence_arr = np.abs(
                        np.diff(
                            np.array([
                                (
                                    (self._n_grams[i][already_indices[:i]]) / i
                                ) for i in range(1, n)
                            ]),
                            axis=0
                        )
                    )
                    # convergence_arr.shape == (n - 2, n_transcriptions, len_nodes, 2)
                    norm_shape = convergence_arr.shape[:1] + (1,) + convergence_arr.shape[2:]
                    # with np.printoptions(threshold=sys.maxsize):
                    #     print("Convergence arr:", convergence_arr)
                    convergence_arr /= np.linalg.norm(convergence_arr, axis=1, ord=np.inf).reshape(norm_shape)
                    mask = (convergence_arr[-1] <= self._convergence_tol).astype(bool)
                    # mask.shape == (n_transcriptions, len_nodes, 2)

                # If all have converged, we're done
                if np.all(mask):
                    # We have converged all of them, don't both iterating
                    break
                if np.all(mask[:, already_indices[-1]]):
                    continue

                # Add another node to the n-gram
                for new_idx in range(len_nodes):
                    tup = already_indices + (new_idx,)
                    this_it = already_total_it + new_idx
                    percent_done = this_it * 100 / n_its
                    print(tup)
                    self.set_status(f"Finalising: Creating {n}-grams ({percent_done:.2f}%) (Previous created {len(self._n_grams[n-1])} n-grams)")

                    self._n_grams[n][tup] = self._n_grams[n-1][already_indices] + self._n_grams[1][(new_idx,)]

                    # Eliminate those for which the simultaneity is greater than the limit
                    v_limit_mask = np.zeros(self._n_grams[n][tup].mask.shape)
                    v_limit_mask[:, :, 1] = (self._n_grams[n][tup][:, :, 1] / n) >= self._v_limit
                    updated_mask = np.logical_or(self._n_grams[n][tup].mask, mask, v_limit_mask)
                    print(f"{100 * np.count_nonzero(updated_mask) / updated_mask.size}% masked")
                    self._n_grams[n][tup][updated_mask.nonzero()] = ma.masked
        yield self._n_grams
