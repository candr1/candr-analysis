"""Generate statistics for queried graphs"""
import io
import os
import json
import logging
import bisect
import csv
import pandas as pd
import dask.array as da
import dask.dataframe as dd
from typing import Iterator
from collections import namedtuple
import collections.abc
from pathlib import Path
import numpy as np
import scipy.stats
from sklearn.preprocessing import QuantileTransformer
from slugify import slugify
from sortedcontainers import SortedList
import matplotlib.pyplot as plt
from ...utilities.pipeline import PipeObject
from .graph_analyser import RepresentationSimilarities

GraphStatisticRecord = namedtuple(
    'GraphStatisticRecord',
    ['name', 'key', 'header', 'values', 'blob', 'extension', 'metadata', 'cleanup_files'],
    defaults=(None, None, None, None, None, None, None, [])
)

class GraphSimilarityStatistics(PipeObject):
    def __init__(self, p = 0.05, **kwargs):
        self._accumulated_similarities = SortedList([], key=lambda x: x[0])
        self._p = p
        super().__init__(**kwargs)
    def process(self, input_data: RepresentationSimilarities) -> Iterator[None]:
        # RepresentationSimilarities(
        #     representation,
        #     {
        #         (node_id, instance_name): (
        #             (
        #                 (node_id, instance_name),
        #                 score,
        #             ),
        #             (
        #                 (node_id, instance_name),
        #                 score,
        #             ),
        #             ...
        #         ),
        #         (node_id, instance_name): (
        #             (
        #                 ...
        #             ),
        #             ...
        #         ),
        #         ...
        #     } 
        # )
        for node_a, score_dat in input_data.similarities.items():
            self._accumulated_similarities.update([
                (score, (node_a, node_b)) for node_b, score in score_dat
            ])
        yield from []
    def set_finalise_processing_status(self, status):
        self.set_status("Finalising: " + status)
    def finalise(self) -> Iterator[GraphStatisticRecord]:
        self.set_finalise_processing_status("All similarity scores")
        hist_scores = np.sort(
            np.array(
                [x[0] for x in self._accumulated_similarities]
            )
        )
        _, p = scipy.stats.normaltest(hist_scores)
        hist, bin_edges = np.histogram(hist_scores, bins='fd')
        plt.figure()
        plt.hist(hist_scores, bins='fd')
        plt.axvline(x=np.min(hist_scores, axis=None), color='red')
        plt.axvline(x=np.max(hist_scores, axis=None), color='red')
        plt.title("All similarity scores")
        plt.xlabel("Similarity score")
        plt.ylabel("Frequency")
        yield GraphStatisticRecord(
            'All similarity scores',
            None,
            ("Frequency",),
            hist,
            self.save_fig_to_buf(plt),
            'png',
            {
                "bin edges": bin_edges.tolist()
            }
        )
        plt.figure()
        ax = plt.gca()
        prob = scipy.stats.probplot(hist_scores, dist='norm', plot=ax)
        plt.title("All similarity scores")
        plt.ylabel("Similarity score")
        yield GraphStatisticRecord(
            'All similarity scores against normal distribution',
            None,
            ("Score",),
            hist_scores,
            self.save_fig_to_buf(plt),
            'png',
            {
                "chi2 normal test p": p
            }
        )

        quantile_transformer = QuantileTransformer(
            n_quantiles=1000,
            subsample=int(1e5),
            output_distribution='normal'
        )
        hist_scores_reshaped = hist_scores.reshape(-1, 1)
        transformed = quantile_transformer.fit_transform(hist_scores_reshaped).reshape(-1)
        _, p = scipy.stats.normaltest(transformed)
        hist, bin_edges = np.histogram(transformed, bins='fd')
        plt.figure()
        plt.hist(transformed, bins='fd')
        plt.title("Similarity score, after quantile transform")
        plt.xlabel("Similarity score")
        plt.ylabel("Frequency")
        yield GraphStatisticRecord(
            'All similarity scores, after quantile transform',
            None,
            ("Frequency",),
            hist,
            self.save_fig_to_buf(plt),
            'png',
            {
                "bin edges": bin_edges.tolist()
            }
        )
        plt.figure()
        ax = plt.gca()
        _ = scipy.stats.probplot(transformed, dist='norm', plot=ax)
        plt.title("Similarity score, after quantile transform")
        plt.ylabel("Similarity score")
        yield GraphStatisticRecord(
            'All similarity scores, after quantile transform, against normal distribution',
            None,
            ("Score",),
            transformed,
            self.save_fig_to_buf(plt),
            'png',
            {
                "chi2 normal test p": p
            }
        )

        trans_mean = np.mean(transformed)
        trans_sd = np.std(transformed)
        key_inv = float((scipy.stats.norm.isf(self._p) * trans_sd) + trans_mean)
        idx = bisect.bisect_left(transformed, key_inv)

        significant_scores = self._accumulated_similarities[idx:]
        p_values = scipy.stats.norm.sf(abs((transformed[idx:] - trans_mean) / trans_sd))

        assert(len(significant_scores) == p_values.shape[0])
        yield GraphStatisticRecord(
            'Significant scores',
            None,
            ("Node 1", "Node 2", "Score", "P Val"),
            np.asarray(
                [
                    [key[0][0], key[1][0], score, p_val]
                    for (score, key), p_val in zip(significant_scores, p_values)
                ]
            )
        )
    def save_fig_to_buf(self, fig):
        buf = io.BytesIO()
        fig.savefig(buf, format='png')
        buf.seek(0)
        return buf.read()

class CSVFile:
    def __init__(self, fname = None):
        self._fname = None
        self._file_handle = None
        self._csv_obj = None
        self.fname = fname
    def flush(self):
        if self._file_handle:
            self._file_handle.flush()
    def _get_fname(self):
        return self._fname
    def _get_headers(self, fname):
        try:
            fp = open(fname, 'rt')
            reader = csv.DictReader(fp)
            fieldnames = tuple()
            if reader.fieldnames:
                fieldnames = tuple(reader.fieldnames)
            fp.close()
            return fieldnames
        except FileNotFoundError:
            return tuple()
    def _set_fname(self, fname):
        if fname == self._fname:
            return
        if self._file_handle:
            self._file_handle.flush()
            self._file_handle.close()
            self._file_handle = None
        self._fname = fname
        headers = self._get_headers(self._fname)
        self._file_handle = open(fname, 'at')
        self._csv_obj = csv.DictWriter(self._file_handle, fieldnames=headers)
    def check_headers(self, headers):
        if self._csv_obj is None:
            return False
        if len(self._csv_obj.fieldnames) == 0:
            if self._file_handle is None:
                return False
            self._csv_obj = csv.DictWriter(self._file_handle, fieldnames=tuple(headers))
            self._csv_obj.writeheader()
            self.flush()
        return tuple(headers) == tuple(self._csv_obj.fieldnames)
    def write_rows(self, rows):
        if self._csv_obj is None:
            raise Exception("CSV file not initialised")
        return self._csv_obj.writerows(rows)
    fname = property(
        fget=_get_fname,
        fset=_set_fname
    )

class GraphStatisticSaver(PipeObject):
    def __init__(self, folder = 'stats', **kwargs):
        self._folder = folder
        self._last_file = CSVFile()
        self._blob_idx = 1
        self._metadata_idx = 1
        super().__init__(**kwargs)
    def process(self, input_data: GraphStatisticRecord) -> Iterator[GraphStatisticRecord]:
        folder = Path(self._folder).joinpath(input_data.name)
        if input_data.key:
            folder = folder.joinpath(slugify(str(input_data.key)))
        folder.mkdir(parents=True, exist_ok=True)
        if input_data.values is not None:
            if len(input_data.values) == 0:
                logger = logging.getLogger('GraphStatisticSaver')
                logger.warning("GraphStatisticRecord was empty")
                print("Warning: GraphStatisticRecord was empty")
            else:
                csv_file = folder.joinpath('data.csv')
                to_save = input_data.values
                self._last_file.fname = csv_file
                if not self._last_file.check_headers(input_data.header):
                    raise Exception(
                        "Header check failed",
                        self._last_file._csv_obj.fieldnames, "vs",
                        input_data.header
                    )
                if isinstance(to_save, da.Array):
                    self._last_file.flush()
                    df = dd.from_array(
                        to_save,
                        columns=list(input_data.header)
                    )
                    if os.path.isfile(csv_file):
                        try:
                            original_df = dd.read_csv(csv_file, blocksize=None)
                            df = dd.concatenate((original_df, df))
                        except pd.errors.EmptyDataError:
                            pass
                    df.to_csv(csv_file, single_file=True)
                else:
                    try:
                        def write_dict_generator(a, header):
                            for ts in a:
                                yield dict(zip(header, ts))
                        write_dict = write_dict_generator(
                            to_save,
                            input_data.header
                        )
                        self._last_file.write_rows(write_dict)
                        self._last_file.flush()
                    except TypeError as e:
                        raise TypeError(
                            f"{str(e)}, got: {to_save} in {input_data.name}"
                        )
        if input_data.blob and input_data.extension:
            blob_file = folder.joinpath(
                'data-' + str(self._blob_idx) + '.' + input_data.extension
            )
            with blob_file.open('wb') as bf:
                bf.write(input_data.blob)
        if input_data.metadata:
            metadata_file = folder.joinpath(
                'metadata-' + str(self._metadata_idx) + '.json'
            )
            with metadata_file.open('w') as mf:
                json.dump(input_data.metadata, mf)
        for file in input_data.cleanup_files:
            print(f"Deleting {file}")
            os.remove(file)
        yield input_data
