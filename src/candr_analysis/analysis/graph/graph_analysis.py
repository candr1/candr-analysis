"""Graph analysis routines"""

import time
import json
from typing import Union, Any, List
from collections import OrderedDict
from sqlalchemy import create_engine, event, and_, func, case
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker, joinedload
from . import models
from ...utilities.pipeline import Timer

@event.listens_for(Engine, "connect")
def set_sqlite_pragma(db, record):
    """On ORM connect, set the SQLite to put its journal in memory"""
    cur = db.cursor()
    cur.execute("PRAGMA journal_mode = MEMORY")
    cur.close()

class ORMWrapper:
    """Wraps the SQLAlchemy ORM and stores its objects"""
    def __init__(self, store: str = 'graph.db', logger = None):
        #self.engine = create_engine('sqlite:///' + store, echo=True)
        self.engine = create_engine('sqlite:///' + store, echo=False)
        s = sessionmaker(bind=self.engine)
        self.session = s()
        self._queries = dict()
        models.bind_engine(self.engine)
        self._logger = logger
        self._timer = [Timer(), 0]

        @event.listens_for(Engine, "before_cursor_execute")
        def before_cursor_execute(conn, cur, stmt, params, ctx, executemany):
            self._timer[0].start()
            if not self._logger:
                return
            ctx._query_start_time = time.time()

        @event.listens_for(Engine, "after_cursor_execute")
        def after_cursor_execute(conn, cur, stmt, params, ctx, executemany):
            self._timer[0].stop()
            self._timer[1] += 1
            if not self._logger:
                return
            total = time.time() - ctx._query_start_time
            self._logger.write(
                '"' +
                ''.join(stmt.splitlines()) +
                '" took ' +
                str(total * 1000) +
                "ms\n"
            )
            if stmt not in self._queries:
                self._queries[stmt] = [0, 0]
            self._queries[stmt][0] += total * 1000
            self._queries[stmt][1] += 1
            self._log_json()
    def _log_json(self):
        ll = sorted(
            list(
                [(k, (v[0]/v[1], v[0], v[1])) for k, v in self._queries.items()]
            ),
            key=lambda x: x[1][1]
        )
        self._logger.write(
            "\n" + 
            json.dumps(ll) +
            "\n"
        )
    def __del__(self):
        self.session.rollback()
        self._log_json()

class Graph:
    """The SQL graph object that contains the graph data"""
    def __init__(self, store: str = 'graph.db', logger = None):
        self._orm = ORMWrapper(store, logger)
        self._autocommit = True
        self.__direction_in = None
        self.__direction_out = None
    def unrepresented_nodes(self):
        return self._orm.session.query(models.Node).filter(~models.Node.representations.any()).all()
    def addmany(self, iterable):
        self._autocommit = False
        ret = [self.add(*item) for item in iterable]
        self._autocommit = True
        self._do_commit()
        return ret
    def add(self, item: Any, representation_type: Union[str, models.RepresentationType], instance: models.Instance):
        """Add a node to the graph"""
        # Get our representation_type
        if type(representation_type) != models.RepresentationType:
            representation_type = self._get_or_create(
                models.RepresentationType,
                name = representation_type
            )
        # Get our representation
        representation = self._get_or_create(
            models.Representation,
            type = representation_type,
            representation = item
        )
        # Find a node with representations containing this representation
        node = self._orm.session.query(models.Node).filter(
            models.Node.representations.contains(representation),
            models.Node.instance == instance
        ).first()
        if not node:
            node = models.Node(
                representations = [representation],
                instance = instance
            )
            self._orm.session.add(node)
        self._do_commit()
        return node
    def __get_link_direction_records(self):
        """Link directions are either 'in' or 'out'. Make sure these exist and
        cache them"""
        # These records should be cached because linking many things together
        # will require a lot of these records
        if not self.__direction_in:
            self.__direction_in = self._get_or_create(
                models.DirectionType,
                name = 'in'
            )
        if not self.__direction_out:
            self.__direction_out = self._get_or_create(
                models.DirectionType,
                name = 'out'
            )
    def linkmany(self, iterable):
        self._autocommit = False
        ret = [self.link(*item) for item in iterable]
        self._autocommit = True
        self._do_commit()
        return ret
    def link(self, a: Union[models.Node, List[models.Node]], b: Union[models.Node, List[models.Node]], link_type: Union[str, models.LinkType], instance: models.Instance, weight = 1):
        """Link two nodes together"""
        if type(link_type) != models.LinkType:
            link_type = self._get_or_create(
                models.LinkType,
                name = link_type
            )
        if type(a) == models.Node:
            a = [a]
        if type(b) == models.Node:
            b = [b]

        self.__get_link_direction_records()

        a_ids = list([aa.id for aa in set(a)])
        b_ids = list([bb.id for bb in set(b)])

        # Big SQL to get the right links
        links_a = self._orm.session.query(models.Link.id).join(
            # links with link_association
            models.LinkAssociation
        ).group_by(
            models.Link.id
        ).having(
            # where the link_association has _exactly_ the same set of nodes. We
            # count the link_association with those nodes to be equal to the
            # length of the set, and the link_association without those nodes to
            # be zero
            and_(
                # short-circuit: these have to be equal for anything below to be
                # true
                func.count(
                    models.LinkAssociation.node_id
                ) == len(a_ids),
                func.sum(
                    case(
                        (models.LinkAssociation.node_id.in_(a_ids), 1),
                        else_=0
                    )
                ) == len(a_ids),
                func.sum(
                    case(
                        (models.LinkAssociation.node_id.not_in(a_ids), 1),
                        else_=0
                    )
                ) == 0
            )
        ).filter(
            # with the right link type
            models.Link.type_id == link_type.id
        ).filter(
            # in the correct instance
            models.Link.instance_id == instance.id
        ).filter(
            # with links going out
            models.LinkAssociation.type_id == self.__direction_out.id
        ).order_by(
            # order by link_id to correlate with the other
            models.Link.id
        )
        # almost identical to above, except b_ids and direction_in
        links_b = self._orm.session.query(models.Link.id).join(
            models.LinkAssociation
        ).group_by(
            models.Link.id
        ).having(
            and_(
                func.count(
                    models.LinkAssociation.node_id
                ) == len(b_ids),
                func.sum(
                    case(
                        (models.LinkAssociation.node_id.in_(b_ids), 1),
                        else_=0
                    )
                ) == len(a_ids),
                func.sum(
                    case(
                        (models.LinkAssociation.node_id.not_in(b_ids), 1),
                        else_=0
                    )
                ) == 0
            )
        ).filter(
            models.Link.type_id == link_type.id
        ).filter(
            models.Link.instance_id == instance.id
        ).filter(
            models.LinkAssociation.type_id == self.__direction_in.id
        ).order_by(
            models.Link.id
        )

        # Create a set of ids
        corr_links_a = set([a[0] for a in links_a.all()])
        corr_links_b = set([b[0] for b in links_b.all()])
        # All of them together
        correct_links = list(corr_links_a.intersection(corr_links_b))
        if len(correct_links) > 1:
            # This is not proven but shouldn't happen
            print("IntegrityError: should only have one link of this kind")
        if len(correct_links) == 0:
            # no links. make a new link
            a_link_associations = [models.LinkAssociation(
                node = el,
                type = self.__direction_out
            ) for el in a]
            b_link_associations = [models.LinkAssociation(
                node = el,
                type = self.__direction_in
            ) for el in b]
            link_associations = a_link_associations + b_link_associations
            [self._orm.session.add(a) for a in link_associations]
            link = models.Link(
                type = link_type,
                link_associations = link_associations,
                instance = instance
            )
        else:
            # strengthen the found link
            link = self._orm.session.query(models.Link).filter(
                models.Link.id == correct_links[0]
            ).first()
            link.strength += weight
        self._orm.session.add(link)
        self._do_commit()
    def __enter__(self):
        """We should be able to lock the graph while we do multiple inserts"""
        self._autocommit = False
        return self
    def __exit__(self, type, value, traceback):
        """Unlock the graph"""
        self._autocommit = True
        self._orm.session.commit()
    def _do_commit(self):
        """Wrapper for a commit"""
        if self._autocommit:
            self._orm.session.commit()
    def _get_or_create(self, model, **kwargs):
        """Fetch an item, or if not existing, create it"""
        instance = self._orm.session.query(model).filter_by(**kwargs).first()
        if instance:
            return instance
        else:
            instance = model(**kwargs)
            self._orm.session.add(instance)
            self._do_commit()
            return instance
