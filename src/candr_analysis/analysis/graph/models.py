"""Defines the models used in the graph-based analysis"""

from typing import Dict, Any
from sqlalchemy import Table, Column, Integer, Unicode, UnicodeText, PickleType, ForeignKey
from sqlalchemy.engine.base import Engine
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import DetachedInstanceError
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

def bind_engine(engine: Engine) -> None:
    """Bind the engine to the base, create all tables"""
    Base.metadata.bind = engine
    Base.metadata.create_all()

class BaseMixin(object):
    """A model from which all the models derive. Has helper functions for
    repr"""
    def __repr__(self) -> str:
        return self._repr(id=self.id)
    def _repr(self, **fields: Dict[str, Any]) -> str:
        """Helper for __repr__"""
        # Taken from https://stackoverflow.com/a/55749579
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f'{key}={field!r}')
            except DetachedInstanceError:
                field_strings.append(f'{key}=DetachedInstanceError')
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"f<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"

RepresentationAssociation = Table('representation_association',
    Base.metadata,
    Column('representable_id', ForeignKey('representable.id'), primary_key=True, index=True),
    Column('representation_id', ForeignKey('representation.id'), primary_key=True, index=True)
)

class Representable(Base, BaseMixin):
    """An abstract class that forms the basis for nodes & instances, classes
    that can be represented"""
    __tablename__ = 'representable'
    id = Column(Integer, primary_key=True)
    representation_type = Column(Unicode(20))
    representations = relationship(lambda: Representation,
        back_populates="represents",
        secondary=RepresentationAssociation
    )

    def __repr__(self):
        return self._repr(
            id = self.id,
            representations = type(self.representations)
        )

    __mapper_args__ = {
        'polymorphic_identity': 'representable',
        'polymorphic_on': representation_type
    }

class DirectionType(Base, BaseMixin):
    """A type model to indicate the direction of a link association: from a node
    into a link, or from a link back out to a node?"""
    __tablename__ = 'direction_type'
    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(), index=True)
    link_associations = relationship(lambda: LinkAssociation, back_populates="type")

    def __repr__(self):
        return self._repr(
            id = self.id,
            name = self.name,
            link_associations = type(self.link_associations)
        )

class Instance(Representable):
    """The real-world item that a link was drawn from"""
    __tablename__ = 'instance'
    id = Column(Integer, ForeignKey('representable.id'), primary_key=True)
    name = Column(UnicodeText(), index=True)
    type_id = Column(Integer, ForeignKey('instance_type.id'), index=True)
    type = relationship(lambda: InstanceType, back_populates="instances", uselist=False)
    links = relationship(lambda: Link, back_populates="instance", foreign_keys="Link.instance_id")
    nodes = relationship(lambda: Node, back_populates="instance", foreign_keys="Node.instance_id")

    def __repr__(self):
        return self._repr(
            id = self.id,
            name = self.name,
            type = type(self.type),
            links = type(self.links),
            nodes = type(self.nodes),
            representations = type(self.representations)
        )

    __mapper_args__ = {
        'polymorphic_identity': 'instance'
    }

class InstanceType(Base, BaseMixin):
    """A type model to indicate what kind of instance this is (a setting, a
    folio?)"""
    __tablename__ = 'instance_type'

    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(), index=True)
    instances = relationship(lambda: Instance, back_populates="type")

    def __repr__(self):
        return self._repr(
            id = self.id,
            name = self.name,
            instances = type(self.instances)
        )

class Link(Base, BaseMixin):
    """This is more of a nexus class that receives the coordinates the links
    between nodes. It has a type and is related to a real-world item through its
    instance"""
    __tablename__ = 'link'
    id = Column(Integer, primary_key=True)
    type_id = Column(Integer, ForeignKey('link_type.id'), index=True)
    instance_id = Column(Integer, ForeignKey('instance.id'), index=True)
    strength = Column(Integer, default=1)
    type = relationship(lambda: LinkType, back_populates="links", uselist=False)
    link_associations = relationship(lambda: LinkAssociation, back_populates="link")
    instance = relationship(lambda: Instance, back_populates="links", uselist=False, foreign_keys=[instance_id])

    def __repr__(self):
        return self._repr(
            id = self.id,
            strength = self.strength,
            type = type(self.type),
            link_associations = type(self.link_associations),
            instance = type(self.instance)
        )

class LinkAssociation(Base, BaseMixin):
    """An association model that forms a many-to-many relationship between links
    and nodes"""
    __tablename__ = 'link_association'
    node_id = Column(Integer, ForeignKey('node.id'), primary_key=True, index=True)
    link_id = Column(Integer, ForeignKey('link.id'), primary_key=True, index=True)
    type_id = Column(Integer, ForeignKey('direction_type.id'), primary_key=True, index=True)
    type = relationship(lambda: DirectionType, back_populates="link_associations", uselist=False)
    node = relationship(lambda: Node, back_populates="link_associations", uselist=False)
    link = relationship(lambda: Link, back_populates="link_associations", uselist=False)

    def __repr__(self):
        return self._repr(
            type = type(self.type),
            node = type(self.node),
            link = type(self.link)
        )

class LinkType(Base, BaseMixin):
    """A type model which indicates what kind of link we're making
    (synchronicity, serialism, soft-synchs?)"""
    __tablename__ = 'link_type'

    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(), index=True)
    links = relationship(lambda: Link, back_populates="type")

    def __repr__(self):
        return self._repr(
            id = self.id,
            name = self.name,
            links = type(self.links)
        )

class Node(Representable):
    """A node may be a gram: it has a representation (the gram itself), and is
    linked to other grams through link associations and links"""
    __tablename__ = 'node'
    id = Column(Integer, ForeignKey('representable.id'), primary_key=True)
    instance_id = Column(Integer, ForeignKey('instance.id'), index=True)
    instance = relationship(lambda: Instance, back_populates="nodes", uselist=False, foreign_keys=[instance_id])
    link_associations = relationship(lambda: LinkAssociation, back_populates="node")

    def __repr__(self):
        return self._repr(
            id = self.id,
            representations = type(self.representations),
            link_associations = type(self.link_associations),
            instance = type(self.instance)
        )

    __mapper_args__ = {
        'polymorphic_identity': 'node'
    }

class Representation(Base, BaseMixin):
    """A class that coordinates the pickleable representation of items"""
    __tablename__ = 'representation'

    id = Column(Integer, primary_key=True)
    representation = Column(PickleType, index=True)
    type_id = Column(Integer, ForeignKey('representation_type.id'), index=True)
    type = relationship(lambda: RepresentationType, back_populates="representations", uselist=False)
    represents = relationship(lambda: Representable,
        back_populates="representations",
        secondary=RepresentationAssociation
    )

    def __repr__(self):
        return self._repr(
            id = self.id,
            representation = type(self.representation),
            type = type(self.type),
            represents = type(self.represents)
        )

class RepresentationType(Base, BaseMixin):
    """A type model to indicate what kind of representation this is (a
    representable may be able to be represented in multiple ways!)"""
    __tablename__ = 'representation_type'

    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(), index=True)
    representations = relationship(lambda: Representation, back_populates="type")

    def __repr__(self):
        return self._repr(
            id = self.id,
            name = self.name,
            representations = type(self.representations)
        )
