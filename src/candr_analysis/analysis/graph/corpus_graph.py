"""Creates a corpus graph"""

import gc
import math
import numbers
from typing import Iterator, Dict
from queue import Queue, Empty
from operator import ior
from functools import reduce
from itertools import chain, cycle, islice, permutations
from ordered_set import OrderedSet
from frozendict import frozendict
import networkx as nx
from collections import namedtuple, defaultdict
from ...utilities.pipeline import PipeObject
from ...utilities.progress import CLIProgress
from ...transcribers.transcription_record import TranscriptionRecord

CorpusGraphKey = namedtuple('CorpusGraphKey', ['subject', 'key'])

class CorpusAnalysisResult(namedtuple('CorpusAnalysisResult', [
    'missing_edges', 'missing_modes', 'missing_datum',
    'datum_numeric_differences', 'datum_other_differences'
])):
    __slots__ = ()
    def __str__(self):
        return f"{self.missing_edges}/{self.missing_modes}/{self.missing_datum}/{len(self.datum_numeric_differences)}/{len(self.datum_other_differences)}"
class CorpusGraph(PipeObject):
    """Creates a corpus graph"""
    def __init__(self, strip_fun = lambda a: a, progress = CLIProgress(), **kwargs):
        self._graph = nx.MultiDiGraph()
        self._prog = progress
        self._strip_fun = strip_fun
        super().__init__(**kwargs)
    def process(self, input_data: TranscriptionRecord) -> Iterator[None]:
        """Add a transcription to the corpus graph"""
        inp_subject = input_data.subject
        inp_transcription = input_data.transcription
        for u_unstripped, v_unstripped, k, d in inp_transcription.edges(data=True, keys=True):
            graph_key = CorpusGraphKey(inp_subject, k)
            u = self._strip_fun(u_unstripped)
            v = self._strip_fun(v_unstripped)
            mode = d['mode']
            d_dict = {k: v for k, v in d.items() if k != 'mode'}
            if not self._graph.has_edge(u, v, key=graph_key):
                param = {mode: d_dict}
                self._graph.add_edge(u, v, graph_key, **param)
            elif len(d_dict) > 0:
                if mode not in self._graph[u][v][graph_key].keys():
                    self._graph[u][v][graph_key][mode] = {}
                for key, val in d_dict.items():
                    if key in self._graph[u][v][graph_key][mode].keys():
                        if key == 'weight':
                            self._graph[u][v][graph_key][mode][key] += val
                        else:
                            self._prog.warning(f"Corpus graph overwriting key {key} for layer {graph_key}")
                            self._graph[u][v][graph_key][mode][key] = val
                    else:
                        self._graph[u][v][graph_key][mode][key] = val
        yield from []
    def finalise(self):
        yield self._graph

class CorpusAnalysis(PipeObject):
    def __init__(self, graph, n, graph_compare, strip_fun = lambda a: a, tmpdir=None, **kwargs):
        self._graph = graph
        self._n = n
        self._keyed_subgraphs = {}
        self._graph_compare = graph_compare
        self._strip_fun = strip_fun
        self._tmpdir = tmpdir
        self._difference_graphs = {}
        super().__init__(**kwargs)
    def _get_n_gram(self, start, graph, n, pred = lambda _: True):
        if n <= 1:
            return [[start]]
        paths = []
        for u, v, k, d in graph.edges(start, data=True, keys=True):
            if not pred((u, v, k, d)):
                continue
            for path in self._get_n_gram(v, graph, n - 1, pred):
                paths.append([u] + path)
        return paths
    def setup(self):
        n_edges = self._graph.size()
        for i, (u, v, k, d) in enumerate(self._graph.edges(data=True, keys=True)):
            percent_done = i * 100 / n_edges
            self.set_status(f"Starting: {percent_done:.2f}%")
            if k not in self._keyed_subgraphs.keys():
                self._keyed_subgraphs[k] = nx.MultiDiGraph()
            for mode, d_dict in d.items():
                self._keyed_subgraphs[k].add_edge(u, v, key=mode, **d_dict)
        n_comb = 0
        for (a_k, a_g), (b_k, b_g) in permutations(self._keyed_subgraphs.items(), 2):
            n_comb += a_g.order() * b_g.order()
        i = 0
        add_edge_arg = []
        for (a_k, a_g), (b_k, b_g) in permutations(self._keyed_subgraphs.items()):
            for n_a, n_b in product(a_g.nodes, b_g.nodes):
                percent_done = i * 100 / n_comb
                self.set_status(f"Generating differences: {percent_done:.2f}%")
                dfa = defaultdict(lambda: dict())
                dfb = defaultdict(lambda: dict())
                dfa_uvks = set()
                dfb_uvks = set()
                missing_edges = 0
                missing_modes = 0
                missing_datum = 0
                datum_numeric_differences = defaultdict(lambda: defaultdict(lambda: 0))
                datum_other_differences = defaultdict(lambda: defaultdict(lambda: []))
                for u, v, k, d in a_g.edges(n_a, data=True, keys=True):
                    dfa[(u, v)][k] = d
                    dfa_uvks.add((u, v, k))
                for u, v, k, d in b_g.edges(n_b, data=True, keys=True):
                    dfb[(u, v)][k] = d
                    dfb_uvks.add((u, v, k))
                for edge, dfa_mode_dat in dfa.items():
                    if edge not in dfb.keys():
                        missing_edges += 1
                        continue
                    dfb_mode_dat = dfb[edge]
                    for mode, dfa_data in dfa_mode_dat.items():
                        if mode not in dfb_mode_dat.keys():
                            missing_modes += 1
                            continue
                        dfb_data = dfb_mode_dat[mode]
                        for data_key, dfa_datum in dfa_data.items():
                            if data_key not in dfb_data.keys():
                                missing_datum += 1
                            dfb_datum = dfb_data[data_key]
                            if isinstance(dfa_datum, numbers.Number):
                                datum_numeric_differences[mode][data_key] += abs(dfa_datum - dfb_datum)
                            else:
                                datum_other_differences[mode][data_key].append((dfa_datum, dfb_datum))
                analysis_result = CorpusAnalysisResult(
                    missing_edges,
                    missing_modes,
                    missing_datum,
                    frozendict({k: frozendict(v) for k, v in datum_numeric_differences.items()}),
                    frozendict({k: frozendict({kk: tuple(vv) for kk, vv in v.items()}) for k, v in datum_other_differences.items()})
                )
                add_edge_arg.append(((n_a,), n_b, {(a_k, b_k): analysis_result}))
                i += 1
        self._difference_graphs[1] = nx.DiGraph(add_edge_arg)

        # SORT THIS SHIT OUT
        add_edge_arg = []
        for a_u_tup, a_v, a_d in self._difference_graphs[1].edges(data=True):
            a_u_last = a_u_tup[-1]
            a_u_last_only_tup = (a_u_last,)
            for key, graph in self._keyed_subgraphs.items():
                for n_u, n_v, n_k in graph.edges(a_u_last, keys=True):
                    if n_k != 'sequential':
                        continue
                    attr_dict = {}
                    try:
                        difference_one_edge_d = self._difference_graphs[1].edges[a_u_last_only_tup, n_v]
                        for key_tup, result in a_d.items():
                            if key_tup[0] != key:
                                continue
                            if key_tup in difference_one_edge_d.keys():
                                combined = combine_analysis_result(result, difference_one_edge_d[key_tup])
                                attr_dict[key_tup] = combined
                    except KeyError:
                        pass
                    add_edge_arg.append(((a_u_tup + (n_v,)), a_v, attr_dict))
        self._difference_graphs[2] = nx.DiGraph(add_edge_arg)

    def process(self, input_data: TranscriptionRecord) -> Iterator[Dict]:
        this_graph = nx.MultiDiGraph()
        for u, v, d in input_data.transcription.edges(data=True):
            mode = d['mode']
            d_dict = {k: v for k, v in d.items() if k != 'mode'}
            this_graph.add_edge(u, v, key=mode, **d_dict)
        def get_start_nodes(graph):
            has_seq_in = set()
            for uvkd in filter(lambda x: x[2] == 'sequential', graph.edges(data=True, keys=True)):
                has_seq_in.add(uvkd[1])
            return filter(
                lambda x: x not in has_seq_in,
                graph.nodes()
            )
        start_nodes = list(get_start_nodes(this_graph))
        def seq_iterator(node, graph):
            stack = Queue()
            done = set()
            stack.put(node)
            try:
                while True:
                    top = stack.get(block=True, timeout=1)
                    if top in done:
                        continue
                    done.add(top)
                    yield top
                    for _, v, k, in graph.edges(top, keys=True):
                        if k != 'sequential' or v in done:
                            continue
                        stack.put(v)
            except Empty:
                return
        def roundrobin_unique(iterables):
            done = set()
            num_active = len(iterables)
            nexts = cycle(iter(it).__next__ for it in iterables)
            while num_active:
                try:
                    for next in nexts:
                        n = next()
                        if n in done:
                            continue
                        done.add(n)
                        yield n
                except StopIteration:
                    num_active -= 1
                    nexts = cycle(islice(nexts, num_active))
        done = set()
        self.set_status("Processing: generating nodes", force=True)
        nodes_unstripped = list(roundrobin_unique([seq_iterator(n, this_graph) for n in start_nodes]))
        to_process_list = []
        this_subgraphs = OrderedSet()
        node_list = []
        node_dict = {}
        jobs = OrderedSet()
        def add_node(node):
            if node not in node_dict.keys():
                node_dict[node] = len(node_list)
                node_list.append(node)
            return node_dict[node]
        for i, node_unstripped in enumerate(nodes_unstripped):
            node = self._strip_fun(node_unstripped)
            if node in done:
                continue
            done.add(node)
            def pred(tup):
                return tup[2] == 'sequential'
            for this_path_unstripped in self._get_n_gram(node_unstripped, this_graph, self._n, pred):
                this_stripped_edges = defaultdict(lambda: defaultdict(lambda: dict()))
                for u, v, k, d in chain(
                    this_graph.out_edges(this_path_unstripped, data=True, keys=True),
                    this_graph.in_edges(this_path_unstripped, data=True, keys=True)
                ):
                    key = (self._strip_fun(u), self._strip_fun(v))
                    def iterate_dict(da, db):
                        intersection = da.keys() & db.keys()
                        for k in intersection:
                            da_val = da[k]
                            db_val = db[k]
                            if k == 'weight':
                                yield k, (da_val + db_val)
                            else:
                                yield k, db_val
                        for k, v in db.items():
                            if k in intersection:
                                continue
                            yield k, v
                        for k, v in da.items():
                            if k in intersection:
                                continue
                            yield k, v
                    this_stripped_edges[key][k] = {kk: vv for kk, vv in iterate_dict(this_stripped_edges[key][k], d)}
                processed_edges = []
                for key, key_dat in this_stripped_edges.items():
                    for k, d in key_dat.items():
                        processed_edges.append((key[0], key[1], k, d))
                this_subgraph = nx.MultiDiGraph(processed_edges)
                this_subgraph_tup = (this_subgraph, input_data.subject, node, node_unstripped)
                this_subgraphs.add(this_subgraph_tup)
                this_subgraph_idx = this_subgraphs.index(this_subgraph_tup)
                for key, key_subgraph in self._keyed_subgraphs.items():
                    for other_path in self._get_n_gram(node, key_subgraph, self._n, pred):
                        # We don't just want the nodes in the path, but also all their
                        # adjacent nodes
                        # to_process = key_subgraph.edge_subgraph(chain(
                        #     key_subgraph.out_edges(other_path, keys=True),
                        #     key_subgraph.in_edges(other_path, keys=True)
                        # ))
                        idxs = set(add_node(n) for n in other_path)
                        bitmask = reduce(ior, (1 << i for i in idxs))
                        # Place the job items in the job queue
                        job_tup = (node, key, bitmask, this_subgraph_idx)
                        jobs.add(job_tup)
                        to_process_list.append(jobs.index(job_tup))
                        duplicate_multiplier = (len(to_process_list) - len(jobs))/len(jobs)
                        self.set_status(f"Processing: {len(jobs)} jobs created (+ {duplicate_multiplier:.2f}x duplicates)")

        # function that transforms an bitmask into a list of idxs for the nodelist
        def bitmask_to_graph(key, bitmask):
            bit_list = ((bitmask >> shift_ind) & 1 for shift_ind in range(bitmask.bit_length()))
            nodes = [node_list[n] for n, i in enumerate(bit_list) if i == 1]
            return self._keyed_subgraphs[key].edge_subgraph(chain(
                self._keyed_subgraphs[key].out_edges(nodes, keys=True),
                self._keyed_subgraphs[key].in_edges(nodes, keys=True)
            ))

        # set up the results list (should be faster to allocate at-once)
        job_results = [None] * len(jobs)
        for i, job_tup in enumerate(jobs):
            # fetch the parameters
            node, key, bitmask, this_subgraph_idx = job_tup
            # get the "this" subgraph
            this_subgraph_tup = this_subgraphs[this_subgraph_idx]
            this_subgraph = this_subgraph_tup[0]
            # make the "other" subgraph from the bitmask and key
            other_subgraph = bitmask_to_graph(key, bitmask)
            # create the parameters for placing in the result array
            result_tup = (key, this_subgraph_tup[1], this_subgraph_tup[3])
            # do the calculation and assign to the job result array
            job_results[i] = (result_tup, self._graph_compare(node, this_subgraph, other_subgraph))
            percent_done = i * 100 / len(jobs)
            self.set_status(f"Processing: {percent_done:.2f}% GED: {job_results[job_tup]}")

        result = {}
        # for every "false" job (i.e. including duplicates)
        for i, job_idx in enumerate(to_process_list):
            # the job has been done
            assert job_results[job_idx] is not None
            # the results gave us the tuple for the result array and the GED
            job_tup, ged = job_results[job_idx]
            # unpack the tuple into parameters
            key, subject, node_unstripped = job_tup
            # create the final result tuple
            result_tup = (subject, node_unstripped)
            if result_tup not in result.keys():
                result[result_tup] = {}
            # set into result array
            result[result_tup][key] = ged
            percent_done = i * 100 / len(to_process_list)
            self.set_status(f"Processing: Creating results {percent_done:.2f}%")

        for subgraph_tup in this_subgraphs:
            # really make sure that that graph is deleted
            del subgraph_tup[0]
        del this_subgraphs
        # just an array of ints, should be fine
        del to_process_list
        # array of tuples
        del jobs
        # array of tuples
        del job_results
        yield result
        # dict[dict] of dict[tuple] of tuple
        del result
        gc.collect()
