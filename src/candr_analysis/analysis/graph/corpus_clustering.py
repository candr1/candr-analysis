import gc
import os
import random
import zlib
import pickle
import sqlite3
import math
from typing import Iterator
from collections import defaultdict, OrderedDict
from itertools import count
from collections_extended import frozenbag
import numpy as np
import numpy.ma as ma
from ordered_set import OrderedSet
import sparse
from sqlitedict import SqliteDict
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import IncrementalPCA, PCA
from sklearn.kernel_approximation import Nystroem
from sklearn.linear_model import SGDOneClassSVM
from sklearn.exceptions import NotFittedError
from ...utilities.pipeline import PipeObject
from ...transcribers.transcription_record import TranscriptionRecord
from .graph_statistics import GraphStatisticRecord

class CorpusClustering(PipeObject):
    """Cluster corpus points together in n-dimensions"""
    def __init__(
        self, strip_fun = lambda a: a, pca_components = 100,
        kernel_approximation_components = 150, nystroem_gamma = 0.1, nu = 0.5,
        minimum_batch = 100, min_svm = 3, tmp_a = 'tmp_a.sqlite',
        tmp_b = 'tmp_b.sqlite', visualisation_samples = 100, learning_rate_steps = 1000000,
        learning_rate_decay = 0.98, **kwargs
    ):
        self._strip_fun = strip_fun
        self._nodes = OrderedSet([])
        self._nu = nu
        self._min_svm = min_svm
        self._transcriptions = {}
        self._tmp_a = tmp_a
        self._tmp_b = tmp_b
        self._pca_components = pca_components
        self._nystroem_gamma = nystroem_gamma
        self._kernel_approximation_components = kernel_approximation_components
        self._minimum_batch = minimum_batch
        self._visualisation_samples = visualisation_samples
        def decayed_learning_rate_scheduler(initial_learning_rate, decay_steps, decay_rate):
            step = yield
            while True:
                step = yield initial_learning_rate * math.pow(decay_rate, step / decay_steps)
        self._learning_rate_scheduler = decayed_learning_rate_scheduler(1, learning_rate_steps, learning_rate_decay)
        next(self._learning_rate_scheduler)
        super().__init__(**kwargs)
    def process(self, input_data: TranscriptionRecord) -> Iterator[None]:
        """Add a transcription record to the corpus"""
        inp_subject = input_data.subject
        inp_transcription = input_data.transcription
        self._nodes.update({self._strip_fun(x) for x in inp_transcription.nodes()})
        self._transcriptions[inp_subject] = inp_transcription
        yield from []
    def finalise(self):
        """Process the graph"""
        # Step one: generate the weighting matrix
        def sqlitedict_encode(obj):
            return sqlite3.Binary(zlib.compress(pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)))
        def sqlitedict_decode(obj):
            return pickle.loads(zlib.decompress(bytes(obj)))
        def set_pragmas(sqlite_dict):
            sqlite_dict.conn.execute("PRAGMA journal_mode = 'MEMORY'")
            sqlite_dict.conn.execute("PRAGMA temp_store = 2")
            sqlite_dict.conn.execute("PRAGMA synchronous = 0")
            sqlite_dict.conn.execute("PRAGMA cache_size = -1000000")
            return sqlite_dict
        rng = np.random.default_rng()
        _n_grams = set_pragmas(SqliteDict(self._tmp_a, encode=sqlitedict_encode, decode=sqlitedict_decode))
        _prev_grams = set_pragmas(SqliteDict(self._tmp_b, encode=sqlitedict_encode, decode=sqlitedict_decode))
        len_nodes = len(self._nodes)
        weighting_matrix = np.zeros((len(self._transcriptions), len_nodes, len_nodes, 2))
        # 0 = seq
        # 1 = sim
        node_lookup = {node: self._nodes.index(node) for node in self._nodes}
        edge_data = defaultdict(lambda: defaultdict(lambda: []))
        self.set_status(f"Finalising: Creating weighting matrix")
        def masked_to_sparse_store(masked):
            ret = (sparse.COO(ma.getdata(masked)), sparse.COO(ma.getmaskarray(masked)), masked.fill_value)
            return ret
        def sparse_store_to_masked(sparse_store):
            ret = ma.masked_array(sparse_store[0].todense(), mask=sparse_store[1].todense(), fill_value=sparse_store[2])
            return ret
        for i, (_, transcription) in enumerate(self._transcriptions.items()):
            percent_done = i * 100 / len(self._transcriptions)
            self.set_status(f"Finalising: Creating weighting matrix {percent_done:.2f}%")
            for u_unstripped, v_unstripped, d in transcription.edges(data=True):
                u = self._strip_fun(u_unstripped)
                v = self._strip_fun(v_unstripped)
                mode = d['mode']
                weight = 1
                if 'weight' in d.keys():
                    weight = d['weight']
                mode_idx = 1
                if mode == 'sequential':
                    mode_idx = 0
                u_idx = node_lookup[u]
                v_idx = node_lookup[v]
                weighting_matrix[i, u_idx, v_idx, mode_idx] += weight
                for k, v in d.items():
                    if k not in ['weight', 'mode']:
                        edge_data[(u, v)][k].append(v)
        self.set_status(f"Finalising: Creating unigrams")
        frozenbag_lookup = {}
        # 2a: Start with 1-grams
        for i in range(len(self._nodes)):
            ngram_bag = frozenbag((i, ))
            hashed_bag = hex(hash(ngram_bag))
            _n_grams[hashed_bag] = masked_to_sparse_store(ma.masked_array(weighting_matrix[:, i].reshape((weighting_matrix.shape[0], -1)), fill_value=0))
            frozenbag_lookup[hashed_bag] = ngram_bag
        _n_grams.commit()
        # Start composition/decomposition process
        explained_variances = []
        means = []
        mean_variances = []
        positive_results_store = []
        sample = []
        ratio_positive_settings = np.full((len(self._nodes),), 1)
        self.set_status(f"Finalising: Composing n-grams")
        for n in count(2):
            learning_rate = 1
            current_step = 0
            def n_gram_result_generator(n_grams):
                yield_n = 0
                items_n = 0
                len_n_grams = len(n_grams)
                percent = 0
                for n_gram_hash, val_sparse_store in n_grams.items():
                    items_n += 1
                    val = sparse_store_to_masked(val_sparse_store)
                    n_gram = frozenbag_lookup[n_gram_hash]
                    if ma.is_masked(val[:, 0]):
                        indices = (val.mask[:, 0] != True).nonzero()[0]
                    else:
                        indices = range(val.shape[0])
                    for index in indices:
                        yield_n += 1
                        yield (percent, tuple(str(self._nodes[x]) for x in sorted(list(n_gram))) + (str(index),))
                    average_yield_per_item = yield_n / items_n
                    remaining_items = len_n_grams - items_n
                    calc_remaining_yield = remaining_items * average_yield_per_item
                    try:
                        percent = calc_remaining_yield * 100 / (yield_n + calc_remaining_yield)
                    except ZeroDivisionError:
                        percent = 0
            self.set_status(f"Finalising: sending n-gram results")
            header = tuple([f"Item {i}" for i in range(n - 1)]) + ("Setting",)
            for percent, record in n_gram_result_generator(_n_grams):
                self.set_status(f"Finalising: sending n-gram results {percent:.2f}%")
                yield GraphStatisticRecord(
                    "N-grams",
                    n - 1,
                    header,
                    np.array([record], dtype='str')
                )
            explained_variance_np = np.array(explained_variances)
            positive_results_np = np.array(positive_results_store).reshape(-1, 1)
            ratio_positive_settings_np = np.array(ratio_positive_settings).reshape(-1, 1)
            means_np = np.array(means).reshape(-1, 1)
            mean_variances_np = np.array(mean_variances).reshape(-1, 1)
            yield GraphStatisticRecord(
                'Explained Variance',
                n - 1,
                tuple([f"Feature {i}" for i in range(explained_variance_np.shape[1] if len(explained_variance_np.shape) > 1 else 0)]),
                explained_variance_np
            )
            yield GraphStatisticRecord(
                'Positive Results Ratio',
                n - 1,
                ("Ratio",),
                positive_results_np
            )
            yield GraphStatisticRecord(
                'Positive Settings Ratio',
                n - 1,
                ("Ratio",),
                ratio_positive_settings_np
            )
            yield GraphStatisticRecord(
                'Mean',
                n - 1,
                ("Mean",),
                means_np
            )
            yield GraphStatisticRecord(
                'Mean Variance',
                n - 1,
                ("Mean variance",),
                mean_variances_np
            )
            _n_grams.commit()
            _n_grams.close()
            _prev_grams.close()
            os.rename(self._tmp_a, self._tmp_b)
            _n_grams = SqliteDict(self._tmp_a, encode=sqlitedict_encode, decode=sqlitedict_decode)
            _prev_grams = SqliteDict(self._tmp_b, encode=sqlitedict_encode, decode=sqlitedict_decode)
            prev_frozenbag_lookup = frozenbag_lookup
            frozenbag_lookup = {}
            if n > 2 and len(_prev_grams) == 0:
                self.set_status(f"Finished, prev_grams = 0", force=True)
                break
            kernel_approximation_components = min(len(_prev_grams), self._kernel_approximation_components)
            self.set_status(f"Finalising: creating kernel approximation for {n}-grams using {kernel_approximation_components} components")
            sub_idxs = []
            sample = []
            prev_frozenbag_lookup_keys_list = list(prev_frozenbag_lookup.keys())
            while len(sample) < kernel_approximation_components:
                percent_kernel = len(sample) * 100 / kernel_approximation_components
                self.set_status(f"Finalising: creating kernel approximation for {n}-grams using {kernel_approximation_components} components {percent_kernel:.2f}%")
                key_hash = random.choice(prev_frozenbag_lookup_keys_list)
                try:
                    key = prev_frozenbag_lookup[key_hash]
                    val_sparse_store = _prev_grams[key_hash]
                    val = sparse_store_to_masked(val_sparse_store)
                    if ma.is_masked(val[:, 0]):
                        sub_indices = (val.mask[:, 0] != True).nonzero()[0]
                        if sub_indices.shape[0] == 0:
                            print(f"n-gram {key} is completely masked")
                            continue
                        sub_idx = np.random.choice(sub_indices)
                    else:
                        sub_indices = range(val.shape[0])
                        sub_idx = np.random.randint(0, val.shape[0])
                    sub_idxs.append(sub_idx)
                    sample.append(val[sub_idx])
                except KeyError:
                    continue
            del prev_frozenbag_lookup_keys_list
            # Idxs of next nodes to choose
            next_idxs = np.random.randint(0, weighting_matrix.shape[1], kernel_approximation_components)
            kernel_approximation_sample = (
                np.array(sample[:kernel_approximation_components]).reshape(kernel_approximation_components, -1) +
                weighting_matrix[sub_idxs, next_idxs].reshape((kernel_approximation_components, -1))
            )
            scaler = StandardScaler()
            # Use "proper" PCA here for slightly better results for kernel_approximation
            pca_components = min(self._pca_components, kernel_approximation_components, kernel_approximation_sample.shape[1])
            pca = PCA(pca_components, whiten=True)
            kernel_approximation = Nystroem(gamma=self._nystroem_gamma, n_components=kernel_approximation_components)
            kernel_approximation.fit(pca.fit_transform(scaler.fit_transform(kernel_approximation_sample)))

            del kernel_approximation_sample
            del sub_idxs
            del sample
            gc.collect()

            scaler = StandardScaler()
            pca = IncrementalPCA(pca_components, whiten=True)
            svm = SGDOneClassSVM(self._nu)
            visualisation_pca = IncrementalPCA(2, whiten=True)
            dropped = 0
            explained_variances = []
            means = []
            mean_variances = []
            positive_results_store = []
            ratio_positive_settings = []
            positive_results = 0
            positive_results_percent = 100
            total_results = 0
            training_sample_offset = 0
            training_samples = OrderedDict()
            fitted = False
            self.set_status(f"Finalising: Composing {n}-grams")
            for n_idx, (ngram_hash, weighting_sparse_store) in enumerate(_prev_grams.items()):
                weighting = sparse_store_to_masked(weighting_sparse_store)
                ngram = prev_frozenbag_lookup[ngram_hash]
                percent_done = n_idx * 100 / len(_prev_grams)
                percent_dropped = 0
                try:
                    percent_dropped = dropped * 100 / (n_idx * len_nodes)
                except ZeroDivisionError:
                    pass
                explained_variance_ratio = 0
                for next_node_idx in range(len_nodes):
                    scaler_status = ""
                    mean = 0
                    mean_variance = 0
                    if hasattr(scaler, 'n_samples_seen_'):
                        mean = np.mean(scaler.mean_)
                        mean_variance = np.mean(scaler.var_)
                        scaler_status = f"\nScaler: n_samples = {scaler.n_samples_seen_}; n_features = {scaler.n_features_in_}; mean = {mean:.2f}; mean_variance = {mean_variance:.2f}.\nPCA ({pca.n_components_} components) explains {explained_variance_ratio*100:.2f}% variance."
                        if not fitted:
                            scaler_status += "\nSVM: Not yet fitted"
                        else:
                            positive_results_percent = 0 if total_results == 0 else (positive_results * 100 / total_results)
                            scaler_status += f"\nNystroem kernel: {kernel_approximation_components} components.\nSVM: Positive {positive_results_percent:.2f}% (Training schedule {learning_rate:.2f})"
                    self.set_status(f"Finalising: Composing {n}-grams {percent_done:.2f}% (Skip rate {percent_dropped:.2f}%)" + scaler_status)
                    if not np.any(weighting[:, next_node_idx]):
                        continue
                    new_frozenbag = ngram + frozenbag((next_node_idx,))
                    sqlite_hashed = hex(hash(new_frozenbag))
                    frozenbag_lookup[sqlite_hashed] = new_frozenbag
                    reshaped_weighting = weighting_matrix[:, next_node_idx].reshape((weighting_matrix.shape[0], -1))
                    # New weighting is P(A) + P(B) - P(AB)
                    new_weighting = weighting + reshaped_weighting - weighting * reshaped_weighting
                    if sqlite_hashed in _n_grams.keys():
                        original_weighting = sparse_store_to_masked(_n_grams[sqlite_hashed])
                        original_unmasked = ma.getdata(original_weighting)
                        original_mask = ma.getmask(original_weighting)
                        new_unmasked = ma.getdata(new_weighting)
                        new_mask = ma.getmask(new_weighting)
                        only_new_masked = new_mask & ~original_mask
                        both_masked = original_mask & new_mask
                        none_masked = ~both_masked
                        new_unmasked[only_new_masked] = original_unmasked[only_new_masked]
                        new_unmasked[none_masked] = np.maximum(original_unmasked[none_masked], new_unmasked[none_masked])
                        new_weighting = ma.masked_array(new_unmasked, mask=both_masked)
                        if ma.allclose(new_weighting, original_weighting, masked_equal=True):
                            dropped += 1
                            continue
                    if ma.is_masked(new_weighting[:, 0]):
                        training_samples[sqlite_hashed] = ((new_weighting.mask[:, 0] != True).nonzero()[0], new_weighting, training_sample_offset)
                        offset = np.count_nonzero(new_weighting.mask[:, 0] != True)
                    else:
                        training_samples[sqlite_hashed] = (np.arange(new_weighting.shape[0]), new_weighting, training_sample_offset)
                        offset = new_weighting.shape[0]
                    training_sample_offset += offset
                    del new_weighting
                    del new_frozenbag
                    if training_sample_offset < pca_components or training_sample_offset < self._minimum_batch:
                        min_offset = min([x[2] for x in training_samples.values()])
                        training_samples = OrderedDict({k: (a, b, c - min_offset) for k, (a, b, c) in training_samples.items()})
                        assert np.all([x[2] >= 0 for x in training_samples.values()])
                        max_offset = max([(x[2], x[0].shape[0]) for x in training_samples.values()], key=sum)
                        training_sample_offset = max_offset[0] + max_offset[1]
                        print("BATCH AROUND")
                        continue
                    if len(training_samples) == 0:
                        training_samples = {}
                        training_sample_offset = 0
                        continue
                    reshaped = np.concatenate([n_w.compressed().reshape((-1,) + n_w.shape[1:]) for training_sample, n_w, offset in training_samples.values()], axis=0)
                    if reshaped.shape[0] == 0:
                        dropped += len(training_samples)
                        training_samples = {}
                        training_sample_offset = 0
                        del reshaped
                        continue
                    try:
                        assert reshaped.shape[0] == training_sample_offset
                    except AssertionError as e:
                        print(f"{reshaped.shape[0]} vs {training_sample_offset}. {len(training_samples)} training batches")
                        for i_bag, (i_idxs, i_weighting, i_offset) in training_samples.items():
                            print(i_bag, i_idxs.shape, i_weighting.shape, i_offset)
                        raise e
                    if n >= self._min_svm:
                        scaler.partial_fit(reshaped)
                        scaled = scaler.transform(reshaped)
                        del reshaped
                        pca.partial_fit(scaled)
                        visualisation_pca.partial_fit(scaled)
                        explained_variance_ratio = np.sum(pca.explained_variance_ratio_)
                        pcaed = pca.transform(scaled)
                        del scaled
                        nystroemed = kernel_approximation.transform(pcaed)
                        del pcaed
                        learning_rate = self._learning_rate_scheduler.send(current_step)
                        sample = nystroemed
                        n_select = int(np.ceil(nystroemed.shape[0] * learning_rate))
                        if n_select < nystroemed.shape[0]:
                            sample = rng.choice(nystroemed, size=n_select, axis=0, replace=False)
                        svm.partial_fit(sample)
                        try:
                            labels = svm.predict(nystroemed)
                            fitted = True
                        except NotFittedError:
                            labels = np.full((nystroemed.shape[0],), -1)
                        del nystroemed
                        current_step += sample.shape[0]
                        this_positive = np.count_nonzero(labels == -1)
                        explained_variances.append(np.array(pca.explained_variance_ratio_))
                    else:
                        labels = np.full((reshaped.shape[0],), -1)
                        this_positive = labels.shape[0]
                    positive_results += this_positive
                    ratio_positive_settings.append(this_positive / labels.shape[0])
                    total_results += labels.shape[0]
                    means.append(mean)
                    mean_variances.append(mean_variance)
                    positive_results_store.append(positive_results_percent / 100)
                    for i_frozenbag_hash, (i_training_samples, i_new_weighting, i_training_sample_offset) in training_samples.items():
                        if n >= self._min_svm:
                            these_labels = labels[i_training_sample_offset:(i_training_sample_offset + i_training_samples.shape[0])]
                            filtered_idxs = i_training_samples[these_labels == 1]
                            i_new_weighting[filtered_idxs] = ma.masked
                            if np.any(i_new_weighting):
                                _n_grams[i_frozenbag_hash] = masked_to_sparse_store(i_new_weighting)
                        else:
                            _n_grams[i_frozenbag_hash] = masked_to_sparse_store(i_new_weighting)
                    _n_grams.commit()
                    del labels
                    gc.collect()
            if len(training_samples) == 0:
                continue
            reshaped = np.concatenate([n_w.compressed().reshape((-1,) + n_w.shape[1:]) for training_sample, n_w, offset in training_samples.values()], axis=0)
            if reshaped.shape[0] == 0:
                dropped += len(training_samples)
                continue
            if n >= self._min_svm:
                scaler.partial_fit(reshaped)
                scaled = scaler.transform(reshaped)
                if reshaped.shape[0] >= pca_components:
                    pca.partial_fit(scaled)
                    visualisation_pca.partial_fit(scaled)
                del reshaped
                explained_variance_ratio = np.sum(pca.explained_variance_ratio_)
                pcaed = pca.transform(scaled)
                del scaled
                nystroemed = kernel_approximation.transform(pcaed)
                del pcaed
                sample = nystroemed
                n_select = int(np.ceil(nystroemed.shape[0] * learning_rate))
                if n_select < nystroemed.shape[0]:
                    sample = rng.choice(nystroemed, size=n_select, axis=0, replace=False)
                svm.partial_fit(sample)
                try:
                    labels = svm.predict(nystroemed)
                    fitted = True
                except NotFittedError:
                    labels = np.full((nystroemed.shape[0],), -1)
                current_step += sample.shape[0]
                del nystroemed
                explained_variances.append(np.array(pca.explained_variance_ratio_))
                means.append(np.mean(scaler.mean_))
                mean_variances.append(np.mean(scaler.var_))
            else:
                labels = np.full((reshaped.shape[0],), -1)
            total_results += labels.shape[0]
            positive_results_store.append(positive_results_percent / 100)
            for i_frozenbag_hash, (i_training_samples, i_new_weighting, i_training_sample_offset) in training_samples.items():
                if n >= self._min_svm:
                    these_labels = labels[i_training_sample_offset:(i_training_sample_offset + i_training_samples.shape[0])]
                    filtered_idxs = i_training_samples[these_labels == 1]
                    i_new_weighting[filtered_idxs] = ma.masked
                    if np.any(i_new_weighting):
                        this_positive = np.count_nonzero(these_labels == -1)
                        positive_results += this_positive
                        ratio_positive_settings.append(this_positive / these_labels.shape[0])
                        _n_grams[i_frozenbag_hash] = masked_to_sparse_store(i_new_weighting)
                else:
                    this_positive = labels.shape[0]
                    positive_results += this_positive
                    ratio_positive_settings.append(this_positive / labels.shape[0])
                    _n_grams[i_frozenbag_hash] = masked_to_sparse_store(i_new_weighting)
            _n_grams.commit()
            del training_samples
            del labels

            # Do visualisation
            xx, yy = np.meshgrid(
                np.linspace(-1, 1, self._visualisation_samples),
                np.linspace(-1, 1, self._visualisation_samples)
            )
            visualisation_vals = np.c_[xx.ravel(), yy.ravel()]
            inverse_transformed = visualisation_pca.inverse_transform(visualisation_vals)
            visualisation_pcaed = pca.transform(inverse_transformed)
            visualisation_nystroemed = kernel_approximation.transform(visualisation_pcaed)
            visualisation_z = svm.decision_function(visualisation_nystroemed)
            visualisation_stats = np.c_[xx.ravel(), yy.ravel(), visualisation_z]
            yield GraphStatisticRecord(
                '2D PCA Visualisation',
                n,
                ("X", "Y", "Distance"),
                visualisation_stats
            )

            gc.collect()
            training_samples = {}
            training_sample_offset = 0
        yield from []
