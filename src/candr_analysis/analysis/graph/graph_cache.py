"""Cache analysis graphs in memory"""

from typing import Iterator
import networkx as nx
from . import graph_record
from ...utilities.pipeline import PipeObject

class GraphCache(PipeObject):
    """GraphCreator with a memory rather than SQLite store"""
    def __init__(self, flush = None, logger = None, **kwargs):
        self._representation_type = 'Not specified'
        self.graph = nx.MultiDiGraph()
        self._logger = logger
        self._flush = flush
        super().__init__(**kwargs)
    def process(self, input_data: graph_record.GraphRecord) -> Iterator[graph_record.GraphRecord]:
        """For every record, link the first to the second relating to that
        instance"""
        instance = input_data.instance
        link_type = input_data.relationship
        first = input_data.first
        second = input_data.second
        weight = None
        if hasattr(input_data, 'weight'):
            weight = input_data.weight
        edge_update = {}
        did_update = False
        for u, v, k, d in self.graph.edges(nbunch=first, data=True, keys=True):
            if (
                    v != second
                    or d['instance'] != instance
                    or d['link_type'] != link_type
                    or d['representation_type'] != self._representation_type
            ):
                continue
            did_update = True
            uvk_tup = (u, v, k)
            if uvk_tup not in edge_update:
                edge_update[uvk_tup] = {}
            if weight:
                if 'weight' not in edge_update[uvk_tup]:
                    edge_update[uvk_tup]['weight'] = d['weight']
                edge_update[uvk_tup]['weight'] += weight
            else:
                if 'weight' not in edge_update[uvk_tup]:
                    edge_update[uvk_tup]['weight'] = 0
                edge_update[uvk_tup]['weight'] += 1
        if did_update:
            nx.set_edge_attributes(self.graph, edge_update)
        else:
            attr_dict = {
                'instance': instance,
                'link_type': link_type,
                'representation_type': self._representation_type
            }
            if weight:
                attr_dict['weight'] = weight
            else:
                attr_dict['weight'] = 1
            self.graph.add_edge(first, second, key=None, **attr_dict)
        if self._flush is not None and self.graph.number_of_edges() >= self._flush:
            yield from self.flush()
        else:
            yield from []
    def _processing_string(self) -> str:
        """Get processing string for status update"""
        n_edges = self.graph.number_of_edges()
        return str(n_edges) + " links"
    def finalise(self) -> Iterator[graph_record.GraphRecord]:
        """Finish off this cache"""
        yield from self.flush()
    def flush(self) -> Iterator[graph_record.GraphRecord]:
        """Flush the cache to output"""
        coll = list([graph_record.GraphRecord(
            d['link_type'],
            u,
            v,
            d['instance'],
            self._representation_type
        ) for u, v, d in self.graph.edges(data=True)])
        yield graph_record.GraphRecordCollection(coll)
        self.graph = nx.MultiDiGraph()
        self._count = 0

class MEIGraphCache(GraphCache):
    """MEI flavour of graph cache"""
    def __init__(self, flush = None, logger = None, **kwargs):
        super().__init__(flush, logger, **kwargs)
        self._representation_type = 'MEI'
    def flush(self) -> Iterator[graph_record.GraphRecord]:
        """Flush the cache to output"""
        coll = list([graph_record.MEIGraphRecord(
            d['link_type'],
            u,
            v,
            d['instance'],
            self._representation_type,
            d['weight']
        ) for u, v, d in self.graph.edges(data=True)])
        yield graph_record.GraphRecordCollection(coll)
        self.graph = nx.MultiDiGraph()
