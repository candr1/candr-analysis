from typing import Iterator
from collections import namedtuple
from itertools import tee
from ...utilities.pipeline import PipeObject
from ...transformers.text.gram import NGramsTranscriptionRecord

"""Converts grams into records to be inserted into a graph"""

GraphRecord = namedtuple('GraphRecord', ['relationship', 'first', 'second', 'instance', 'type'])

GraphRecordCollection = namedtuple('GraphRecordCollection', ['records'])

class TextGraphRecord(GraphRecord):
    pass

MEIGraphRecord = namedtuple(
    'MEIGraphRecord',
    TextGraphRecord._fields + ('weight',),
    defaults=(None, None, None, None, None, 1)
)

class TextGraphRecordTransformer(PipeObject):
    def process(self, input_data: NGramsTranscriptionRecord) -> Iterator[TextGraphRecord]:
        """Convert an Ngram transcription into a whole load of records to insert
        into a graph"""
        a, b = tee(input_data.transcription)
        next(b, None)
        for first, second in zip(a, b):
            yield TextGraphRecord('nextgram', first, second, input_data.subject, input_data.type)
