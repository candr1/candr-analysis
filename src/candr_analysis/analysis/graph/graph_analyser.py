"""Query analysis graphs from input data"""

import math
from typing import Iterator
from collections import namedtuple, defaultdict
from itertools import combinations
from sqlalchemy.orm import aliased, with_polymorphic
import numpy as np
import scipy.optimize
import uuid
from frozendict import frozendict
from . import graph_analysis, models
from ...utilities.pipeline import PipeObject

RepresentationLinks = namedtuple(
    'RepresentationLinks',
    ['representation', 'links_dict'],
    defaults=(None, frozendict())
)
RepresentationSimilarities = namedtuple(
    'RepresentationSimilarities',
    ['representation', 'similarities'],
    defaults=(None, frozendict())
)
GraphLink = namedtuple(
    'GraphLink',
    ['type', 'strength', 'direction', 'link'],
    defaults=(None, 1, None, None)
)

class GraphAnalyserQuery(PipeObject):
    """PipeObject that extracts links from a graph"""
    def __init__(self, store: str = 'graph.db', **kwargs):
        self.graph = graph_analysis.Graph(store)
        self._representation_type = self._get_representation_type('Not specified')
        self._instance_type = self._get_instance_type('Not specified')
        self.__processing_i_len = [0, 0]
        super().__init__(**kwargs)
    def _get_instance_type(self, instance_type_str):
        """An instanceType is how that item is represented in Python, such as
        text or MEI"""
        return self.graph._get_or_create(
            models.InstanceType,
            name = instance_type_str
        )
    def _get_representation_type(self, representation_str):
        """A representationType is how that item is represented in Python, such
        as text or MEI"""
        return self.graph._get_or_create(
            models.RepresentationType,
            name = representation_str
        )
    def _get_model(self, model, **kwargs):
        """Get a model of type"""
        return self.graph._orm.session.query(model).filter_by(**kwargs).first()
    def _get_all_representations(self):
        """Get all the representations in the database"""
        return self.graph._orm.session.query(models.Representation).filter_by(
            type = self._representation_type
        )
    def _get_links_query(self, representation):
        """Get all the links for this representation"""
        # two LinkAssociation: one for node_a, one for node_b
        link_assoc_a = aliased(models.LinkAssociation)
        link_assoc_b = aliased(models.LinkAssociation)
        # two nodes, including their base attributes
        node_a = with_polymorphic(
            models.Representable,
            [models.Node],
            flat=True,
            aliased=True
        )
        node_b = with_polymorphic(
            models.Representable,
            [models.Node],
            flat=True,
            aliased=True
        )
        # two RepresentationAssociations (a / b)
        representationAssociation_a = aliased(models.RepresentationAssociation)
        representationAssociation_b = aliased(models.RepresentationAssociation)
        # representations linked to associations...
        representation_a = aliased(models.Representation)
        representation_b = aliased(models.Representation)
        # their respective types
        representation_type_a = aliased(models.RepresentationType)
        representation_type_b = aliased(models.RepresentationType)
        # the instance connected to node_a
        instance_a = with_polymorphic(
            models.Representable,
            [models.Instance],
            flat=True,
            aliased=True
        )
        # the instance connected to node_b
        instance_b = with_polymorphic(
            models.Representable,
            [models.Instance],
            flat=True,
            aliased=True
        )
        # link_assoc direction types
        direction_type_a = aliased(models.DirectionType)
        direction_type_b = aliased(models.DirectionType)
        # instance_types
        instance_type_a = aliased(models.InstanceType)
        instance_type_b = aliased(models.InstanceType)
        # One huge SELECT statement, no subqueries, just joins
        return self.graph._orm.session.query(
            # Node ID for grouping and backreferencing
            node_a.id,
            # The representation itself
            representation_a.representation,
            # Name of the instance that contains node_a
            # (should be the same for node_b)
            instance_a.Instance.name,
            # Direction of the link
            direction_type_a.name,
            # Type of the link
            models.LinkType.name,
            # Strength of the link
            models.Link.strength,
            # Second node representation
            representation_b.representation
        ).join(
            # Join representationType
            representation_type_a,
            representation_type_a.id == representation_a.type_id
        ).join(
            # Join representationAssociation
            representationAssociation_a,
            representation_a.id == representationAssociation_a.columns.representation_id
        ).join(
            # Join to first node
            node_a,
            representationAssociation_a.columns.representable_id == node_a.Node.id
        ).join(
            instance_a,
            node_a.Node.instance_id == instance_a.Instance.id
        ).join(
            instance_type_a,
            instance_a.Instance.type_id == instance_type_a.id
        ).join(
            # Get the first node's association
            link_assoc_a,
            node_a.Node.id == link_assoc_a.node_id
        ).join(
            # And that association's direction type
            direction_type_a,
            link_assoc_a.type_id == direction_type_a.id
        ).join(
            # The association's link
            models.Link,
            link_assoc_a.link_id == models.Link.id
        ).join(
            # The link's type
            models.LinkType,
            models.Link.type_id == models.LinkType.id
        ).join(
            # (Other side of the link) the next association
            link_assoc_b,
            link_assoc_b.link_id == models.Link.id
        ).join(
            # The second association's direction type
            direction_type_b,
            link_assoc_b.type_id == direction_type_b.id
        ).join(
            # And the association's node
            node_b,
            node_b.id == link_assoc_b.node_id
        ).join(
            # And the instance connected to that node
            instance_b,
            node_b.Node.instance_id == instance_b.Instance.id
        ).join(
            instance_type_b,
            instance_b.Instance.type_id == instance_type_b.id
        ).join(
            # Association item for representations of that node
            representationAssociation_b,
            node_b.Node.id == representationAssociation_b.columns.representable_id
        ).join(
            # Representation of node_b
            representation_b,
            representationAssociation_b.columns.representation_id == representation_b.id
        ).join(
            # Type of that representation
            representation_type_b,
            representation_type_b.id == representation_b.type_id
        ).filter(
            # One must be in the other out
            direction_type_b.id != direction_type_a.id
        ).filter(
            # Filter out getting the first node again when finding second nodes
            link_assoc_a.node_id != link_assoc_b.node_id
        ).filter(
            # For integrity I guess
            models.Link.instance_id == node_a.Node.instance_id
        ).filter(
            # Where the first node's representation is our parameter
            representation_a.representation == representation
        ).filter(
            # And the type of the representations is correct
            representation_a.type_id == self._representation_type.id
        ).filter(
            # ...
            representation_b.type_id == self._representation_type.id
        ).filter(
            # Type of the instances is correct
            instance_type_a.id == self._instance_type.id
        ).filter(
            # ...
            instance_type_b.id == self._instance_type.id
        )
    def _processing_string(self) -> str:
        """Get the processing string for status update"""
        if self.__processing_i_len[1] > 0:
            return (
                "Processing (" +
                str(self.__processing_i_len[0]) +
                "/" +
                str(self.__processing_i_len[1]) +
                ")"
            )
        else:
            return "Processing"
    def process(self, _) -> Iterator[RepresentationLinks]:
        """Process DB links into output"""
        representations = list(self._get_all_representations())
        self.__processing_i_len[1] = len(representations)
        for i, representation in enumerate(representations):
            self.__processing_i_len[0] = i 
            d = defaultdict(list)
            for (
                node_id,
                first_representation,
                instance_name,
                direction_type,
                link_type,
                link_strength,
                second_representation
            ) in self._get_links_query(representation.representation):
                d[(node_id, instance_name)].append(GraphLink(
                   link_type,
                   link_strength,
                   direction_type,
                   second_representation
                ))
            yield RepresentationLinks(
                representation.representation,
                frozendict({k: tuple(v) for k, v in d.items()})
            )
        raise EOFError

DummyAnalyserNode = namedtuple('DummyAnalyserNode', ['id', 'uniqid'])

class DummyAnalyserQuery(PipeObject):
    """Debugging-useful class that spits out random data for analysis"""
    def __init__(self, n_nodes = 1000, mu_links: int = 5, **kwargs):
        self._n_nodes = n_nodes
        self._mu_links = mu_links
        super().__init__(**kwargs)
    def process(self, _) -> Iterator[RepresentationLinks]:
        """Create some random, chi-squared data"""
        my_uuid = uuid.uuid4()
        nodes = list([DummyAnalyserNode(i, str(my_uuid)) for i in range(self._n_nodes)])
        for node in nodes:
            links = np.round(np.random.chisquare(1, self._n_nodes) * self._mu_links) + 1
            dict = {}
            for i, other_node in enumerate(nodes):
                if node is other_node:
                    continue
                n_links = int(links[i])
                idxs = np.random.randint(0, self._n_nodes, n_links)
                strength = np.random.chisquare(1, n_links)
                dict[other_node] = tuple(
                    [GraphLink(
                        'dummy',
                        strength[j],
                        'dummy',
                        nodes[idx]
                    ) for j, idx in enumerate(idxs)]
                )
            yield RepresentationLinks(
                node,
                frozendict(dict)
            )
        raise EOFError

class GraphNodesSimilarityAnalyse(PipeObject):
    """Analyse the similarity between nodes"""
    def __init__(self, similarity_func, **kwargs):
        self._similarity_func = similarity_func
        super().__init__(**kwargs)
    def process(self, input_data: RepresentationLinks) -> Iterator[RepresentationSimilarities]:
        """Convert a representation's node links into similarity scores"""
        representation = input_data.representation
        links_dict = input_data.links_dict
        similarities = defaultdict(lambda: defaultdict(lambda: 0))
        n_comb = math.comb(len(links_dict), 2)
        for i, ((
            (a_key, a_links)
        ), (
            (b_key, b_links)
        )) in enumerate(combinations(links_dict.items(), 2)):
            percent_done = i * 100 / n_comb
            self.set_status(f"Processing: ({percent_done:.2f}%)")
            similarity = self._similarity_func((a_key, a_links), (b_key, b_links))
            similarities[a_key][b_key] = similarity
        yield RepresentationSimilarities(
            representation,
            frozendict(
                {k: tuple(sorted(
                    v.items(),
                    reverse=True,
                    key=lambda it: it[1]
                )) for k, v in similarities.items()}
            )
        )
        
def link_similarity_default(type_ = 1, direction = 1, strength_multiplier = 1, link = 1):
    """Return a function that calculates similarity of links based on parameters"""
    def comp(a, b):
        nonlocal type_
        nonlocal direction
        nonlocal strength_multiplier
        nonlocal link
        a_type = 'synch' if a.type == 'softsynch' else a.type
        b_type = 'synch' if b.type == 'softsynch' else a.type
        type_sim = type_ if a_type == b_type else 0
        direction_sim = direction if a.direction == b.direction else 0
        strength_sim = strength_multiplier / (np.abs(a.strength - b.strength) + 1)
        link_sim = link if a.link == b.link else 0
        return (
            type_sim ** 2 +
            direction_sim ** 2 +
            strength_sim ** 2 +
            link_sim ** 2
        )
    return comp

def graph_similarity_default(link_similarity_func, missing_link_penalty = 0):
    """Return a function that calculates similarity of nodes based on parameters"""
    def comp(a, b):
        nonlocal link_similarity_func
        nonlocal missing_link_penalty
        a_key, a_links = a
        b_key, b_links = b

        max_links = max(len(a_links), len(b_links))
        min_links = min(len(a_links), len(b_links))
        if min_links == 0:
            return 0
        total_missing_link_penalty = (max_links - min_links) * missing_link_penalty
        cost_matrix = np.zeros((max_links, max_links))
        for i in range(len(a_links)):
            for j in range(len(b_links)):
                cost_matrix[i][j] = link_similarity_func(a_links[i], b_links[j])
        row_ind, col_ind = scipy.optimize.linear_sum_assignment(cost_matrix, maximize=True)
        calculated_similarity = (
            (cost_matrix[row_ind, col_ind].sum() - total_missing_link_penalty)
            / max_links
        )
        return np.clip(calculated_similarity, 0, None)
    return comp

class TextGraphAnalyserQuery(GraphAnalyserQuery):
    """A graph analyser just for text"""
    def __init__(self, store: str = 'graph.db', **kwargs):
        super().__init__(store, **kwargs)
        self._representation_type = self._get_representation_type('Text')
        self._instance_type = self._get_instance_type('Text')

class MEIGraphAnalyserQuery(GraphAnalyserQuery):
    """A graph analyser for MEI"""
    def __init__(self, store: str = 'graph.db', **kwargs):
        super().__init__(store, **kwargs)
        self._representation_type = self._get_representation_type('MEI')
        self._instance_type = self._get_instance_type('MEI')
