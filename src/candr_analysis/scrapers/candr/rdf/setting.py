"""Scrapes setting data from CANDR using its RDF API"""

from typing import Iterator
from rdflib import Graph, Namespace, RDF
from ....utilities.progress import CLIProgress
from ....utilities.pipeline import PipeObject, SingleConnection

class Setting(PipeObject):
    """CANDR setting data RDF scraper"""
    def __init__(self, root: str = 'https://candr.org.uk/', namespace: str = 'https://candr.org.uk/', progress = CLIProgress(), **kwargs):
        self._root = root
        self._setting_path = self._root + '/browse/setting/'
        self._graph = None
        self._candr_ns = Namespace(namespace)
        self._progress = progress
        super().__init__(**kwargs)
        self.pipe_in = SingleConnection()
    def scrape(self) -> Iterator[Graph]:
        """Scrape the data"""
        self._graph = Graph()
        self._progress.info_1("Scraping setting page")
        self._graph.parse(self._setting_path)
        def make_triple_fun():
            return self._graph.subjects(RDF.type, self._candr_ns.Setting)
        n_settings = sum(1 for _ in make_triple_fun())
        for setting in self._progress.bar(make_triple_fun(), total=n_settings, desc="Scraping settings"):
            yield_graph = Graph()
            yield_graph.parse(setting)
            yield yield_graph
    def process(self, _) -> Iterator[Graph]:
        """Return the next scraped setting"""
        p = next(self._scraper)
        yield p
        return
    def setup(self):
        """Setup the scraper"""
        self._scraper = self.scrape()
