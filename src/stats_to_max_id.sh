#!/bin/bash
mlr --c2j --no-auto-unflatten cat "$1" | \
jq '.[]|with_entries(if (.key|test("_2$")) then ( {key: ( .key|sub("_2$"; "")|split("/")[-2] ), value: .value } ) else empty end )|to_entries|min_by(.value)|[.key, .value]' | \
jq -sr '.[]|@csv'
