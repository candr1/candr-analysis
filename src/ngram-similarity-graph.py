#!/usr/bin/env python3
import sys
import networkx as nx
import pandas as pd
import itertools
from collections import defaultdict
from collections_extended import frozenbag
from tqdm import tqdm

df = pd.read_csv(sys.argv[1], compression='gzip')

n_grams = df[df.columns[:-1]].to_numpy()
settings = df[df.columns[-1]].to_numpy()
n_gram_dict = defaultdict(lambda: set())

for r, s in zip(n_grams, settings):
    n_gram_dict[frozenbag(r.tolist())].add(s)

G = nx.MultiGraph()
for k, v in tqdm(n_gram_dict.items()):
    for a, b in itertools.combinations(v, 2):
        G.add_edge(a, b, k)

def comb_gen(graph):
    for a, b in itertools.combinations(graph.nodes, 2):
        yield (a, b, graph.number_of_edges(a, b))
print(sorted(comb_gen(G), key=lambda a: a[2], reverse=True))
