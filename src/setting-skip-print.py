#!/usr/bin/env python3
from candr_analysis.transformers import pickle
from candr_analysis.utilities import progress, pipeline

pickle_file = 'setting-MEI.pkl'
prog = progress.CLIProgress("MEI skip print grams")
processes = pipeline.Pipeline(progress=prog)
processes.add(pickle.PickleLoader('setting-MEI.pkl', progress=prog))
#processes.add(pipeline.SkipN(810))
def subjprint(x):
    print(x.subject)
processes.add(pipeline.PipePrinter(subjprint))
processes.start()
processes.stop()
