#!/bin/bash
jq -cR 'split(",")' "$1"/data.csv | jq -cs 'del(.[0])|flatten|map(tonumber)' | jq -sr '[.[0], .[1]."bin edges"[:-1], .[1]."bin edges"[1:]]|transpose|["Frequency","Bin start","Bin end"],.[]|@csv' - "$1"/metadata.json
